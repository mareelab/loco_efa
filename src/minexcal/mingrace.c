#include "minexcal.h"

int grace=1;
double graceborder=0.1;

static unsigned char graceCol[256][4]={{1,255, 255, 255},{1,0, 0, 0},{1,255, 0, 0},{1,0, 255, 0},{1,0, 0, 255},{1,255, 255, 0},{1,188, 143, 143},{1,220, 220, 220},{1,148, 0, 211},{1,0, 255, 255},{1,255, 0, 255},{1,255, 165, 0},{1,114, 33, 188},{1,103, 7, 72},{1,64, 224, 208},{1,0, 139, 0}};

extern int lastcolor;
extern unsigned char **userCol;

/********************************************functions*/

void GraceError(const char *msg)
{
  fprintf(stderr,"Grace: error: %s\n",msg);
}

void OpenGrace(int y,int x,char *parfile)
{
  char *first=NULL;
  char *second=NULL;
  char *third=NULL;
  char *fourth=NULL;
  char hardcopy_string[]="-hardcopy";
  char noprint_string[]="-noprint";
  char par_string[]="-par";
#ifdef __APPLE__
  char xmgrace[STRING]="/usr/local/bin/xmgrace";// check that this path is OK
#else
  char xmgrace[STRING]="/usr/bin/xmgrace";// check that this path is OK
#endif
  
  int i,j;
  double width,height;

  if(!grace) {
    first=hardcopy_string;
    second=noprint_string;
#ifdef __APPLE__
    sprintf(xmgrace,"/usr/local/bin/gracebat");
#else
    sprintf(xmgrace,"/usr/bin/grace");
#endif
    if (parfile!=NULL) {
      third=par_string;
      fourth=parfile;
    }
  }
  else if (parfile!=NULL) {
    first=par_string;
    second=parfile;
  }
  
  GraceRegisterErrorFunction(GraceError);

  if (GraceOpenVA(xmgrace,2048,"-nosafe","-noask","-free",first,second,third,fourth,NULL) == -1)
    fprintf(stderr,"OpenGrace: warning: can't run xmgrace\n");

  width=(1.295-graceborder*((double)x+1.))/(double)x;
  height=(1.-1.5*graceborder*((double)y)-graceborder)/(double)y;
  for(i=0;i<y;i++)
    for(j=0;j<x;j++) {
      GracePrintf("g%d on",i*x+j); //generate the graphs
      Grace(GGRAPH,i*x+j);          //focus on graph
      if(parfile==NULL) {
	Grace(GVIEWPORT,graceborder+(double)j*(graceborder+width),graceborder+(double)(y-i-1)*(1.5*graceborder+height),graceborder+(double)j*(graceborder+width)+width,graceborder+(double)(y-i-1)*(1.5*graceborder+height)+height);    //create the right viewports
	GracePrintf("legend on");
	GracePrintf("legend %g, %g",graceborder+(double)j*(graceborder+width),graceborder+(double)(y-i-1)*(1.5*graceborder+height)+height);
      }
    }

  Grace(GGRAPH,0);                  //focus on graph 0
}

int ChangeGraceCol(int color)
{
  static int nextcolor=15;
  int i;
  
  if(color>lastcolor || !userCol[color][0])
    ColorRGB(color,graceCol[color%16][1],graceCol[color%16][2],graceCol[color%16][3]);
  for(i=0;i<256;i++)
    if(graceCol[i][0] && userCol[color][1]==graceCol[i][1] && userCol[color][2]==graceCol[i][2] && userCol[color][3]==graceCol[i][3])
      return i;

  nextcolor=(++nextcolor>255)?16:nextcolor;
  graceCol[nextcolor][0]=1;
  for(i=1;i<4;i++)
    graceCol[nextcolor][i]=userCol[color][i];
  GracePrintf("map color %d to (%d, %d, %d), \"\"",nextcolor,(int)userCol[color][1],(int)userCol[color][2],(int)userCol[color][3]);
  return nextcolor;
}

void Grace(int command,...)
{
  va_list ap;        /* points to each unnamed arg in turn */
  static int graph=0,set=0;
  static char axisstring[STRING]="xax";
  double xmin,xmax,ymin,ymax;
  double x,y;
  int whattodo,color,i,value,newcolor;

  va_start(ap,command);  /* make ap point to first unnamed arg */
  switch (command) {
  case GGRAPH:
    graph=va_arg(ap,int);
    GracePrintf("focus g%d",graph); //`focus' is better than `with', which does not unhide the graph
    break;
  case GSET:
    set=va_arg(ap,int);
    break;
  case GTITLE:
    GracePrintf("title \"%s\"",va_arg(ap,char *));
    break;
  case GSUBTITLE:
    GracePrintf("subtitle \"%s\"",va_arg(ap,char *));
    break;
  case GVIEWPORT:
    xmin=va_arg(ap,double);
    ymin=va_arg(ap,double);
    xmax=va_arg(ap,double);
    ymax=va_arg(ap,double);
    GracePrintf("view %f, %f, %f, %f",xmin,ymin,xmax,ymax);
    break;
  case GWORLD:
    xmin=va_arg(ap,double);
    ymin=va_arg(ap,double);
    xmax=va_arg(ap,double);
    ymax=va_arg(ap,double);
    GracePrintf("world %f, %f, %f, %f",xmin,ymin,xmax,ymax);
    GracePrintf("autoticks");
    break;
  case GAXIS:
    snprintf(axisstring,STRING,"%cax",va_arg(ap,int));
    break;
  case GSCALE:
    GracePrintf("%ses scale %s",axisstring,va_arg(ap,char *));
    break;
  case GLABEL:
    GracePrintf("%sis on",axisstring);
    GracePrintf("%sis label \"%s\"",axisstring,va_arg(ap,char *));
    GracePrintf("%sis label place auto",axisstring);
    break;
  case GSETTYPE:
    GracePrintf("s%d type %s",set,va_arg(ap,char *));
    break;
  case GLEGEND:
    /* GLEGEND extended by Jos Kafer */
    whattodo=va_arg(ap,int);
    switch(whattodo) {
    case GLABEL:
      GracePrintf("s%d legend \"%s\"",set,va_arg(ap,char *));
      break;
    case GWORLD:
      GracePrintf("legend loctype world");
      x=va_arg(ap,double);
      y=va_arg(ap,double);
      GracePrintf("legend %f, %f",x,y);
      break;
    case GVIEWPORT:
      GracePrintf("legend loctype view");
      x=va_arg(ap,double);
      y=va_arg(ap,double);
      GracePrintf("legend %f, %f",x,y);
      break;
    default:
      break;
    }
    break;
  case GLINE:
    whattodo=va_arg(ap,int);
    switch (whattodo) {
    case GTYPE:
      GracePrintf("g%d.s%d line type %d",graph,set,va_arg(ap,int));
      break;
    case GSTYLE:
      GracePrintf("g%d.s%d line linestyle %d",graph,set,va_arg(ap,int));
      break;
    case GCOLOR:
      color=va_arg(ap,int);
      newcolor=ChangeGraceCol(color);
      GracePrintf("g%d.s%d line color %d",graph,set,newcolor);
      break;
    case GPATTERN:
      GracePrintf("g%d.s%d line pattern %d",graph,set,va_arg(ap,int));
      break;
    case GWIDTH:
      GracePrintf("g%d.s%d line linewidth %f",graph,set,va_arg(ap,double));
      break;
    default:
      break;
    }
    break;
  case GSYMBOL:
    whattodo=va_arg(ap,int);
    switch (whattodo) {
    case GTYPE:
      GracePrintf("g%d.s%d symbol %d",graph,set,va_arg(ap,int));
      break;
    case GSTYLE:
      GracePrintf("g%d.s%d symbol linestyle %d",graph,set,va_arg(ap,int));
      break;
    case GCOLOR:
      color=va_arg(ap,int);
      newcolor=ChangeGraceCol(color);
      GracePrintf("g%d.s%d symbol color %d",graph,set,newcolor);
      GracePrintf("g%d.s%d symbol fill color %d",graph,set,newcolor);
      break;
    case GPATTERN:
      value=va_arg(ap,int);
      GracePrintf("g%d.s%d symbol pattern %d",graph,set,value);
      GracePrintf("g%d.s%d symbol fill pattern %d",graph,set,value);
      break;
    case GSIZE:
      GracePrintf("g%d.s%d symbol size %f",graph,set,va_arg(ap,double));
      break;
    case GWIDTH:
      GracePrintf("g%d.s%d symbol linewidth %f",graph,set,va_arg(ap,double));
      break;
    default:
      break;
    }
    break;
  case GERRORBAR:
    whattodo=va_arg(ap,int);
    switch (whattodo) {
    case GTYPE:
      GracePrintf("g%d.s%d errorbar place %s",graph,set,va_arg(ap,char *));
      break;
    case GSTYLE:
      GracePrintf("g%d.s%d errorbar linestyle %d",graph,set,va_arg(ap,int));
      break;
    case GCOLOR:
      color=va_arg(ap,int);
      newcolor=ChangeGraceCol(color);
      GracePrintf("g%d.s%d errorbar color %d",graph,set,newcolor);
      break;
    case GPATTERN:
      GracePrintf("g%d.s%d errorbar pattern %d",graph,set,va_arg(ap,int));
      break;
    case GSIZE:
      GracePrintf("g%d.s%d errorbar size %f",graph,set,va_arg(ap,double));
      break;
    case GWIDTH:
      GracePrintf("g%d.s%d errorbar linewidth %f",graph,set,va_arg(ap,double));
      break;
    default:
      break;
    }
    break;
  case GPOINT:
    whattodo=va_arg(ap,int);
    x=va_arg(ap,double);
    y=va_arg(ap,double);
    GracePrintf("g%d.s%d point %g, %g",graph,set,x,y);
    for(i=2;i<whattodo;i++)
      GracePrintf("g%d.s%d.y%d[s%d.LENGTH - 1] = %g",graph,set,i-1,set,va_arg(ap,double));
    break;
  case GAUTOSCALE:
    GracePrintf("autoscale");
    if (grace && GraceIsOpen())
      GracePrintf("redraw");
    break;
  case GREDRAW:
    if (grace && GraceIsOpen())
      GracePrintf("redraw");
    break;
  case GKILL:
    GracePrintf("kill g%d.s%d",graph,set);
    break;
    /* GPRINT by Jos Kafer */
  case GPRINT:
    GracePrintf("hardcopy device \"%s\"",va_arg(ap,char *));
    GracePrintf("print to device");
    GracePrintf("print to \"%s\"",va_arg(ap,char *));
    GracePrintf("print");
    break;
  case GSAVE:
    GracePrintf("saveall \"%s\"",va_arg(ap,char *));
    break;
    /* GLOAD en GSORT by Jos Kafer */
  case GLOAD:
    GracePrintf("load \"%s\"",va_arg(ap,char *));
    break;
  case GSORT:
    GracePrintf("sort g%d.s%d %s %s",graph,set,va_arg(ap,char *),va_arg(ap,char *));
    break;
  default:
    break;
  }
  va_end(ap); /* clean up */
}

void CloseGrace(int command,...)
{
  va_list ap;        /* points to each unnamed arg in turn */

  va_start(ap,command);  /* make ap point to first unnamed arg */
  switch (command) {
  case GDETACH:
    if(!grace)
      fprintf(stderr,"CloseGrace: warning: detaching xmgrace which runs in the background\n");
    GraceFlush();
    GraceClosePipe();
    sleep(1); //bug in xmgrace, needs pause
    break;
  case GCLOSE:
    GraceFlush();
    GraceClose();
    sleep(1); //bug in xmgrace, needs pause
    break;
  default:
    break;
  }
  va_end(ap); /* clean up */
}
