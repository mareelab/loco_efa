#include "minexcal.h"

extern int nrow;
extern int ncol;

/********************************************functions*/

TYPE **Xor(TYPE **a,TYPE  **b,TYPE  **c)
{
  PLANE(
	a[i][j] = (TYPE)(b[i][j] != c[i][j]);
	);
  return a;
}
