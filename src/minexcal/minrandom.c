#include "minexcal.h"

#define MBIG_ 1000000000
#define MSEED_ 161803398
#define MZ_ 0
#define FAC_ (1.0/MBIG_)

#define IB1_ 1
#define IB2_ 2
#define IB5_ 16
#define IB1_8_ 131072

int idum = -1;
long idum2 = -1;
unsigned long iseed=55;

/********************************************functions*/

double uniform()
{
  /* Knuth's substrative method,ran3, see Numerical Recipes */
  static int inext,inextp;
  static long ma[56];
  static int iff=0;
  long mj,mk;
  int i,ii,k;

  if (idum < 0 || iff == 0) {
    iff=1;
    mj=labs(MSEED_-labs(idum));
    mj %= MBIG_;
    ma[55]=mj;
    mk=1;
    for (i=1;i<=54;i++) {
      ii=(21*i) % 55;
      ma[ii]=mk;
      mk=mj-mk;
      if (mk < MZ_) mk += MBIG_;
      mj=ma[ii];
    }
    for (k=1;k<=4;k++)
      for (i=1;i<=55;i++) {
        ma[i] -= ma[1+(i+30) % 55];
        if (ma[i] < MZ_) ma[i] += MBIG_;
      }
    inext=0;
    inextp=31;
    idum=1;
  }
  if (++inext == 56) inext=1;
  if (++inextp == 56) inextp=1;
  mj=ma[inext]-ma[inextp];
  if (mj < MZ_) mj += MBIG_;
  ma[inext]=mj;
  return mj*FAC_;
}

int booleanrandom() 
{
  /*irbit1*/
  unsigned long newbit;
  newbit = (iseed & IB1_8_) >> 17
    ^ (iseed & IB5_) >> 4
    ^ (iseed & IB2_) >> 1
    ^ (iseed & IB1_);
  iseed=(iseed << 1) | newbit;
  return (int) newbit;
}

int set_seed(int seed)
{
  int i;
  idum = -seed;
  idum2 = -seed;
  iseed = (unsigned long)seed;
  for (i=0; i<100; i++)
    RANDOM();
  for (i=0; i<100; i++)
    booleanrandom();
  return seed;
}
