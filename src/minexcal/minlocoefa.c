#include "minexcal.h"

extern double locoefaerror;
extern int locoefamodes;
extern int locoefaareanormalisation;
extern int nrow;
extern int ncol;
extern int (*newindex)(int,int);
extern int boundaryvalue;
extern int boundary;
extern int specialcelltype;

/********************************************functions*/

void InitLOCOEFA(Cell *cell)
{
  static int firsttime=0;
  int c;
  
  if(firsttime || cell->contour==NULL) {
    if((cell->contour=(Contour **)calloc((size_t)cell->maxcells,sizeof(Contour*)))==NULL) {
      fprintf(stderr,"InitLOCOEFA: error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
  }
  if(firsttime || cell->contourlength==NULL) {
    if((cell->contourlength=(int *)calloc((size_t)cell->maxcells,sizeof(int)))==NULL) {
      fprintf(stderr,"InitLOCOEFA: error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
  }
  if(firsttime || cell->locoefa==NULL) {
    if((cell->locoefa=(Locoefa **)calloc((size_t)cell->maxcells,sizeof(Locoefa*)))==NULL) {
      fprintf(stderr,"InitLOCOEFA: error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
    for(c=0;c<cell->maxcells;c++) {
      if((cell->locoefa[c]=(Locoefa *)calloc((size_t)(locoefamodes+2),sizeof(Locoefa)))==NULL) {
	fprintf(stderr,"InitLOCOEFA: error in memory allocation\n");
	exit(EXIT_FAILURE);
      }
    }
  }
}

void InitLOCOEFAContour(Cell *cell,int cellnumber,int contourlength)
{
  if(cellnumber<0 && cellnumber>=cell->maxcells) {
    fprintf(stderr,"InitLOCOEFAContour: invalid cell number\n");
    exit(EXIT_FAILURE);
  }
  if(cell->contour[cellnumber]!=NULL) {
    free(cell->contour[cellnumber]);
    cell->contour[cellnumber]=NULL;
  }
    
  if((cell->contour[cellnumber]=(Contour *)calloc((size_t)(contourlength+1),sizeof(Contour)))==NULL) {
    fprintf(stderr,"InitLOCOEFAContour: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  cell->contourlength[cellnumber]=contourlength;
}

void ReadContour(Cell *cell,int cellnumber,char *filename)
{
  FILE *fp;
  int i,c;
  int lines=0;
  int K;
  
  fp=fopen(filename,"r");
  while ((c = fgetc(fp)) != EOF) {
    if(c == '\n') {
      lines++;
    }
  }
  if (ferror(fp)) {
    fprintf(stderr,"ReadContour: error reading file: %s\n",filename);
    exit(EXIT_FAILURE);
  }

  lines+=2; //add one line in case loop not closed, and add one line in case no newline at the end of the file
  fclose(fp);

  InitLOCOEFAContour(cell,cellnumber,lines);

  K=0;
  fp=fopen(filename,"r");
  while (fscanf(fp,"%lf,%lf",&cell->contour[cellnumber][K].x,&cell->contour[cellnumber][K].y) != EOF) {
    //check if not accidentally same point; if so, ignore
    if(K==0 || fabs(cell->contour[cellnumber][K].x-cell->contour[cellnumber][K-1].x)>locoefaerror ||
       fabs(cell->contour[cellnumber][K].y-cell->contour[cellnumber][K-1].y)>locoefaerror)
      K++;
  }
  fclose(fp);
  if(K && (fabs(cell->contour[cellnumber][0].x-cell->contour[cellnumber][K-1].x)>locoefaerror ||
	   fabs(cell->contour[cellnumber][0].y-cell->contour[cellnumber][K-1].y)>locoefaerror)) {
    //if last point not equal to first point, add one point equal to first point
    cell->contour[cellnumber][K].x=cell->contour[cellnumber][0].x;
    cell->contour[cellnumber][K].y=cell->contour[cellnumber][0].y;
  }
  else
    K--;

  if(K<2) {
    fprintf(stderr,"ReadContour: error: contour contains only one point\n");
    exit(EXIT_FAILURE);
  }

  if(K<=cell->contourlength[cellnumber])
    cell->contourlength[cellnumber]=K;
  else {
    fprintf(stderr,"ReadContour: error: contour contains only one point\n");
    exit(EXIT_FAILURE);
  }

  if(DEBUG) {
    fprintf(stdout,"\nReadContour:\n\ndata points:\n============\n\n");
    for(i=0;i<=K;i++)
      fprintf(stdout,"point %d:\tx=%lf\ty=%lf\n",i,cell->contour[cellnumber][i].x,cell->contour[cellnumber][i].y);
  }
}

void CalculateEFACoefficients(Cell *cell,int cellnumber)
{
  int i,j;
  double ttotal;
  
  //Below eq.5: DeltaX, DeltaY, DeltaT
  cell->contour[cellnumber][0].t=0.;
  for(i=1;i<=cell->contourlength[cellnumber];i++) {
    cell->contour[cellnumber][i].deltax=cell->contour[cellnumber][i].x-cell->contour[cellnumber][i-1].x;
    cell->contour[cellnumber][i].deltay=cell->contour[cellnumber][i].y-cell->contour[cellnumber][i-1].y;
    cell->contour[cellnumber][i].deltat=hypot(cell->contour[cellnumber][i].deltax,cell->contour[cellnumber][i].deltay);
    cell->contour[cellnumber][i].t=cell->contour[cellnumber][i-1].t+cell->contour[cellnumber][i].deltat;
  }
  
  //Below eq.5: ttotal
  ttotal=cell->contour[cellnumber][cell->contourlength[cellnumber]].t;

  if(ttotal<locoefaerror) {
    fprintf(stderr,"CalculateEFACoefficient: error: contour of cellnumber %d has zero length\n",cellnumber);
    exit(EXIT_FAILURE);
  }

  //Below eq. 7: sumDeltaxj, sumDeltayj, xi, epsilon
  cell->contour[cellnumber][1].xi=0.;
  cell->contour[cellnumber][1].epsilon=0.;
  cell->contour[cellnumber][1].sumdeltaxj=0.;
  cell->contour[cellnumber][1].sumdeltayj=0.;
  for(i=2;i<=cell->contourlength[cellnumber];i++) {
    cell->contour[cellnumber][i].sumdeltaxj=cell->contour[cellnumber][i-1].sumdeltaxj+cell->contour[cellnumber][i-1].deltax;
    cell->contour[cellnumber][i].sumdeltayj=cell->contour[cellnumber][i-1].sumdeltayj+cell->contour[cellnumber][i-1].deltay;
    cell->contour[cellnumber][i].xi=cell->contour[cellnumber][i].sumdeltaxj-cell->contour[cellnumber][i].deltax/cell->contour[cellnumber][i].deltat*cell->contour[cellnumber][i-1].t;
    cell->contour[cellnumber][i].epsilon=cell->contour[cellnumber][i].sumdeltayj-cell->contour[cellnumber][i].deltay/cell->contour[cellnumber][i].deltat*cell->contour[cellnumber][i-1].t;
  }

  //Equation 7: alpha0, gamma0
  cell->locoefa[cellnumber][0].alpha=cell->contour[cellnumber][0].x;
  cell->locoefa[cellnumber][0].gamma=cell->contour[cellnumber][0].y;
  for(i=1;i<=cell->contourlength[cellnumber];i++) {
    cell->locoefa[cellnumber][0].alpha+=(cell->contour[cellnumber][i].deltax/(2.*cell->contour[cellnumber][i].deltat)*(cell->contour[cellnumber][i].t*cell->contour[cellnumber][i].t-cell->contour[cellnumber][i-1].t*cell->contour[cellnumber][i-1].t)+cell->contour[cellnumber][i].xi*(cell->contour[cellnumber][i].t-cell->contour[cellnumber][i-1].t))/ttotal;
    cell->locoefa[cellnumber][0].gamma+=(cell->contour[cellnumber][i].deltay/(2.*cell->contour[cellnumber][i].deltat)*(cell->contour[cellnumber][i].t*cell->contour[cellnumber][i].t-cell->contour[cellnumber][i-1].t*cell->contour[cellnumber][i-1].t)+cell->contour[cellnumber][i].epsilon*(cell->contour[cellnumber][i].t-cell->contour[cellnumber][i-1].t))/ttotal;

    //Equation 6: alpha, beta, gamma, delta
    for(j=1;j<=locoefamodes;j++) {
      cell->locoefa[cellnumber][j].alpha+=cell->contour[cellnumber][i].deltax/cell->contour[cellnumber][i].deltat*(cos(2.*(double)j*M_PI*cell->contour[cellnumber][i].t/ttotal)-cos(2*(double)j*M_PI*cell->contour[cellnumber][i-1].t/ttotal));
      cell->locoefa[cellnumber][j].beta+=cell->contour[cellnumber][i].deltax/cell->contour[cellnumber][i].deltat*(sin(2.*(double)j*M_PI*cell->contour[cellnumber][i].t/ttotal)-sin(2*(double)j*M_PI*cell->contour[cellnumber][i-1].t/ttotal));
      cell->locoefa[cellnumber][j].gamma+=cell->contour[cellnumber][i].deltay/cell->contour[cellnumber][i].deltat*(cos(2.*(double)j*M_PI*cell->contour[cellnumber][i].t/ttotal)-cos(2*(double)j*M_PI*cell->contour[cellnumber][i-1].t/ttotal));
      cell->locoefa[cellnumber][j].delta+=cell->contour[cellnumber][i].deltay/cell->contour[cellnumber][i].deltat*(sin(2.*(double)j*M_PI*cell->contour[cellnumber][i].t/ttotal)-sin(2*(double)j*M_PI*cell->contour[cellnumber][i-1].t/ttotal));
    }
  }
  for(j=1;j<=locoefamodes;j++) {
    cell->locoefa[cellnumber][j].alpha*=ttotal/(2.*(double)(j*j)*M_PI*M_PI);
    cell->locoefa[cellnumber][j].beta*=ttotal/(2.*(double)(j*j)*M_PI*M_PI);
    cell->locoefa[cellnumber][j].gamma*=ttotal/(2.*(double)(j*j)*M_PI*M_PI);
    cell->locoefa[cellnumber][j].delta*=ttotal/(2.*(double)(j*j)*M_PI*M_PI);
  }

  if(DEBUG) {
    fprintf(stdout,"\nCalculateEFACoefficients:\n\nEFA coefficients:\n=================\n\n");
    for(j=0;j<=locoefamodes;j++) {
      fprintf(stdout,"mode %d:\n",j);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][j].alpha,cell->locoefa[cellnumber][j].beta);
      fprintf(stdout,"(%g\t%g)\n\n",cell->locoefa[cellnumber][j].gamma,cell->locoefa[cellnumber][j].delta);
    }
  }
}

void CalculateLOCOCoefficients(Cell *cell,int cellnumber)
{
  int i;

  //Equation 14: tau1
  cell->locoefa[cellnumber][1].tau=0.5*atan2(2.*(cell->locoefa[cellnumber][1].alpha*cell->locoefa[cellnumber][1].beta+cell->locoefa[cellnumber][1].gamma*cell->locoefa[cellnumber][1].delta),cell->locoefa[cellnumber][1].alpha*cell->locoefa[cellnumber][1].alpha+cell->locoefa[cellnumber][1].gamma*cell->locoefa[cellnumber][1].gamma-cell->locoefa[cellnumber][1].beta*cell->locoefa[cellnumber][1].beta-cell->locoefa[cellnumber][1].delta*cell->locoefa[cellnumber][1].delta); 

  //Below eq. 15: alpha1prime, gamma1prime
  cell->locoefa[cellnumber][1].alphaprime=cell->locoefa[cellnumber][1].alpha*cos(cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][1].beta*sin(cell->locoefa[cellnumber][1].tau);
  cell->locoefa[cellnumber][1].gammaprime=cell->locoefa[cellnumber][1].gamma*cos(cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][1].delta*sin(cell->locoefa[cellnumber][1].tau);

  //Equation 16: rho
  cell->locoefa[cellnumber][1].rho=atan2(cell->locoefa[cellnumber][1].gammaprime,cell->locoefa[cellnumber][1].alphaprime);

  //Equation 17: tau1
  if(cell->locoefa[cellnumber][1].rho<0.)
    cell->locoefa[cellnumber][1].tau+=M_PI;

  //Equation 18: alphastar, betastar, gammastar, deltastar
  for(i=1;i<=locoefamodes;i++) {
    cell->locoefa[cellnumber][i].alphastar=cell->locoefa[cellnumber][i].alpha*cos((double)i*cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][i].beta*sin((double)i*cell->locoefa[cellnumber][1].tau);
    cell->locoefa[cellnumber][i].betastar=-cell->locoefa[cellnumber][i].alpha*sin((double)i*cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][i].beta*cos((double)i*cell->locoefa[cellnumber][1].tau);
    cell->locoefa[cellnumber][i].gammastar=cell->locoefa[cellnumber][i].gamma*cos((double)i*cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][i].delta*sin((double)i*cell->locoefa[cellnumber][1].tau);
    cell->locoefa[cellnumber][i].deltastar=-cell->locoefa[cellnumber][i].gamma*sin((double)i*cell->locoefa[cellnumber][1].tau)+cell->locoefa[cellnumber][i].delta*cos((double)i*cell->locoefa[cellnumber][1].tau);
  }

  //Equation 9: r
  cell->locoefa[cellnumber][1].r=cell->locoefa[cellnumber][1].alphastar*cell->locoefa[cellnumber][1].deltastar-cell->locoefa[cellnumber][1].betastar*cell->locoefa[cellnumber][1].gammastar;

  //Equation 19: betastar, deltastar
  if(cell->locoefa[cellnumber][1].r<0.) {
    for(i=1;i<=locoefamodes;i++) {
      cell->locoefa[cellnumber][i].betastar=-cell->locoefa[cellnumber][i].betastar;
      cell->locoefa[cellnumber][i].deltastar=-cell->locoefa[cellnumber][i].deltastar;
    }
  }

  //Equation 20: a, b, c, d
  cell->locoefa[cellnumber][0].a=cell->locoefa[cellnumber][0].alpha;
  cell->locoefa[cellnumber][0].c=cell->locoefa[cellnumber][0].gamma;

  for(i=1;i<=locoefamodes;i++) {
    cell->locoefa[cellnumber][i].a=cell->locoefa[cellnumber][i].alphastar;
    cell->locoefa[cellnumber][i].b=cell->locoefa[cellnumber][i].betastar;
    cell->locoefa[cellnumber][i].c=cell->locoefa[cellnumber][i].gammastar;
    cell->locoefa[cellnumber][i].d=cell->locoefa[cellnumber][i].deltastar;
    }

  if(DEBUG) {
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\nmodified EFA coefficients:\n==========================\n\n");
    for(i=0;i<=locoefamodes;i++) {
      fprintf(stdout,"mode %d:\n",i);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].a,cell->locoefa[cellnumber][i].b);
      fprintf(stdout,"(%g\t%g)\n\n",cell->locoefa[cellnumber][i].c,cell->locoefa[cellnumber][i].d);
    }
  }
  
  if(DEBUG)
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\nLambda matrices:\n================\n\n");

  for(i=1;i<=locoefamodes;i++) {
    //Equation 26: phi
    cell->locoefa[cellnumber][i].phi=0.5*atan2(2.*(cell->locoefa[cellnumber][i].a*cell->locoefa[cellnumber][i].b+cell->locoefa[cellnumber][i].c*cell->locoefa[cellnumber][i].d),cell->locoefa[cellnumber][i].a*cell->locoefa[cellnumber][i].a+cell->locoefa[cellnumber][i].c*cell->locoefa[cellnumber][i].c-cell->locoefa[cellnumber][i].b*cell->locoefa[cellnumber][i].b-cell->locoefa[cellnumber][i].d*cell->locoefa[cellnumber][i].d); 

    //Below eq. 27: aprime, bprime, cprime, dprime
    cell->locoefa[cellnumber][i].aprime=cell->locoefa[cellnumber][i].a*cos(cell->locoefa[cellnumber][i].phi)+cell->locoefa[cellnumber][i].b*sin(cell->locoefa[cellnumber][i].phi);
    cell->locoefa[cellnumber][i].bprime=-cell->locoefa[cellnumber][i].a*sin(cell->locoefa[cellnumber][i].phi)+cell->locoefa[cellnumber][i].b*cos(cell->locoefa[cellnumber][i].phi);
    cell->locoefa[cellnumber][i].cprime=cell->locoefa[cellnumber][i].c*cos(cell->locoefa[cellnumber][i].phi)+cell->locoefa[cellnumber][i].d*sin(cell->locoefa[cellnumber][i].phi);
    cell->locoefa[cellnumber][i].dprime=-cell->locoefa[cellnumber][i].c*sin(cell->locoefa[cellnumber][i].phi)+cell->locoefa[cellnumber][i].d*cos(cell->locoefa[cellnumber][i].phi);

    //Equation 27: theta
    cell->locoefa[cellnumber][i].theta=atan2(cell->locoefa[cellnumber][i].cprime,cell->locoefa[cellnumber][i].aprime);

    //Equation 25: Lambda
    cell->locoefa[cellnumber][i].lambda1=cos(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].aprime+sin(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].cprime;
    cell->locoefa[cellnumber][i].lambda12=cos(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].bprime+sin(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].dprime;
    cell->locoefa[cellnumber][i].lambda21=-sin(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].aprime+cos(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].cprime;
    cell->locoefa[cellnumber][i].lambda2=-sin(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].bprime+cos(cell->locoefa[cellnumber][i].theta)*cell->locoefa[cellnumber][i].dprime;

    if(DEBUG || fabs(cell->locoefa[cellnumber][i].lambda12)>locoefaerror || fabs(cell->locoefa[cellnumber][i].lambda21)>locoefaerror || cell->locoefa[cellnumber][i].lambda1<0 || cell->locoefa[cellnumber][i].lambda1 < fabs(cell->locoefa[cellnumber][i].lambda2)) {
      if(fabs(cell->locoefa[cellnumber][i].lambda12)>locoefaerror || fabs(cell->locoefa[cellnumber][i].lambda21)>locoefaerror)
	fprintf(stderr,"Warning: off-diagonal Lambda matrix unequal to zero:\n");
      if(cell->locoefa[cellnumber][i].lambda1<0)
	fprintf(stderr,"Warning: lambda1 negative:\n");
      if(cell->locoefa[cellnumber][i].lambda1 < fabs(cell->locoefa[cellnumber][i].lambda2))
	fprintf(stderr,"Warning: lambda1 < |lambda2|:\n");
      fprintf(stdout,"mode %d:\n",i);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].lambda1,cell->locoefa[cellnumber][i].lambda12);
      fprintf(stdout,"(%g\t%g)\n\n",cell->locoefa[cellnumber][i].lambda21,cell->locoefa[cellnumber][i].lambda2);
    }

    //Equation 32: lambdaplus, lambdaminus 
    cell->locoefa[cellnumber][i].lambdaplus=(cell->locoefa[cellnumber][i].lambda1+cell->locoefa[cellnumber][i].lambda2)/2.;
    cell->locoefa[cellnumber][i].lambdaminus=(cell->locoefa[cellnumber][i].lambda1-cell->locoefa[cellnumber][i].lambda2)/2.;

    //Below eq. 37: zetaplus, zetaminus
    cell->locoefa[cellnumber][i].zetaplus=cell->locoefa[cellnumber][i].theta-cell->locoefa[cellnumber][i].phi;
    cell->locoefa[cellnumber][i].zetaminus=-cell->locoefa[cellnumber][i].theta-cell->locoefa[cellnumber][i].phi;
  }

  //Below eq. 39: A0
  cell->locoefa[cellnumber][0].locooffseta=cell->locoefa[cellnumber][0].a;
  cell->locoefa[cellnumber][0].locooffsetc=cell->locoefa[cellnumber][0].c;

  if(DEBUG) {
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\noffset:\n===============\n\n");
    fprintf(stdout,"LOCO-EFA A0 offset:\ta=%g\tc=%g\n",cell->locoefa[cellnumber][0].locooffseta,cell->locoefa[cellnumber][0].locooffsetc);
  }

  //Below eq. 41: A+(l=0)
  cell->locoefa[cellnumber][0].locolambdaplus=cell->locoefa[cellnumber][2].lambdaplus;
  cell->locoefa[cellnumber][0].locozetaplus=cell->locoefa[cellnumber][2].zetaplus;

  //Below eq. 41: A+(l=1)
  cell->locoefa[cellnumber][1].locolambdaplus=cell->locoefa[cellnumber][1].lambdaplus;
  cell->locoefa[cellnumber][1].locozetaplus=cell->locoefa[cellnumber][1].zetaplus;

  //Below eq. 41: A+(l>1)
  for(i=2;i<=locoefamodes-1;i++) {
    cell->locoefa[cellnumber][i].locolambdaplus=cell->locoefa[cellnumber][i+1].lambdaplus;
    cell->locoefa[cellnumber][i].locozetaplus=cell->locoefa[cellnumber][i+1].zetaplus;
  }

  //Below eq. 41: A-(l>0)
  for(i=2;i<=locoefamodes+1;i++) {
    cell->locoefa[cellnumber][i].locolambdaminus=cell->locoefa[cellnumber][i-1].lambdaminus;
    cell->locoefa[cellnumber][i].locozetaminus=cell->locoefa[cellnumber][i-1].zetaminus;
  }

  if(DEBUG) {
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\nLn quadruplets:\n===============\n\n");
    for(i=0;i<=locoefamodes+1;i++) {
      fprintf(stdout,"LOCO-EFA mode %d:\tlambdaplus=%g\tlambdaminus=%g\tzetaplus=%g\tzetaminus=%g\n",i,cell->locoefa[cellnumber][i].locolambdaplus,cell->locoefa[cellnumber][i].locolambdaminus,cell->locoefa[cellnumber][i].locozetaplus,cell->locoefa[cellnumber][i].locozetaminus);
    }
  }

  //Equation 38: Lambda*Zeta
  for(i=0;i<=locoefamodes+1;i++) {
    cell->locoefa[cellnumber][i].locoaplus=cell->locoefa[cellnumber][i].locolambdaplus*cos(cell->locoefa[cellnumber][i].locozetaplus);
    cell->locoefa[cellnumber][i].locobplus=-cell->locoefa[cellnumber][i].locolambdaplus*sin(cell->locoefa[cellnumber][i].locozetaplus);
    cell->locoefa[cellnumber][i].lococplus=cell->locoefa[cellnumber][i].locolambdaplus*sin(cell->locoefa[cellnumber][i].locozetaplus);
    cell->locoefa[cellnumber][i].locodplus=cell->locoefa[cellnumber][i].locolambdaplus*cos(cell->locoefa[cellnumber][i].locozetaplus);
    cell->locoefa[cellnumber][i].locoaminus=cell->locoefa[cellnumber][i].locolambdaminus*cos(cell->locoefa[cellnumber][i].locozetaminus);
    cell->locoefa[cellnumber][i].locobminus=-cell->locoefa[cellnumber][i].locolambdaminus*sin(cell->locoefa[cellnumber][i].locozetaminus);
    cell->locoefa[cellnumber][i].lococminus=-cell->locoefa[cellnumber][i].locolambdaminus*sin(cell->locoefa[cellnumber][i].locozetaminus);
    cell->locoefa[cellnumber][i].locodminus=-cell->locoefa[cellnumber][i].locolambdaminus*cos(cell->locoefa[cellnumber][i].locozetaminus);
  }

  if(DEBUG) {
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\nLOCO coefficients:\n==================\n\n");
    for(i=0;i<=locoefamodes+1;i++) {
      fprintf(stdout,"mode %d, Aplus:\n",i);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].locoaplus,cell->locoefa[cellnumber][i].locobplus);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].lococplus,cell->locoefa[cellnumber][i].locodplus);
      fprintf(stdout,"mode %d, Aminus:\n",i);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].locoaminus,cell->locoefa[cellnumber][i].locobminus);
      fprintf(stdout,"(%g\t%g)\n",cell->locoefa[cellnumber][i].lococminus,cell->locoefa[cellnumber][i].locodminus);
    }
  }

  //Equation 47: L
  for(i=1;i<=locoefamodes+1;i++) {
    cell->locoefa[cellnumber][i].locoL=sqrt(cell->locoefa[cellnumber][i].locolambdaplus*cell->locoefa[cellnumber][i].locolambdaplus+cell->locoefa[cellnumber][i].locolambdaminus*cell->locoefa[cellnumber][i].locolambdaminus+2.*cell->locoefa[cellnumber][i].locolambdaplus*cell->locoefa[cellnumber][i].locolambdaminus*cos(cell->locoefa[cellnumber][i].locozetaplus-cell->locoefa[cellnumber][i].locozetaminus-2.*cell->locoefa[cellnumber][1].locozetaplus));
    //the next line applies area normalisation, if wanted
    if(locoefaareanormalisation==1) //area normalisaiton
      cell->locoefa[cellnumber][i].locoL/=sqrt(cell->area[cellnumber]);
    else if(locoefaareanormalisation==2) { //Calculate E (lenght of the semimajor axis of the first harmonic).
      //Kuhl-Giardina, 1982 proposed to divide by the longest axis. This is problematic when cells are very long...
      cell->locoefa[cellnumber][i].locoL/=sqrt(cell->locoefa[cellnumber][i].a*cell->locoefa[cellnumber][i].a+cell->locoefa[cellnumber][i].c*cell->locoefa[cellnumber][i].c);
    }
  }
  
  if(DEBUG) {
    fprintf(stdout,"\nCalculateLOCOCoefficients:\n\nLn scalar:\n==========\n\n");
    for(i=0;i<=locoefamodes+1;i++)
      fprintf(stdout,"LOCO-EFA mode %d:\tLn=%g\n",i,cell->locoefa[cellnumber][i].locoL);
  }
}

void ReconstructContourpoint(Cell *cell,int cellnumber,double timepoint,int type,int first,int last,double *x,double *y)
{
  int p;

  if(first<0)
    first=0;
  if(last<0)
    last=locoefamodes;
  *x=0;
  *y=0;

  if(type==EFA) {
    if(cell->contourlength[cellnumber]) { //assume timepoint is contour-based, allowing to compare individual datapoints
      timepoint/=cell->contour[cellnumber][cell->contourlength[cellnumber]].t;
    }
    
    if(first==0) {
      *x+=cell->locoefa[cellnumber][0].alpha;
      *y+=cell->locoefa[cellnumber][0].gamma;
    }
    for(p=max(1,first);p<=min(last,locoefamodes);p++) {
      *x+=cell->locoefa[cellnumber][p].alpha*cos(2.*M_PI*(double)p*timepoint)+cell->locoefa[cellnumber][p].beta*sin(2.*M_PI*(double)p*timepoint);
      *y+=cell->locoefa[cellnumber][p].gamma*cos(2.*M_PI*(double)p*timepoint)+cell->locoefa[cellnumber][p].delta*sin(2.*M_PI*(double)p*timepoint);
    }
  }
  else if(type==LOCOEFA) {
    if(cell->contourlength[cellnumber]) { //assume provided timepoint is contour-based, allowing to compare individual datapoints
      timepoint-=cell->contour[cellnumber][cell->contourlength[cellnumber]].t*cell->locoefa[cellnumber][1].tau/(2.*M_PI);
      if(cell->locoefa[cellnumber][1].r<0.)
	timepoint=-timepoint;
      timepoint/=cell->contour[cellnumber][cell->contourlength[cellnumber]].t;
    }
    if(first==0) {
      *x+=cell->locoefa[cellnumber][0].locooffseta;
      *y+=cell->locoefa[cellnumber][0].locooffsetc;
      //L=0
      *x+=cell->locoefa[cellnumber][0].locolambdaplus*(cos(cell->locoefa[cellnumber][0].locozetaplus)*cos(2.*M_PI*2.*timepoint)-sin(cell->locoefa[cellnumber][0].locozetaplus)*sin(2.*M_PI*2.*timepoint));
      *y+=cell->locoefa[cellnumber][0].locolambdaplus*(sin(cell->locoefa[cellnumber][0].locozetaplus)*cos(2.*M_PI*2.*timepoint)+cos(cell->locoefa[cellnumber][0].locozetaplus)*sin(2.*M_PI*2.*timepoint));
    }
    if(first<=1) {
      //L=1
      *x+=cell->locoefa[cellnumber][1].locolambdaplus*(cos(cell->locoefa[cellnumber][1].locozetaplus)*cos(2.*M_PI*timepoint)-sin(cell->locoefa[cellnumber][1].locozetaplus)*sin(2.*M_PI*timepoint));
      *y+=cell->locoefa[cellnumber][1].locolambdaplus*(sin(cell->locoefa[cellnumber][1].locozetaplus)*cos(2.*M_PI*timepoint)+cos(cell->locoefa[cellnumber][1].locozetaplus)*sin(2.*M_PI*timepoint));
    }
    //L=2..N,+
    for(p=max(2,first);p<=min(last,locoefamodes-1);p++) {
      *x+=cell->locoefa[cellnumber][p].locolambdaplus*(cos(cell->locoefa[cellnumber][p].locozetaplus)*cos(2.*M_PI*(double)(p+1)*timepoint)-sin(cell->locoefa[cellnumber][p].locozetaplus)*sin(2.*M_PI*(double)(p+1)*timepoint));
      *y+=cell->locoefa[cellnumber][p].locolambdaplus*(sin(cell->locoefa[cellnumber][p].locozetaplus)*cos(2.*M_PI*(double)(p+1)*timepoint)+cos(cell->locoefa[cellnumber][p].locozetaplus)*sin(2.*M_PI*(double)(p+1)*timepoint));
    }
    //L=2..N,-
    for(p=max(2,first);p<=min(last,locoefamodes+1);p++) {
      *x+=cell->locoefa[cellnumber][p].locolambdaminus*(cos(cell->locoefa[cellnumber][p].locozetaminus)*cos(2.*M_PI*(double)(p-1)*timepoint)-sin(cell->locoefa[cellnumber][p].locozetaminus)*sin(2.*M_PI*(double)(p-1)*timepoint));
      *y-=cell->locoefa[cellnumber][p].locolambdaminus*(sin(cell->locoefa[cellnumber][p].locozetaminus)*cos(2.*M_PI*(double)(p-1)*timepoint)+cos(cell->locoefa[cellnumber][p].locozetaminus)*sin(2.*M_PI*(double)(p-1)*timepoint));
    }
  }
}

void UpdateCellContour(TYPE **state,TYPE **a,Cell *cell)
{
  //determine contour of all but state 0
  const int xval[8] = { 1, 1, 0,-1,-1,-1, 0, 1};
  const int yval[8] = { 0, 1, 1, 1, 0,-1,-1,-1};
  int removed,connections;
  int ii,jj,c,perimeter;
  int direction,connected;
  int next;
  int x,y;
  int tmpboundary,tmpboundaryvalue,tmpspecialcelltype;
  int (*tmpnewindex)(int,int);
  
  for(c=0;c<cell->maxcells;c++) {
    if(cell->contour[c]!=NULL) {
      free(cell->contour[c]);
      cell->contour[c]=NULL;
      cell->contourlength[c]=0;
    }
  }

  tmpboundary=boundary;
  tmpboundaryvalue=boundaryvalue;
  tmpspecialcelltype=specialcelltype;
  tmpnewindex=newindex;
  
  boundary=FIXED;
  boundaryvalue=0;
  specialcelltype=0;
  newindex=NewIndexFixed;   

  Fill(state,0);
  Boundaries(a);
  Boundaries(state);
  Copy(state,a);
  MultV(state,state,-1);
  
  //we remove loose pixels but do not check for global connectivity, as SPM guarantees this
  do {
    removed=0;
    PLANE(
	  if(state[i][j]) {
	    connections=0;
	    NEIGHBOURS(
		       if(state[i+y][j+x]==state[i][j])
			 connections++;
		       );
	    if(connections<3) {
	      state[i][j]=0;
	      a[i][j]=0;
	      removed++;
	    }
	  }
	  );
    if(DEBUG)
      fprintf(stdout,"UpdateCellContour: %d weakly connected pixels removed\n",removed);
  } while(removed);

  UpdateCFill(a,cell);

  PLANE(
	if(state[i][j]<0) {
	  if(cell->contour[-state[i][j]]==NULL) {
	    c=state[i][j];
	    perimeter=1;
	    ii=i;
	    jj=j;
	    state[ii][jj]=perimeter;
	    direction=0;
	    connected=0;
	    do {
	      next=0;
	      do {
		x=xval[direction];
		y=yval[direction];
		if(state[newindex(ii+y,nrow)][newindex(jj+x,ncol)]==c || (state[newindex(ii+y,nrow)][newindex(jj+x,ncol)]>0 && a[newindex(ii+y,nrow)][newindex(jj+x,ncol)]==-c)) {
		  next=1;
		  ii=ii+y;
		  jj=jj+x;
		  if(state[ii][jj]<0) {
		    state[ii][jj]=++perimeter;
		  }
		  else {
		    connected=1;
		    InitLOCOEFAContour(cell,-c,perimeter);
		  }
		  direction=(direction+5)%8;
		}     
		else direction=(direction+1)%8;
	      } while(!next);
	    } while(!connected);
	  }
	  else {
	    state[i][j]=0;
	  }
	}
	);
  PLANE(
	if(state[i][j]) {
	  cell->contour[a[i][j]][state[i][j]-1].y=i;
	  cell->contour[a[i][j]][state[i][j]-1].x=j;
	  if(state[i][j]==1) {
	    cell->contour[a[i][j]][cell->contourlength[a[i][j]]].y=i;
	    cell->contour[a[i][j]][cell->contourlength[a[i][j]]].x=j;
	  }
	}
	);
  
  boundary=tmpboundary;
  boundaryvalue=tmpboundaryvalue;
  specialcelltype=tmpspecialcelltype;
  newindex=tmpnewindex;
}
