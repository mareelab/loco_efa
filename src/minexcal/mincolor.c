#include "minexcal.h"

unsigned char fixedCol[9][3] = {{0,0,0}, {255,255,255}, {255,0,0}, {0,255,0}, {0,0,255}, \
{255,255,0}, {255,0,255}, {100,100,100}, {0,255,255}};
int lastcolor=-1;
unsigned char **userCol=NULL;

extern int cellboundarycolor;

/********************************************functions*/

void hsv2rgb(FLOAT h,FLOAT s,FLOAT v,unsigned char *r,unsigned char *g,unsigned char *b) 
{
  FLOAT f;
  int i;
  unsigned char k,m,n,p;

  while(h<0.0)           /* Hue */
    h+=1.0;
  while(h>1.0)
    h-=1.0;
  s = min(1.,max(0.,s)); /* Saturation */
  v = min(1.,max(0.,v)); /* value or brightness */
  v*=255.;   //rgb between 0-255
  p = (unsigned char)v;
   
#ifdef _SMALL
   if (fabsf(s)<EPSILON)
#else
   if (fabs(s)<EPSILON)
#endif
     {
       /* Achromatic case, set level of grey */
       *r = p;
       *g = p;
       *b = p;
     } 
   else {
     /* Determine levels of primary colors */
     if (h >= 1.0) {
       h = 0.0;
     } 
     else {
       h = h * 6.;
     } 
     i = (int) h;   /* should be in the range 0..5 */
     f = h - i;     /* fractional part */
     
     m = (unsigned char)(v * (1. - s));
     n = (unsigned char)(v * (1. - s * f));
     k = (unsigned char)(v * (1. - s * (1. - f)));
     
     if (i == 0) { *r = p; *g = k; *b = m; }
     if (i == 1) { *r = n; *g = p; *b = m; }
     if (i == 2) { *r = m; *g = p; *b = k; }
     if (i == 3) { *r = m; *g = n; *b = p; }
     if (i == 4) { *r = k; *g = m; *b = p; }
     if (i == 5) { *r = p; *g = m; *b = n; }
   }
}

void rgb2hsv(unsigned char r,unsigned char g,unsigned char b,FLOAT *h,FLOAT *s,FLOAT *v) 
{
  FLOAT hh,ss,vv;
  FLOAT max_v,min_v,diff_v,r_dist,g_dist,b_dist;
  int largest=0;
  unsigned char min,max,diff;

  if(g==(max=max(r,g)))
    largest++;
  if(b==(max=max(max,b)))
    largest+=2;
  min=min(r,min(g,b));
  diff=max-min;

  max_v = (FLOAT)max;
  min_v = (FLOAT)min;
  diff_v = max_v-min_v;
  vv = max_v/255.;
  
  if((int)max==0 || (int)diff==0) {
    ss = 0.0; 
    hh = 0.0; //it is actually undefined
  }
  else {
    ss = diff_v/max_v;
    r_dist = (max_v - (FLOAT)r)/diff_v;
    g_dist = (max_v - (FLOAT)g)/diff_v;
    b_dist = (max_v - (FLOAT)b)/diff_v;
    if(largest == 0)       //r is largest 
      hh = b_dist - g_dist;
    else if (largest == 1) //g is largest
      hh = 2. + r_dist - b_dist;
    else //if (largest == 2 || largest == 3) //b is largest
      hh = 4. + g_dist - r_dist;
  }
  hh /=6.;
  if( hh < 0.)
    hh += 1.0;
  
  *h = hh;
  *s = ss;
  *v = vv;
}

void AllocateUserCol(int color)
{
  int i,j;
  static int old=-1;

  if(color<0) {
    free(userCol[0]);
    free(userCol);
    old=-1;
    return;
  }
  
  if((userCol=realloc(userCol,(size_t)((color+1)*sizeof(unsigned char*))))== NULL) {
    fprintf(stderr,"AllocateUserCol: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  if(old==-1)
    userCol[0]=NULL;
  if((userCol[0]=realloc(userCol[0],(size_t)((color+1)*4*sizeof(unsigned char))))== NULL) {
    fprintf(stderr,"AllocateUserCol: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for (i=1;i<=color;i++) {
    userCol[i] = userCol[i-1] + 4;
  }
  
  if(color>old)
    for(i=old+1;i<=color;i++)
      for(j=0;j<4;j++)
	userCol[i][j]=0;
  old=color;
  lastcolor=color;
}

int ColorTable(int s,int e, ...)
{
  va_list ap;        /* points to each unnamed arg in turn */
  int i,n,cnt,col;

  /* some checks, and now hope the user supplied enough arguments */
  if ( (s>e) || (s<0) ) {
    fprintf(stderr, "ColorTable: warning: start- or end-value out-of-range (%d, %d)\n",s,e);
    return -1;
  }

  if(e>lastcolor)
    AllocateUserCol(e);
  n = e - s + 1;
  va_start(ap,e);  /* make ap point to first unnamed arg */
  
  for(cnt=0;cnt<n;cnt++) {   /* loop over 'n' userdefined colors */
    col=va_arg(ap,int);      /* retrieve value from next unnamed arg */
    if (col<0 || col>8) {
      fprintf(stderr,"ColorTable: error: color-arg %d out-of-range (%d)\n",cnt+1,col);
      exit(EXIT_FAILURE);
    }
    userCol[s+cnt][0] = 1;
    for (i=0; i<3; i++) 
      userCol[s+cnt][i+1] = fixedCol[col][i];
  }
  va_end(ap); /* clean up */
  return s;
}

int ColorName(int s,int e, ...)
{
  va_list ap;        /* points to each unnamed arg in turn */
  int i,n,cnt,values[3],found,succes=-1;
  char *col;
  FILE *fp;
  char line[STRING],a_string[STRING];
  
  /* some checks, and now hope the user supplied enough arguments */
  if ( (s>e) || (s<0) ) {
    fprintf(stderr, "ColorName: warning: start- or end-value out-of-range (%d, %d)\n",s,e);
    return -1;
  }

  if(e>lastcolor)
    AllocateUserCol(e);
  n = e - s + 1;

  va_start(ap,e);  /* make ap point to first unnamed arg */
  
  for(cnt=0;cnt<n;cnt++) {   /* loop over 'n' userdefined colors */
    col=va_arg(ap,char*);      /* retrieve value from next unnamed arg */
    if ((fp = fopen("/usr/X11R6/lib/X11/rgb.txt","r"))==NULL) {
      if ((fp = fopen("/usr/share/X11/rgb.txt","r"))==NULL) {
        if ((fp = fopen("/usr/X11/share/X11/rgb.txt","r"))==NULL) {
	  if ((fp = fopen("/nbi/software/testing/excalib2/2/x86_64/etc/rgb.txt","r"))==NULL) {
	    fprintf(stderr,"ColorName: warning: file /usr/X11R6/lib/X11/rgb.txt, /usr/share/X11/rgb.txt, /usr/X11/share/X11/rgb.txt or /nbi/software/testing/excalib2/2/x86_64/etc/rgb.txt does not exist\n");
          return -1;
	  }
        }
      }
    }
    found=0;
    while ((found=fscanf(fp,"%d %d %d\t\t",&values[0],&values[1],&values[2])) != EOF) {
      i=0;
      while (((line[i]=(char)(getc(fp))) != EOF) && line[i]!='\n')
	i++;
      line[i]='\0';
      if (!strncmp(line,col,STRING)) {     
	userCol[s+cnt][0] = 1;
	for (i=0; i<3; i++) 
	  userCol[s+cnt][i+1] = (unsigned char)values[i];
	if(cnt==0)
	  succes=s;
	fclose(fp);
	break;
      }
    }
    if(found==EOF) {
      fclose(fp);
      fprintf(stderr,"ColorName: warning: color \"%s\" does not exist\n",col);
      fprintf(stderr,"ColorName: maybe you meant one of the following colors:\n");
      snprintf(a_string,STRING,"grep -s \"%s\" /usr/X11R6/lib/X11/rgb.txt /usr/share/X11/rgb.txt /usr/X11/share/X11/rgb.txt /nbi/software/testing/excalib2/2/x86_64/etc/rgb.txt",col);
      system(a_string);
    }
  }
  va_end(ap); /* clean up */
  return succes;
}

int ColorRamp(int s,int e,int c)
{
  int i,ii,n0,n1,n2,m0,m1,ss=1;
  double s0,s1,s2,sm0,sm1;
  int i1,i2,i3,bw;
  int n;

  n = e - s + 1;
  if (s > e) {
    ss = -1; 
    n = 2 - n;
  }
  if (s<0 || e<0 ) {
    fprintf(stderr, "ColorRamp: warning: start- or end-value out-of-range (%d, %d)\n",s,e);
    return -1;
  }
  if(s>lastcolor || e>lastcolor) 
    AllocateUserCol(max(s,e));
    
  n0 = n1 = n2 = n/3;
  m0 = m1 = n/2;
  if ((n0+n1+n2) < n) n0++;
  if ((n0+n1+n2) < n) n1++;
  if ((m0+m1) < n) m0++;
  s0 = 255./(double)n0; 
  s1 = 255./(double)n1; 
  s2 = 255./(double)n2;
  sm0= 255./(double)m0; 
  sm1= 255./(double)m1;
  
  if (c == WHITE || c == BLACK || c == GRAY) 
    bw = 1; 
  else 
    bw = 0;
  for (i=0; i < n; i++) {
    ii = s + ss*i; 
    userCol[ii][0] = 1;
  }
  if (bw) {
    for (i=1; i <= n; i++) {
      ii = s + ss*(i-1);
      if (c == WHITE) userCol[ii][1] = userCol[ii][2] = userCol[ii][3] = 255 - i*255/n;
      else userCol[ii][1] = userCol[ii][2] = userCol[ii][3] = i*255/n;
    }
  } 
  else if (c == RED || c == BLUE || c == GREEN) {
    if (c == RED) {i1=1; i2=2; i3=3;}
    else if (c == BLUE) {i1=3; i2=2; i3=1;}
    else /*if (c == GREEN)*/ {i1=2; i2=1; i3=3;}
    for (i=1; i <= n0; i++) {
      ii = s + ss*(i-1); 
      userCol[ii][i1] = i*s0; userCol[ii][i2] = 0; userCol[ii][i3] = 0;
    }
    for (i=1; i <= n1; i++) {
      ii = s + ss*(i+n0-1); 
      userCol[ii][i2] = i*s1; userCol[ii][i1] = 255; userCol[ii][i3] = 0;
    }
    for (i=1; i <= n2; i++) {
      ii = s + ss*(i+n0-1+n1);
      userCol[ii][i3] = i*s2; userCol[ii][i1] = 255; userCol[ii][i2] = 255;
    }
  }
  else if (c == YELLOW || c == CYAN || c == MAGENTA) {
    if (c == CYAN) {i1=1; i2=2; i3=3;}
    else if (c == YELLOW) {i1=3; i2=2; i3=1;}
    else /*if (c == MAGENTA)*/ {i1=2; i2=1; i3=3;}
    for (i=1; i <= m0; i++) {
      ii = s + ss*(i-1); 
      userCol[ii][i1] = 0; userCol[ii][i2] = i*sm0; userCol[ii][i3] = i*sm0;
    }
    for (i=1; i <= m1; i++) {
      ii = s + ss*(i+m0-1);
      userCol[ii][i2] = 255; userCol[ii][i3] = 255; userCol[ii][i1] = i*sm1;
    }
  }
  else {
    fprintf(stderr,"ColorRamp: warning: unknown color\n");
    return -1;
  }
  return s; 
}

int ColorRGB(int i,int r,int g,int b)
{
  if (i<0) {
    fprintf(stderr, "ColorRGB: warning: value out-of-range (%d)\n",i);
    return -1;
  }
  if(i>lastcolor)
    AllocateUserCol(i);
   
  userCol[i][0] = 1;
  userCol[i][1] = r;
  userCol[i][2] = g;
  userCol[i][3] = b;
  return i;
}

int ColorHSV(int i,FLOAT h,FLOAT s,FLOAT v)
{
  if (i<0) {
    fprintf(stderr, "ColorHSV: warning: value out-of-range (%d)\n",i);
    return -1;
  }
  if(i>lastcolor)
    AllocateUserCol(i);
   
  userCol[i][0] = 1;
  hsv2rgb(h,s,v,&userCol[i][1],&userCol[i][2],&userCol[i][3]);
  return i;
}

int ColorGrad(int s,int e)
{
  int i,ii,n,sss=1;
  FLOAT sh,dh,eh,ssh,ss,ds,es,sv,dv,ev;
  
  if (s<0 || e<0) {
    fprintf(stderr, "ColorGrad: warning: start- or end-value smaller than 0 (%d, %d)\n"
	    "probably due to an unsuccesful color allocation\n",s,e);

    return -1;
  }
  
  if(s>lastcolor || e>lastcolor) 
    AllocateUserCol(max(s,e));
  if(userCol[s][0]==0)
    ColorTable(s,s,BLACK);
  if(userCol[e][0]==0)
    ColorTable(e,e,WHITE);

  n = e - s;
  if(n<0) {
    n=-n;
    sss=-1;
  }
  if(n<2)
    return s;

  rgb2hsv(userCol[s][1],userCol[s][2],userCol[s][3],&sh,&ss,&sv);
  rgb2hsv(userCol[e][1],userCol[e][2],userCol[e][3],&eh,&es,&ev);
  //if s=0 or v=0, then h is meaningless
  if(ss<EPSILON || sv<EPSILON)
    sh=eh;
  else if(es<EPSILON || ev<EPSILON)
    eh=sh;
  dh = eh-sh;
  if(dh>0.5)
    dh-=1.;
  else if (dh<-0.5)
    dh+=1.;
  dh/=(FLOAT)n;
  ds =(es-ss)/(FLOAT)n;
  dv =(ev-sv)/(FLOAT)n;
  for (i=1; i < n; i++) {
    ii = s + sss*i;
    userCol[ii][0]=1;
    ssh=sh+(FLOAT)i*dh;
    if(ssh<0.)
      ssh+=1.;
    if(ssh>1.)
      ssh-=1.;
    hsv2rgb(ssh,ss+(FLOAT)i*ds,sv+(FLOAT)i*dv,&userCol[ii][1],&userCol[ii][2],&userCol[ii][3]);
  }
  return s;
}
