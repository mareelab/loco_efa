#include "minexcal.h"

static png_structp png_ptr;
static png_infop info_ptr;
static png_infop end_info_ptr;
static png_bytep ptr;

extern unsigned char **userCol;
extern int specialcelltype;
extern int nrow;
extern int ncol;
extern int lastcolor;
extern int boundary;
extern TYPE boundaryvalue;
extern int cellboundarycolor;
extern int (*newindex)(int,int);

/********************************************functions*/

void OpenPNG(Png *png,char *name)
{
  char command[STRING];

  if(snprintf(png->dirname,STRING,"%s",name)>=STRING) {
    fprintf(stderr,"OpenPNG: error: directory name too long: %s\n",name);
    exit(EXIT_FAILURE);   
  }

  if(snprintf(command,STRING,"%s%s","mkdir -p ",png->dirname)>=STRING) {
    fprintf(stderr,"OpenPNG: error: directory name too long: %s\n",png->dirname);
    exit(EXIT_FAILURE);   
  }

  system(command);
  png->nframes=0;
}

void PlanePNG(TYPE **a,Png *png, int c)
{
  int i,j;
  int width,offset,entry;
  char name[STRING];
  FILE *fp;
  unsigned char *data; 

  if(lastcolor<c+Max(a) || c+Min(a)<0) {
    if(lastcolor<0)
      fprintf(stderr,"PlanePNG: warning: as yet no colourmap entries have been defined\nno file saved\nuse any of the Color...() functions to define the colormap entries\n");
    else if(c+Min(a)<0)
      fprintf(stderr,"PlanePNG: warning: invalid negative colourmap entries have been requested\nno file saved\nto prevent this error, the offset (the third argument of PlanePNG) should be at least %d\n",-Min(a));
    else
      fprintf(stderr,"PlanePNG: warning: trying to use as yet undefined colormap entries\ncurrently, the largest valid colourmap entry is %d,\nwhile the largest requested colormap entry is %d\nno file saved\nuse any of the Color...() functions to define the required colormap entries\n",lastcolor,c+Max(a));
    return;
  }

  if(snprintf(name,STRING,"%s/%.5d.png",png->dirname,png->nframes)>=STRING) {
    fprintf(stderr,"PlanePNG: warning: filename too long: %s/%.5d.png\n",png->dirname,png->nframes);
    return;
  }
  
  if ((data = (unsigned char *)malloc((size_t)((3*nrow*ncol)*sizeof(unsigned char))))==NULL) {
    fprintf(stderr,"PlanePNG: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  if((fp = fopen(name,"wb"))!=NULL) {
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
			  (png_voidp)NULL,(png_error_ptr)NULL,(png_error_ptr)NULL);
    info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr,fp);
    png_set_IHDR(png_ptr,info_ptr,ncol,nrow,
	     8,PNG_COLOR_TYPE_RGB,PNG_INTERLACE_NONE,
	     PNG_COMPRESSION_TYPE_BASE,PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr,info_ptr);

    width=3*ncol;
    for (i=nrow;i>0;i--) {
      offset=(nrow-i)*width;
      for (j=1;j<=ncol;j++) {
        entry=(j-1)*3; 
        data[offset+entry] = userCol[c+a[i][j]][1];
        data[offset+entry+1] = userCol[c+a[i][j]][2];
        data[offset+entry+2] = userCol[c+a[i][j]][3];
      }
      ptr=data+offset;
      png_write_rows(png_ptr,&ptr,1);
    }

    png_write_end(png_ptr,info_ptr);
    png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
    fclose(fp);
    png->nframes++;
    free(data);
  }
  else {
    fprintf(stderr,"PlanePNG: warning: cannot open file %s\n",name);
    free(data);
  }
}

void ClosePNG(Png *png)
{
  FILE *fp;
  int nr,nc;
  char name[STRING],a_string[STRING];

  if(snprintf(name,STRING,"%s/mpeg.par",png->dirname)>=STRING) {
    fprintf(stderr,"ClosePNG: warning: filename too long: %s/mpeg.par\n",png->dirname);
    return;
  }
  
  if(snprintf(a_string,STRING,"%s/00000.png",png->dirname)>=STRING) {
    fprintf(stderr,"ClosePNG: warning: filename too long: %s/00000.png\n",png->dirname);
    return;
  }

  if(ReadSizePNG(&nr,&nc,a_string)) {
    fprintf(stderr,"ClosePNG: warning: could not open %s\nno parameter file saved\n",a_string);
    return;
  }
  
  if((fp = fopen(name,"w"))!=NULL) {
    fprintf(fp,"MPEG-1 Sequence, 25 frames/sec \n");
    fprintf(fp,"%%.5d     /* name of source files */ \n");
    fflush(fp);
    fprintf(fp,"-         /* name of reconstructed images ('-': don't store) */ \n");
    fflush(fp);
    fprintf(fp,"-         /* name of intra quant matrix file     ('-': default matrix) */  \n");
    fprintf(fp,"-         /* name of non intra quant matrix file ('-': default matrix) */ \n");
    fprintf(fp,"/dev/null /* name of statistics file ('-': stdout ) */ \n");
    fprintf(fp,"2         /* input picture file format: 0=*.Y,*.U,*.V, 1=*.yuv, 2=*.ppm */  \n");
    fprintf(fp,"%d        /* number of frames */ \n",png->nframes);
    fprintf(fp,"0         /* number of first frame */ \n");
    fprintf(fp,"00:00:00:00 /* timecode of first frame */ \n");
    fprintf(fp,"12        /* N (# of frames in GOP) */ \n");
    fprintf(fp,"1         /* M (I/P frame distance) */ \n");
    fprintf(fp,"1         /* ISO/IEC 11172-2 stream */ \n");
    fprintf(fp,"0         /* 0:frame pictures, 1:field pictures */ \n");
    fprintf(fp,"%d        /* horizontal_size */ \n",nc);
    fprintf(fp,"%d        /* vertical_size */ \n",nr);
    fprintf(fp,"1         /* aspect_ratio_information 8=CCIR601 625 line, 9=CCIR601 525 line */ \n");
    fprintf(fp,"3         /* frame_rate_code 1=23.976, 2=24, 3=25, 4=29.97, 5=30 frames/sec. */ \n");
    fprintf(fp,"1152000.0 /* bit_rate (bits/s) */ \n");
    fprintf(fp,"20        /* vbv_buffer_size (in multiples of 16 kbit) */ \n");
    fprintf(fp,"0         /* low_delay  */ \n");
    fprintf(fp,"1         /* constrained_parameters_flag */ \n");
    fprintf(fp,"4         /* Profile ID: Simple = 5, Main = 4, SNR = 3, Spatial = 2, High = 1 */ \n");
    fprintf(fp,"8         /* Level ID:   Low = 10, Main = 8, High 1440 = 6, High = 4          */ \n");
    fprintf(fp,"1         /* progressive_sequence */ \n");
    fprintf(fp,"1         /* chroma_format: 1=4:2:0, 2=4:2:2, 3=4:4:4 */ \n");
    fprintf(fp,"1         /* video_format: 0=comp., 1=PAL, 2=NTSC, 3=SECAM, 4=MAC, 5=unspec. */ \n");
    fprintf(fp,"5         /* color_primaries */ \n");
    fprintf(fp,"5         /* transfer_characteristics */ \n");
    fprintf(fp,"5         /* matrix_coefficients */ \n");
    fprintf(fp,"%d       /* display_horizontal_size */ \n",nc);
    fprintf(fp,"%d       /* display_vertical_size */ \n",nr);
    fprintf(fp,"0         /* intra_dc_precision (0: 8 bit, 1: 9 bit, 2: 10 bit, 3: 11 bit */ \n");
    fprintf(fp,"0         /* top_field_first */ \n");
    fprintf(fp,"1 1 1     /* frame_pred_frame_dct (I P B) */ \n");
    fprintf(fp,"0 0 0     /* concealment_motion_vectors (I P B) */ \n");
    fprintf(fp,"0 0 0     /* q_scale_type  (I P B) */ \n");
    fprintf(fp,"0 0 0     /* intra_vlc_format (I P B)*/ \n");
    fprintf(fp,"0 0 0     /* alternate_scan (I P B) */ \n");
    fprintf(fp,"0         /* repeat_first_field */ \n");
    fprintf(fp,"1         /* progressive_frame */ \n");
    fprintf(fp,"0         /* P distance between complete intra slice refresh */ \n");
    fprintf(fp,"0         /* rate control: r (reaction parameter) */ \n");
    fprintf(fp,"0         /* rate control: avg_act (initial average activity) */ \n");
    fprintf(fp,"0         /* rate control: Xi (initial I frame global complexity measure) */ \n");
    fprintf(fp,"0         /* rate control: Xp (initial P frame global complexity measure) */ \n");
    fprintf(fp,"0         /* rate control: Xb (initial B frame global complexity measure) */ \n");
    fprintf(fp,"0         /* rate control: d0i (initial I frame virtual buffer fullness) */ \n");
    fprintf(fp,"0         /* rate control: d0p (initial P frame virtual buffer fullness) */ \n");
    fprintf(fp,"0         /* rate control: d0b (initial B frame virtual buffer fullness) */ \n");
    fprintf(fp,"2 2 11 11 /* P:  forw_hor_f_code forw_vert_f_code search_width/height */ \n");
    fprintf(fp,"1 1 3  3  /* B1: forw_hor_f_code forw_vert_f_code search_width/height */ \n");
    fprintf(fp,"1 1 7  7  /* B1: back_hor_f_code back_vert_f_code search_width/height */ \n");
    fprintf(fp,"1 1 7  7  /* B2: forw_hor_f_code forw_vert_f_code search_width/height */ \n");
    fprintf(fp,"1 1 3  3  /* B2: back_hor_f_code back_vert_f_code search_width/height */ \n");
    fprintf(fp," \n");
    fclose(fp);
  }
  else
    fprintf(stderr,"ClosePNG: warning: could not open file %s\n",name);
}

void CellPNG(TYPE **a,Cell *cell, Png *png, int c)
{
  int i,j,k,m;
  int width,offset,entry;
  int minval,maxval;
  char name[STRING];
  FILE *fp;
  unsigned char *data; 

  if(lastcolor<cellboundarycolor || cellboundarycolor<0) {
    fprintf(stderr,"CellPNG: warning: the cellboundarycolor is as yet undefined\nno file saved\nuse any of the Color...() functions to define the cellboundarycolor colormap entry (i.e. entry #%d)\n",cellboundarycolor);
    return;
  }
  
  minval=cell->celltype[(a[0][0])];
  maxval=cell->celltype[(a[0][0])];
  PLANE(
        if(cell->celltype[(a[i][j])]>maxval)
          maxval=cell->celltype[(a[i][j])];
        else if(cell->celltype[(a[i][j])]<minval)
          minval=cell->celltype[(a[i][j])];
        );

  if(lastcolor<c+maxval || c+minval<0) {
    if(lastcolor<0)
      fprintf(stderr,"CellPNG: warning: as yet no colourmap entries have been defined\nno file saved\nuse any of the Color...() functions to define the colormap entries\n");
    else if(c+minval<0)
      fprintf(stderr,"CellPNG: warning: invalid negative colourmap entries have been requested\nno file saved\nto prevent this error, the offset (the fourth argument of CellPNG) should be at least %d\n",-minval);
    else
      fprintf(stderr,"CellPNG: warning: trying to use as yet undefined colormap entries\ncurrently, the largest valid colourmap entry is %d,\nwhile the largest requested colormap entry is %d\nno file saved\nuse any of the Color...() functions to define the required colormap entries\n",lastcolor,c+maxval);
    return;
  }
 
  if(snprintf(name,STRING,"%s/%.5d.png",png->dirname,png->nframes)>=STRING) {
    fprintf(stderr,"CellPNG: warning: filename too long: %s/%.5d.png\n",png->dirname,png->nframes);
    return;
  }

  if ((data = (unsigned char *)malloc((size_t)((12*nrow*ncol)*sizeof(unsigned char))))==NULL) {
    fprintf(stderr,"CellPNG: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  
  if ((fp = fopen(name,"wb"))!=NULL) {
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
			  (png_voidp)NULL,(png_error_ptr)NULL,(png_error_ptr)NULL);
    info_ptr = png_create_info_struct (png_ptr);
    png_init_io(png_ptr,fp);
    png_set_IHDR(png_ptr, info_ptr,2*ncol,2*nrow,
	     8,PNG_COLOR_TYPE_RGB,PNG_INTERLACE_NONE,
	     PNG_COMPRESSION_TYPE_BASE,PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr,info_ptr);
    
    Boundaries(a);
    width=6*ncol;
    for (i=nrow;i>0;i--)
      for (k=0;k<2;k++) {
        offset=(2*(nrow-i)+k)*width;
        for (j=1;j<=ncol;j++)
          for (m=0;m<2;m++) {
	entry=(2*(j-1)+m)*3; 
	if((!k && a[i][j]!=a[i+1][j])||(m && a[i][j]!=a[i][j+1])||(!k && m && a[i][j]!=a[i+1][j+1])) {
	  data[offset+entry] = userCol[cellboundarycolor][1];
	  data[offset+entry+1] = userCol[cellboundarycolor][2];
	  data[offset+entry+2] = userCol[cellboundarycolor][3];
	}
	else {
	  data[offset+entry] = userCol[(c+cell->celltype[(a[i][j])])][1];
	  data[offset+entry+1] = userCol[(c+cell->celltype[(a[i][j])])][2];
	  data[offset+entry+2] = userCol[(c+cell->celltype[(a[i][j])])][3];
	}
          }
        ptr = data+offset;
        png_write_rows(png_ptr,&ptr,1);
      }

    png_write_end(png_ptr,info_ptr);
    png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
    fclose(fp);
    png->nframes++;
    free(data);
  }
  else {
    fprintf(stderr,"CellPNG: warning: cannot open file %s\n",name);
    free(data);
  }
}

void CFPlanePNG(FLOAT **a,TYPE **b, Png *png, FLOAT low, FLOAT high, int start, int end)
{
  int i,j,k,m,c;
  int width,offset,entry;
  char name[STRING];
  FILE *fp;
  double difference;
  unsigned char *data; 

  if(lastcolor<start || lastcolor<end || start<0 || end<0) {
    if(start<0 || end<0)
      fprintf(stderr,"CFPlanePNG: warning: invalid negative colourmap entries have been requested\nno file saved\n(start=%d [argument 6];end=%d [argument 7])\n",start,end);
    else
      fprintf(stderr,"CFPlanePNG: warning: trying to use as yet undefined colormap entries\ncurrently, the largest valid colourmap entry is %d,\nwhile the largest requested colormap entry is %d\nno file saved\n(start=%d [argument 6];end=%d [argument 7])\nuse any of the Color...() functions to define the required colormap entries\n",lastcolor,max(start,end),start,end);
    return;
  }
  
  if(snprintf(name,STRING,"%s/%.5d.png",png->dirname,png->nframes)>=STRING) {
    fprintf(stderr,"CFPlanePNG: warning: filename too long: %s/%.5d.png\n",png->dirname,png->nframes);
    return;
  }

  if ((data = (unsigned char *)malloc((size_t)((12*nrow*ncol)*sizeof(unsigned char))))==NULL) {
    fprintf(stderr,"CFPlanePNG: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  difference=(double)high-(double)low;
  if(fabs(difference)<DBL_EPSILON)
    difference=DBL_EPSILON;
  
  if ((fp = fopen(name,"wb"))!=NULL) {
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
			  (png_voidp)NULL,(png_error_ptr)NULL,(png_error_ptr)NULL);
    info_ptr = png_create_info_struct (png_ptr);
    png_init_io(png_ptr,fp);
    png_set_IHDR(png_ptr, info_ptr,2*ncol,2*nrow,
	     8,PNG_COLOR_TYPE_RGB,PNG_INTERLACE_NONE,
	     PNG_COMPRESSION_TYPE_BASE,PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr,info_ptr);
    
    Boundaries(b);
    width=6*ncol;
    for (i=nrow;i>0;i--)
      for (k=0;k<2;k++) {
        offset=(2*(nrow-i)+k)*width;
        for (j=1;j<=ncol;j++)
          for (m=0;m<2;m++) {
	entry=(2*(j-1)+m)*3; 
	if((!k && b[i][j]!=b[i+1][j])||(m && b[i][j]!=b[i][j+1])||(!k && m && b[i][j]!=b[i+1][j+1])) {
	  data[offset+entry] = userCol[cellboundarycolor][1];
	  data[offset+entry+1] = userCol[cellboundarycolor][2];
	  data[offset+entry+2] = userCol[cellboundarycolor][3];
	}
	else {
	  c=start+(end-start)*(a[i][j]-low)/difference+0.5;
	  if(c<start)
	    c=start;
	  else if(c>end)
	    c=end;
	  data[offset+entry] = userCol[c][1];
	  data[offset+entry+1] = userCol[c][2];
	  data[offset+entry+2] = userCol[c][3];
	}
          }
        ptr = data+offset;
        png_write_rows(png_ptr,&ptr,1);
      }

    png_write_end(png_ptr,info_ptr);
    png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
    fclose(fp);
    png->nframes++;
    free(data);
  }
  else {
    fprintf(stderr,"CFPlanePNG: warning: cannot open file %s\n",name);
    free(data);
  }
}

int ReadSizePNG(int *h,int *w,char *filename)
{
  FILE *fp;
  png_uint_32 width,height;
  int interlace_type, compression_type, filter_type;
  int bit_depth, color_type;

  *w=0;
  *h=0;
  if((fp = fopen(filename,"rb"))!=NULL) {
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
			 (png_voidp)NULL, (png_error_ptr)NULL, (png_error_ptr)NULL);
    if(png_ptr==NULL)
      return 1;
    info_ptr = png_create_info_struct(png_ptr);
    end_info_ptr = png_create_info_struct(png_ptr);
    if(info_ptr==NULL)
      return 1;

    png_init_io(png_ptr, fp);
    png_read_info(png_ptr, info_ptr);
    if(!png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
	         &color_type, &interlace_type, &compression_type, &filter_type))
      return 1;
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info_ptr);
    fclose(fp);
    *w=(int)width;
    *h=(int)height;
    return 0;
  }
  else
    return 1;
}

TYPE **ReadPatPNG(TYPE **a,int destrow,int destcol,char *filename,int offset)
{
  FILE *fp;
  png_uint_32 width,height;
  int interlace_type, compression_type, filter_type;
  int bit_depth, color_type;
  int i,j,found,color;
  unsigned char *row_buf;

  if((fp = fopen(filename,"rb"))!=NULL) {
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
			 (png_voidp)NULL, (png_error_ptr)NULL, (png_error_ptr)NULL);
    info_ptr = png_create_info_struct (png_ptr);
    end_info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, fp);
    png_read_info(png_ptr, info_ptr);
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
	     &color_type, &interlace_type, &compression_type, &filter_type);
    if((row_buf = (unsigned char *) malloc (3*width*height*sizeof(unsigned char)))==NULL) {
      fclose(fp);
      fprintf(stderr,"ReadPatPNG: warning: cannot allocate memory\n");
      return a;
    }
    if(bit_depth == 16)
      png_set_strip_16(png_ptr);
    if (color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_expand(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
      png_set_expand(png_ptr);
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
      png_set_expand(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY) 
      png_set_gray_to_rgb(png_ptr);
    if (color_type & PNG_COLOR_MASK_ALPHA)
      png_set_strip_alpha(png_ptr);

    for (j = 0; j < height; j++) {
      ptr = (row_buf + 3*(height-1-j)*width); 
      png_read_rows(png_ptr, (png_bytepp)&ptr, (png_bytepp)NULL, 1);
    }
    
    for(i=0;i<height;i++)
      for(j=0;j<width;j++) {
        found=0;
        for(color=offset;color<=lastcolor;color++)
          {
	if(userCol[color][0] && (row_buf[(3*(i*width+j))]==userCol[color][1]) && (row_buf[(3*(i*width+j)+1)]==userCol[color][2]) && (row_buf[(3*(i*width+j)+2)]==userCol[color][3])) {
	  found=1;
	  AsynCopy(a,destrow+i,destcol+j,color-offset);
	  break;
	}
          }
        if(!found) {
          color=offset;
          while(color<=lastcolor && userCol[color][0])
	color++;
          ColorRGB(color,(int)row_buf[(3*(i*width+j))],(int)row_buf[(3*(i*width+j)+1)],(int)row_buf[(3*(i*width+j)+2)]);
          AsynCopy(a,destrow+i,destcol+j,color-offset);
        }
      }
    png_read_end(png_ptr, end_info_ptr);
    png_free(png_ptr, row_buf);
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info_ptr);
    fclose(fp);
  }
  
  else
    fprintf(stderr,"ReadPatPNG: warning: cannot open file %s\n",filename);
  
  return(a);
}    

TYPE **ReadPNG(TYPE **a,int destrow,int destcol,char *filename)
{
  FILE *fp;
  png_uint_32 width,height;
  int interlace_type, compression_type, filter_type;
  int bit_depth, color_type;
  int i,j,bytes=3;
  unsigned char *row_buf;

  if((fp = fopen(filename,"rb"))!=NULL) {
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
			 (png_voidp)NULL, (png_error_ptr)NULL, (png_error_ptr)NULL);
    info_ptr = png_create_info_struct (png_ptr);
    end_info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, fp);
    png_read_info(png_ptr, info_ptr);
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
	     &color_type, &interlace_type, &compression_type, &filter_type);
 
    //printf("color_type %d bit %d %d %d %d %d %d\n",color_type,bit_depth,PNG_COLOR_TYPE_RGB,PNG_COLOR_TYPE_GRAY,PNG_COLOR_TYPE_GRAY_ALPHA,PNG_COLOR_TYPE_PALETTE,PNG_COLOR_MASK_ALPHA);

    if (color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_expand(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
      png_set_expand(png_ptr);
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
      png_set_expand(png_ptr);
    
    if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
      bytes=1;
      if(bit_depth == 16)
	bytes*=2;
    }
    else if(bit_depth == 16)
      png_set_strip_16(png_ptr);
    
    if (color_type & PNG_COLOR_MASK_ALPHA)
      png_set_strip_alpha(png_ptr);
    
    if((row_buf = (unsigned char *) malloc (bytes*width*height*sizeof(unsigned char)))==NULL) {
      fclose(fp);
      fprintf(stderr,"ReadPNG: warning: cannot allocate memory\n");
      return a;
    }
    
    for (j = 0; j < height; j++) {
      ptr = (row_buf + bytes*(height-1-j)*width); 
      png_read_rows(png_ptr, (png_bytepp)&ptr, (png_bytepp)NULL, 1);
    }
    
    if(color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
      if(bit_depth == 16) {
	for(i=0;i<height;i++)
	  for(j=0;j<width;j++)
	    AsynCopy(a,destrow+i,destcol+j,256*(int)row_buf[(bytes*(i*width+j))]+(int)row_buf[(bytes*(i*width+j)+1)]);
      }
      else {
	for(i=0;i<height;i++)
	  for(j=0;j<width;j++)
	    AsynCopy(a,destrow+i,destcol+j,(int)row_buf[(bytes*(i*width+j))]);
      }
    }
    else {
      for(i=0;i<height;i++)
	for(j=0;j<width;j++)
	  AsynCopy(a,destrow+i,destcol+j,65536*(int)row_buf[(bytes*(i*width+j))]+256*(int)row_buf[(bytes*(i*width+j)+1)]+(int)row_buf[(bytes*(i*width+j)+2)]);
    }

    png_read_end(png_ptr, end_info_ptr);
    png_free(png_ptr, row_buf);
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info_ptr);
    fclose(fp);
  }
  
  else
    fprintf(stderr,"ReadPNG: warning: cannot open file %s\n",filename);
  
  return(a);
}    
