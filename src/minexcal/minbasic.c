#include "minexcal.h"

int seed=55;
int nrow=100;
int ncol=100;
int boundary=0;//WRAP
TYPE boundaryvalue=0;
int scale=1;
int linewidth=1;
int cellboundarycolor=255; 
double locoefaerror=1e-13;
int locoefamodes=50;
int locoefaareanormalisation=0;

Position neighbours[9]={{1,0},{1,1},{-1,1},{-1,0},{-1,-1},{1,-1},{0,1},{0,-1},{0,0}};

int seedset=0;

extern int specialcelltype;
extern int targetarea;
extern int targetperimeter;
extern int perimeterconstraint;
extern int hexperimeterconstraint;
extern int grace;
extern double graceborder;
extern double dissipation;
extern double zerotemperature;
extern double temperature;
extern double dhcutoff;

/********************************************functions*/

int InDat(char *filename,char *format,char *text,void *value)
{
  char line[STRING];
  FILE *fp;
  int c;
  
  fp = fopen(filename,"r");
  if (fp == NULL) {
    fprintf(stderr,"ReadOptions/InDat: warning: file %s does not exist\n",filename);
    return 0;
  }
 
  while (fscanf(fp,"%s",line) != EOF) {
    if (!strncmp(line,text,STRING)) {
      c=fscanf(fp,format,value);
      fclose(fp);
      if(c==1)
	return 1;
      else
	return 0;
    }
    while (((c=getc(fp)) != EOF) && c!='\n');
  }
  fclose(fp);
  return 0;
}

int SaveOptions(char *inputfile,char *outputfile)
{
  FILE *fp,*pfp;
  char a_string[STRING],a_string2[STRING],another_string[STRING];
  int i;
  /* check if the inputfile exists. */
  if (access(inputfile, F_OK) != 0) {
    fprintf(stderr,"SaveOptions: warning: file %s does not exist;"
	    " command ignored\n",inputfile);
    return 1;
  }

  /* check if the outputfile exists. */
  if (access(outputfile, F_OK) == 0) {
    fprintf(stderr,"SaveOptions: warning: file %s does already exist\n"
	    "this file will be moved to %s.bak\n",outputfile,outputfile);
    snprintf(a_string,STRING,"mv -f %s %s.bak",outputfile,outputfile);
    system(a_string);
  }

  /* get how the file has been called */
#if defined(__APPLE__)
  snprintf(another_string,STRING,"ps -www | egrep %d | grep -v egrep", getpid());
#else
  snprintf(another_string,STRING,"ps -efwww | egrep %d | grep -v egrep", getpid());
#endif
  if((pfp = popen(another_string, "r"))!=NULL) {
#if defined(__APPLE__)
    for(i=0;i<3;i++)
#else
    for(i=0;i<7;i++)
#endif
      fscanf(pfp,"%*s ");
    fgets(a_string2, (int)sizeof(a_string2), pfp);
    pclose(pfp);
    i=0;
    while(a_string2[i]!='\n' && a_string2[i]!='\0')
      i++;
    a_string2[i]='\0';
  }

  /* make the parfile */
  snprintf(a_string,STRING,"%1.2f",VERSION);
  if((fp=fopen(outputfile,"w"))==NULL) {
    fprintf(stderr,"SaveOptions: warning: could not open file %s\n",outputfile);
    return 1;
  }
  fprintf(fp,"/* %s */\n",a_string2);
  if(!(InDat(inputfile,"%s","version",another_string) && !strcmp(a_string,another_string))) {
    fprintf(fp,"version %s\n",a_string);
  }
  fclose(fp);

  if(snprintf(a_string,STRING,"cat %s >> %s",inputfile,outputfile)>=STRING)
    fprintf(stderr,"SaveOptions: warning: filenames too long\n");
  else
    system(a_string);
  return 0;
}

int ReadOptions(char *filename)
{
  char floatconversion[10];
  char intconversion[10];
  FILE *fp;

  fp = fopen(filename,"r");
  if (fp == NULL) {
    fprintf(stderr,"ReadOptions: warning: file %s does not exist\n",filename);
    return 0;
  }
  else 
    fclose(fp);

#ifdef _SMALL
  snprintf(intconversion,10,"%%hd");
  snprintf(floatconversion,10,"%%f");
#else
  snprintf(intconversion,10,"%%d");
  snprintf(floatconversion,10,"%%lf");
#endif
  InDat(filename,"%d","nrow",&nrow);
  InDat(filename,"%d","ncol",&ncol);
  InDat(filename,"%d","boundary",&boundary);
  InDat(filename,intconversion,"boundaryvalue",&boundaryvalue);
  InDat(filename,"%d","scale",&scale);
  InDat(filename,"%d","seed",&seed);
  InDat(filename,"%d","specialcelltype",&specialcelltype);
  InDat(filename,"%d","targetarea",&targetarea);
  InDat(filename,"%d","targetperimeter",&targetperimeter);
  InDat(filename,"%d","grace",&grace);
  InDat(filename,"%d","linewidth",&linewidth);
  InDat(filename,"%d","perimeterconstraint",&perimeterconstraint);
  InDat(filename,"%d","hexperimeterconstraint",&hexperimeterconstraint);
  InDat(filename,"%d","cellboundarycolor",&cellboundarycolor);
  InDat(filename,"%lf","dissipation",&dissipation);
  InDat(filename,"%lf","zerotemperature",&zerotemperature);
  InDat(filename,"%lf","temperature",&temperature);
  InDat(filename,"%lf","dhcutoff",&dhcutoff);
  InDat(filename,"%lf","graceborder",&graceborder);
  InDat(filename,"%lf","locoefaerror",&locoefaerror);
  InDat(filename,"%d","locoefamodes",&locoefamodes);
  InDat(filename,"%d","locoefaareanormalisation",&locoefaareanormalisation);
  
  if (!seedset) {
    SEED(seed);
    seedset = 1;
  }
  return 1;
}

TYPE **NewP()
{
  TYPE **a;
  a = (TYPE **)calloc((size_t)(nrow+2), sizeof(TYPE *));
  if (a == NULL) {
    fprintf(stderr,"NewP: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  return a;
}

TYPE **New()
{
  TYPE **a;
  int i,j;
  a = NewP(); 
  a[0] = (TYPE *)calloc((size_t)((nrow+2)*(ncol+2)),sizeof(TYPE));
  if (a[0] == NULL) {
    fprintf(stderr,"New: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for (i=1,j=nrow+2; i < j; i++)
    a[i] = a[i-1] + ncol + 2;
  return a;
}

TYPE **NewPlane(int row,int col)
{
  TYPE **a;
  int i,j;

  a = (TYPE **)calloc((size_t)(row+2),sizeof(TYPE *));
  if (a == NULL) {
    fprintf(stderr,"NewPlane: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  a[0] = (TYPE *)calloc((size_t)((row+2)*(col+2)),sizeof(TYPE));
  if (a[0] == NULL) {
    fprintf(stderr,"NewPlane: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for (i=1,j=row+2; i < j; i++)
    a[i] = a[i-1] + col + 2;
  
  return a;
}

TYPE **Fill(TYPE **a,TYPE c)
{
  PLANE(
	a[i][j] = c;
	);
  return a;
}

long Total(TYPE **a)
{
  int i,j;
  long k=0;
  int nc, nr;
  nr = nrow;
  nc = ncol;
  for (i=1; i <= nr; i++)
    for (j=1; j <= nc; j++)
    k += a[i][j];
  return k;
}

int Max(TYPE **a)
{
  int i,j;
  TYPE k;
  int nc, nr;
  nr = nrow;
  nc = ncol;
  k=a[1][1];
  for (i=1; i <= nr; i++)
    for (j=1; j <= nc; j++)
    if (a[i][j] > k) k = a[i][j];
  return (int)k;
}

int Min(TYPE **a)
{
  int i,j;
  TYPE k;
  int nc, nr;
  nr = nrow;
  nc = ncol;
  k=a[1][1];
  for (i=1; i <= nr; i++)
    for (j=1; j <= nc; j++)
    if (a[i][j] < k) k = a[i][j];
  return (int)k;
}

TYPE **Copy(TYPE **a,TYPE **b)
{
  if(a==b)
    return a;

   PLANE(
	 a[i][j] = b[i][j];
	 );
  return a;
}

TYPE AsynCopy(TYPE **a,int i,int j,TYPE c)
{
  int n=0;
  int ii,jj;
  
  switch (boundary) {
  case FIXED:
    if(i<=nrow && i>0 && j<=ncol && j>0)
      a[i][j]=c;
    break;
  case WRAP:
    ii=NewIndexWrap(i,nrow);
    jj=NewIndexWrap(j,ncol);
    a[ii][jj]=c;
    if(ii==1) {
      a[nrow+1][jj] = a[1][jj];
      n++;
    }
    else if(ii==nrow) {
      a[0][jj] = a[nrow][jj];
      n++;
    }
    if(jj==1) {
      a[ii][ncol+1] = a[ii][1];
      n++;
    }
    else if(jj==ncol) {
      a[ii][0] = a[ii][ncol];
      n++;
    }
    if(n==2) {
      a[0][0] = a[nrow][ncol];
      a[nrow+1][ncol+1] = a[1][1];
      a[0][ncol+1] = a[nrow][1];
      a[nrow+1][0] = a[1][ncol];
    }
    break;
  case ECHO:
    ii=NewIndexEcho(i,nrow);
    jj=NewIndexEcho(j,ncol);
    a[ii][jj]=c;
    if(i==1) {
      a[0][jj] = a[1][jj];
      n++;
    }
    else if(ii==nrow) {
      a[nrow+1][jj] = a[nrow][jj];
      n++;
    }
    if(jj==1) {
      a[ii][0] = a[ii][1];
      n++;
    }
    else if(jj==ncol) {
      a[ii][ncol+1] = a[ii][ncol];
      n++;
    }
    if(n==2) {
      a[0][0] = a[1][1];
      a[nrow+1][ncol+1] = a[nrow][ncol];
      a[0][ncol+1] = a[1][ncol];
      a[nrow+1][0] = a[nrow][1];
    }
    break;
  default:
    break;
  }
  return c;
}

FLOAT **FNewP()
{
  FLOAT **a;
  a = (FLOAT **)calloc((size_t)(nrow+2), sizeof(FLOAT *));
  if (a == NULL) {
    fprintf(stderr,"FNewP: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  return a;
}

FLOAT **FNew()
{
  FLOAT **a;
  int i,j;
  a = FNewP(); 
  a[0] = (FLOAT *)calloc((size_t)((nrow+2)*(ncol+2)),sizeof(FLOAT));
  if (a[0] == NULL) {
    fprintf(stderr,"FNew: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for (i=1,j=nrow+2; i < j; i++)
    a[i] = a[i-1] + ncol + 2;
  return a;
}
