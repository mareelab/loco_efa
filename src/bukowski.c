#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <png.h>
#include <string.h>

#define BVERSION "0.2"

#define BLACK	0
#define WHITE	1
#define RED	2
#define GREEN	3
#define	BLUE	4
#define YELLOW	5
#define MAGENTA 6
#define GRAY	7
#define GREY	7
#define CYAN	8
#define MAXCOLOR 9
#define STRING 600
#define UR                0
#define UL                1
#define LL                2
#define LR                3
#define	max(a,b)	((a) > (b) ? (a) : (b))
#define	min(a,b)	((a) > (b) ? (b) : (a))

char colortable[STRING];
char dir[STRING];
char namein[STRING]="tmp.png";
char nameout[STRING]="tmp_bar.png";
int x_off=5;
int y_off=5;
int width=40;
int height=10;
int direction=LR;
int color=BLACK;
int _x1;
int _x2;
int _y1;
int _y2;
unsigned char rgb[3];
int usergb=0;
int fixedCol[9][3] = {{0,0,0}, {255,255,255}, {255,0,0}, {0,255,0}, {0,0,255}, \
{255,255,0}, {255,0,255}, {100,100,100}, {0,255,255}};

static char *short_options = "X:Y:x:y:c:i:o:d:r:g:b:hv";
static struct option long_options[] = { \
    {"width",           1, NULL, 'X'}, \
    {"height",          1, NULL, 'Y'}, \
    {"x_offset",        1, NULL, 'x'}, \
    {"y_offset",      	1, NULL, 'y'}, \
    {"color",		1, NULL, 'c'}, \
    {"inputfile",       1, NULL, 'i'}, \
    {"outputfile",    	1, NULL, 'o'}, \
    {"direction",	1, NULL, 'd'}, \
    {"red",     	1, NULL, 'r'}, \
    {"green",     	1, NULL, 'g'}, \
    {"blue",     	1, NULL, 'b'}, \
    {"help",    	0, NULL, 'h'}, \
    {"version",	        0, NULL, 'v'}, \
    {0,			0, NULL, 0} \
};

/********************************************functions*/

void Usage(char *name, int exit_status)
{
  fprintf(stderr,"bukowski (add bars to png pictures) v%s\n"
	  "Usage: %s [options]\n"
	  //	  "See 'man bukowski' or type '%s --help' for options.\n",
	  "type '%s --help' for options.\n",
        BVERSION, name, name);
  exit(exit_status);
}

int CheckAtoi(const char *s)
{
  char *endptr;
  int num = strtol(s, &endptr, 0);
  
  if (endptr == s || *endptr != '\0') {
    fprintf(stderr,"Error: %s is not a valid number\n\n", s);
    Usage("bukowski",1);
  }
  return num;
}

void ShowHelp(char *name, int exit_status)
{
  fprintf(stderr,"bukowski (add bars to png pictures) v%s\n"
	  "Usage: %s [options]\n",
	  BVERSION, name);
  fprintf(stderr,
          "Options:\n"
	  "    --help, -h             This help screen\n"
          "    --width, -X x          width of bar (default=%d)\n"
          "    --height, -Y y         height of bar (default=%d)\n"
          "    --x_offset, -x x       x-offset from corner (default=%d)\n"
          "    --y_offset, -y x       y-offset from corner (default=%d)\n"
          "    --color, -c x          color of bar (default=BLACK)\n"
	  "    --inputfile, -i x      file which needs a bar (default=%s)\n"
	  "    --outputfile, -o x     file which will have the bar (default=%s)\n"
          "    --direction, -d x      corner in which the bar will be (default=lr)\n"
          "                           the value should be ul, ur, ll or lr\n"
          "                           (upper-left, upper-right etc.)\n"
          "    --red, -r x            red-value (e.g. 240) (default=0)\n"
          "    --green, -g x          green-value (e.g. 255) (default=0)\n"
          "    --blue, -b x           blue-value (e.g. 10) (default=0)\n"
          "    --version, -v          Print version information and exit\n"
	  "\n",width,height,x_off,y_off,namein,nameout);
  exit(exit_status);
}

void ReadArg(int argc, char **argv)
{
  int long_index;
  int c,cnt;
  
  while ((c = getopt_long(argc, argv, short_options,
			  long_options, &long_index)) != -1) {
    switch(c) {
    case 'h': 
      ShowHelp(argv[0], 0); 
      break;
    case 'v': 
      fprintf(stderr,"bukowski (add bars to png pictures) v%s\n", BVERSION);
      exit(EXIT_SUCCESS);
      break;
    case 'X': 
      width = CheckAtoi(optarg);
      if(width<0) {
	fprintf(stderr,"Error: width is smaller than zero\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'Y': 
      height = CheckAtoi(optarg);
      if(height<0) {
	fprintf(stderr,"Error: height is smaller than zero\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'y': 
      y_off = CheckAtoi(optarg);
      if(y_off<0) {
	fprintf(stderr,"Error: y-offset is smaller than zero\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'x': 
      x_off = CheckAtoi(optarg);
      if(x_off<0) {
	fprintf(stderr,"Error: x-offset is smaller than zero\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'd':
      snprintf(dir,STRING,"%s",optarg);
      if (!strcmp(dir,"ul"))
	direction=UL;
      else if (!strcmp(dir,"ll"))
	direction=LL;
      else if (!strcmp(dir,"ur"))
	direction=UR;
      else if (!strcmp(dir,"lr"))
	direction=LR;
      else {
	fprintf(stderr,"Error: undefined direction\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'c':
      snprintf(colortable,STRING,"%s",optarg);
      if (!strcmp(colortable,"BLACK"))
	color=BLACK;
      else if (!strcmp(colortable,"WHITE"))
	color=WHITE;
      else if (!strcmp(colortable,"RED"))
	color=RED;
      else if (!strcmp(colortable,"GREEN"))
	color=GREEN;
      else if (!strcmp(colortable,"BLUE"))
	color=BLUE;
      else if (!strcmp(colortable,"YELLOW"))
	color=YELLOW;
      else if (!strcmp(colortable,"MAGENTA"))
	color=MAGENTA;
      else if (!strcmp(colortable,"GRAY"))
	color=GRAY;
      else if (!strcmp(colortable,"GREY"))
	color=GREY;
      else if (!strcmp(colortable,"CYAN"))
	color=CYAN;
      else {
	fprintf(stderr,"Error: undefined color\n\n");
	Usage(argv[0],1);
      }
      break;
    case 'i':
      snprintf(namein,STRING,"%s",optarg);
      break;
    case 'o':
      snprintf(nameout,STRING,"%s",optarg);
      break;
    case 'r': 
      rgb[0] = (unsigned char)CheckAtoi(optarg);
      usergb=1;
      break;
    case 'g': 
      rgb[1] = (unsigned char)CheckAtoi(optarg);
      usergb=1;
      break;
    case 'b': 
      rgb[2] = (unsigned char)CheckAtoi(optarg);
      usergb=1;
      break;
    default:
      Usage(argv[0], 1);
      break;
    }
  }
  if((cnt = argc - optind) != 0)
    Usage(argv[0], 1);
}

void mycpy(unsigned char *to,unsigned char *from,int n)
{
  int i;

  for(i=0;i<n;i++)
    *(to+i)=*(from+i);
}

int mycmp(unsigned char *to,unsigned char *from,int n)
{
  int i=0;
  while((*(to+i)==*(from+i))&&i<n)
    i++;
  if(i==n)
    return(1);
  else
    return(0);
}

void EditPicture()
{
  static FILE *fpin, *fpout;  /* "static" prevents setjmp corruption */
  png_structp read_ptr, write_ptr;
  png_infop read_info_ptr, write_info_ptr, end_info_ptr;
  png_bytep row_pointer;
  png_uint_32 pwidth, pheight;
  int interlace_type, compression_type, filter_type;
  int bit_depth, color_type,buf_width,buf_height;
  int i,j,k;
  unsigned char *row_buf;
  png_bytepp  row_pointers = NULL;
  unsigned char *image_data = NULL;
 
  if ((fpin = fopen(namein, "rb")) == NULL) {
    printf("Could not find input file %s\n\n", namein);
    Usage("bukowski",1);
  }
  
  if ((fpout = fopen(nameout, "wb")) == NULL) {
    printf("Could not open output file %s\n\n", nameout);
    fclose(fpin);
    Usage("bukowski",1);
  }
  
  read_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
				    (png_error_ptr)NULL, (png_error_ptr)NULL);
  
  write_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
				      (png_error_ptr)NULL, (png_error_ptr)NULL);
  read_info_ptr = png_create_info_struct(read_ptr);
  write_info_ptr = png_create_info_struct(write_ptr);
  end_info_ptr = png_create_info_struct(read_ptr);
  png_init_io(read_ptr, fpin);
  png_init_io(write_ptr, fpout);
  png_read_info(read_ptr, read_info_ptr);
  png_get_IHDR(read_ptr, read_info_ptr, &pwidth, &pheight, &bit_depth,
	     &color_type, &interlace_type, &compression_type, &filter_type);
  png_set_IHDR(write_ptr, write_info_ptr,pwidth,pheight, 8,
	       PNG_COLOR_TYPE_RGB,PNG_INTERLACE_NONE, compression_type, filter_type);
  png_write_info(write_ptr, write_info_ptr);

  buf_width=(int)pwidth;
  buf_height=(int)pheight;
  row_buf = (unsigned char *) malloc(3*buf_width*buf_height*sizeof(unsigned char));
  if (color_type == PNG_COLOR_TYPE_PALETTE &&
      bit_depth <= 8) png_set_expand(read_ptr);
  else if (color_type == PNG_COLOR_TYPE_GRAY)
    png_set_gray_to_rgb(read_ptr);
  if(direction==UL || direction==LL) {
    _x1=x_off;
    _x2=min(_x1+width-1,buf_width-1);
  }
  if(direction==UL || direction==UR) {
    _y1=y_off;
    _y2=min(_y1+height-1,buf_height-1);
  }
  if(direction==LR || direction==UR) {
    _x1=max(0,buf_width-x_off-width);
    _x2=min(_x1+width-1,buf_width-1);
  }
  if(direction==LR || direction==LL) {
    _y1=max(0,buf_height-y_off-height);
    _y2=min(_y1+height-1,buf_height-1);
  }

  if(!usergb) {
    rgb[0]=(unsigned char)fixedCol[color][0];
    rgb[1]=(unsigned char)fixedCol[color][1];
    rgb[2]=(unsigned char)fixedCol[color][2];
  }
  
  if ((image_data = (unsigned char *)malloc(3*buf_width*buf_height)) == NULL) {
    png_destroy_read_struct(&read_ptr, &read_info_ptr, NULL);
    Usage("bukowski",1);
  }
  if ((row_pointers = (png_bytepp)malloc(buf_height*sizeof(png_bytep))) == NULL) {
    png_destroy_read_struct(&read_ptr, &read_info_ptr, NULL);
    free(image_data);
    Usage("bukowski",1);
  }
  for (i = 0;  i < buf_height;  ++i)
    row_pointers[i] = image_data + 3*i*buf_width;
  png_read_image(read_ptr, row_pointers);

  for(k=0;k<buf_height;k++)
    mycpy(&row_buf[k*3*buf_width],row_pointers[k],3*buf_width);
 
  for(i=_x1;i<=_x2;i++)
    for(j=_y1;j<=_y2;j++)
      mycpy(&row_buf[(j*3*buf_width+i*3)],rgb,3);

  for (k = 0; k < buf_height; k++) {
    row_pointer = (row_buf + k*3*buf_width);     
    png_write_rows(write_ptr, (png_bytepp)&row_pointer, 1);
  }
  
  png_read_end(read_ptr, end_info_ptr);
  png_write_end(write_ptr, end_info_ptr);
  free(row_buf);
  free(row_pointers);
  free(image_data);
  png_destroy_read_struct(&read_ptr, &read_info_ptr, &end_info_ptr);
  png_destroy_write_struct(&write_ptr, &write_info_ptr);
  
  fclose(fpin);
  fclose(fpout);
}

int main(int argc, char *argv[])
{
  ReadArg(argc,argv);
  EditPicture();
  return 0;
}
