//libraries
#include <minexcal.h>

typedef struct extras {
  int timepoint;
  int oldsigma;
  int minx;
  int maxx;
  int miny;
  int maxy;
  int xoffset;
  int yoffset;
  int width;
  int height;
  double cumulativedifference;
  double totalproportion;
  double entropy;
  double Lobe1;
  double Lobe2;
  double Lobe3;
  double Lobe4;
  double Lobe5;
  double Lobe6;
  double Lobe7;
  double Lobe8;
  double maxdifference;
  int maxdifferencemode;
} Extras;

#define EXTRAS ((Extras*)(originalcells.extras))
#define CELLWALL 0
#define PAVEMENTCELL 1
#define STOMATA 2
#define AREA 0
#define ENTROPY 1
#define L4 2
#define L5 3
#define L6 4
#define L7 5
#define CUMULATIVEDIFFERENCE 7
#define L2 8
#define MAXDIFFERENCE 9
#define L8 10
#define TOTALPROPERTIES 11
static char *propertynames[]={"area","entropy","L4","L5","L6","L7","cells","cumulative","L2","maxdrop","L8"};
static FLOAT **properties[TOTALPROPERTIES];

static TYPE ***image;
static TYPE **state;
static TYPE **outline;
static TYPE ***original;
static TYPE ***reconstructed;
static TYPE ***xor;
static double **difference;
static double **marginaldifference;

static int nooftimepoints;
static int totalcelltypes;
static Cell cells;
static Cell originalcells;
static Cell reconstructedcells;
static int totalcells=0;
static char outputdirname[STRING];
static char outputfilename[STRING];
static int maxcells=0;
static int ncells=0;
static int currentz;
static int stacksize;
static int biggestCellOnly;
static int excludeEdgeCells;
static int saveAverages;
static int saveCellData;
static int saveCellPositions;
static int saveCellOutlines;
static int saveShapeAnalysis;
static double ar_1;
static double ar_2;
static double pe_1;
static double pe_2;
static double as_1;
static double as_2;
static double fo_1;
static double fo_2;
static double an_1;
static double an_2;
static double cu_1;
static double cu_2;
static double en_1;
static double en_2;
static double l1_1;
static double l1_2;
static double l2_1;
static double l2_2;
static double l3_1;
static double l3_2;
static double l4_1;
static double l4_2;
static double l5_1;
static double l5_2;
static double l6_1;
static double l6_2;
static double l7_1;
static double l7_2;
static double l8_1;
static double l8_2;
static double m1_1;
static double m1_2;
static double m2_1;
static double m2_2;
static double m3_1;
static double m3_2;
static double m2_2;
static double m1_2;
static double m2_1;
static double m3_1;
static double m3_2;
static double m4_1;
static double m4_2;
static double m5_1;
static double m5_2;
static double m6_1;
static double m6_2;
static double m7_1;
static double m7_2;
static double m8_1;
static double m8_2;
static int imagewidth;
static int imageheight;
static Png png;
static char readcells[STRING];
static char readimage[STRING];
static int first=2000;
static int second=2125;
static int third=2250;
static int fourth=2375;
static int fifth=2500;
static int sixth=2625;
static int seventh=2750;
static int eighth=2875;
static int nineth=3000;
static TYPE **state_displaycellproperties;
static TYPE **image_displaycellproperties; 
static int maximumsigma=0;

//general
extern int (*newindex)(int,int);
extern TYPE boundaryvalue;
extern int locoefamodes;
extern int nrow;
extern int ncol;
extern int specialcelltype;

/********************************************functions*/

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  ReadOptions(name);
  MyInDat(name,"%d","inoofharmonicsanalyse",&locoefamodes);
  MyInDat(name,"%d","biggestCellOnly",&biggestCellOnly); 
  MyInDat(name,"%d","excludeEdgeCells",&excludeEdgeCells); 
  MyInDat(name,"%d","saveAverages",&saveAverages);
  MyInDat(name,"%d","saveCellData",&saveCellData);
  MyInDat(name,"%d","saveCellPositions",&saveCellPositions);
  MyInDat(name,"%d","saveCellOutlines",&saveCellOutlines);
  MyInDat(name,"%d","saveShapeAnalysis",&saveShapeAnalysis);
  MyInDat(name,"%d","totalcelltypes",&totalcelltypes);
  MyInDat(name,"%lf","ar_1",&ar_1);//Offset for the min and max values to create a color-code.
  MyInDat(name,"%lf","ar_2",&ar_2);
  MyInDat(name,"%lf","pe_1",&pe_1);
  MyInDat(name,"%lf","pe_2",&pe_2);
  MyInDat(name,"%lf","as_1",&as_1);
  MyInDat(name,"%lf","as_2",&as_2);
  MyInDat(name,"%lf","fo_1",&fo_1);
  MyInDat(name,"%lf","fo_2",&fo_2);
  MyInDat(name,"%lf","an_1",&an_1);
  MyInDat(name,"%lf","an_2",&an_2);
  MyInDat(name,"%lf","cu_1",&cu_1);
  MyInDat(name,"%lf","cu_2",&cu_2);
  MyInDat(name,"%lf","en_1",&en_1);
  MyInDat(name,"%lf","en_2",&en_2);
  MyInDat(name,"%lf","l1_1",&l1_1);
  MyInDat(name,"%lf","l1_2",&l1_2);
  MyInDat(name,"%lf","l2_1",&l2_1);
  MyInDat(name,"%lf","l2_2",&l2_2);
  MyInDat(name,"%lf","l3_1",&l3_1);
  MyInDat(name,"%lf","l3_2",&l3_2);
  MyInDat(name,"%lf","l4_1",&l4_1);
  MyInDat(name,"%lf","l4_2",&l4_2);
  MyInDat(name,"%lf","l5_1",&l5_1);
  MyInDat(name,"%lf","l5_2",&l5_2);
  MyInDat(name,"%lf","l6_1",&l6_1);
  MyInDat(name,"%lf","l6_2",&l6_2);
  MyInDat(name,"%lf","l7_1",&l7_1);
  MyInDat(name,"%lf","l7_2",&l7_2);
  MyInDat(name,"%lf","l8_1",&l8_1);
  MyInDat(name,"%lf","l8_2",&l8_2);
  MyInDat(name,"%lf","m1_1",&m1_1);
  MyInDat(name,"%lf","m1_2",&m1_2);
  MyInDat(name,"%lf","m2_1",&m2_1);
  MyInDat(name,"%lf","m2_2",&m2_2);
  MyInDat(name,"%lf","m3_1",&m3_1);
  MyInDat(name,"%lf","m3_2",&m3_2);
  MyInDat(name,"%lf","m4_1",&m4_1);
  MyInDat(name,"%lf","m4_2",&m4_2);
  MyInDat(name,"%lf","m5_1",&m5_1);
  MyInDat(name,"%lf","m5_2",&m5_2);
  MyInDat(name,"%lf","m6_1",&m6_1);
  MyInDat(name,"%lf","m6_2",&m6_2);
  MyInDat(name,"%lf","m7_1",&m7_1);
  MyInDat(name,"%lf","m7_2",&m7_2);
  MyInDat(name,"%lf","m8_1",&m8_1);
  MyInDat(name,"%lf","m8_2",&m8_2);
}

int DetermineTotalNoOfCells(int ac,char *av[])
{
  int k,c,tmpncol=0,tmpnrow=0;
  TYPE **totaldifferentcells;
  int cellnumber=0;

  //read size of image(s)
  stacksize=ac-4;
  nooftimepoints=stacksize;

  for(k=4;k<ac;k++) {
    ReadSizePNG(&nrow,&ncol,av[k]);
    tmpnrow=max(tmpnrow,nrow);
    tmpncol=max(tmpncol,ncol);
  }
  nrow=tmpnrow;
  ncol=tmpncol;  

  //allocate planes and colors
  image = (TYPE***)calloc((size_t)stacksize,sizeof(TYPE**));
  if (image == NULL) {
    fprintf(stderr,"DetermineTotalNoOfCells: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for(k=0;k<stacksize;k++)
    image[k]=New();

  state=New();
  outline=New();

  //read image(s)
  for(k=4;k<ac;k++) {
    ReadSizePNG(&nrow,&ncol,av[k]);
    ReadPNG(image[k-4],1,1,av[k]);
  }
  nrow=tmpnrow;
  ncol=tmpncol;  
  fprintf(stdout,"max nrow: %d, max ncol: %d\n",nrow,ncol);

  //determine ncells
  ncells=0;
  for(k=0;k<stacksize;k++) {
    ReadSizePNG(&nrow,&ncol,av[k+4]);
    PLANE(ncells=max(ncells,image[k][i][j]););
  }
  nrow=tmpnrow;
  ncol=tmpncol;  
  
  if(!ncells) {
    fprintf(stdout,"Warning: no cells in images, error in SetUp function!\n");
    exit(EXIT_FAILURE); 
  }
  maxcells=ncells+1;
  fprintf(stdout,"max cells in image(s): %d\n",maxcells);
  ncells=0;

  //what is highest number of cells?
  totaldifferentcells=NewPlane(stacksize,maxcells-1);
  //register each cell that exists
  for(k=0;k<stacksize;k++) {
    PLANE(
	  if(image[k][i][j])
	    totaldifferentcells[k][image[k][i][j]]=1;
	  );
  }
  
  for(k=0;k<stacksize;k++)
    for(c=1;c<maxcells;c++)
      if(totaldifferentcells[k][c]) {
	cellnumber++;
      }
  
  //allocate cells
  CNew(&cells,maxcells,totalcelltypes);
  InitCellPosition(&cells);
  InitLOCOEFA(&cells);

  return cellnumber;
}

void UpdateCells()
{
  fprintf(stdout,"Initiating cells...\n");
  UpdateCellPosition(state,&cells);
  
  ncells=0;
  CELLS(cells,
	if(c>specialcelltype && cells.area[c])
	  ncells=c;
	);
  fprintf(stdout,"\t...done, %d cells\n",ncells);
}

void SelectCells()
{
  int largestcell=0;
  int largestarea=0;
  fprintf(stdout,"Selecting cells...\n");
  
  CELLS(cells,
	if(c<=specialcelltype) {
	  cells.contourlength[c]=0;
	}
	);

  if(excludeEdgeCells) {
    PLANE(
	  if(state[i][j] && (i==1 || i==nrow || j==1 || j==ncol))
	    cells.contourlength[state[i][j]]=0;
	  );
  }
  
  if(biggestCellOnly) {
    CELLS(cells,
	  if(cells.contourlength[c]) {
	    if(cells.area[c]>largestarea) {
	      largestcell=c;
	      largestarea=cells.area[c];
	    }
	  }
	  );
    CELLS(cells,
	  if(c!=largestcell) {
	    cells.contourlength[c]=0;
	  }
	  );
  }
  
  ncells=0;
  CELLS(cells,
	if(cells.contourlength[c]) {
	  ncells++;
	}
	);
  fprintf(stdout,"\t...done, %d cells selected in SelectCells.\n",ncells);
}
   
void SaveAverages()
{
  FILE *fp;
  double average_cellsize=0.;
  
  fprintf(stdout,"ncells: %d\n",ncells);
  CELLS(cells,
	if(cells.contourlength[c])
	  average_cellsize+=(double)cells.area[c];
	);
  average_cellsize/=(double)ncells;
  fprintf(stdout,"average cell size: %f\n",average_cellsize);
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write average cell characterisitcs: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing overall cell characteristics...\n");
  fprintf(fp,"Average cell characteristics\n\n");
  fprintf(fp,"Number of cells: %d\n",ncells);
  fprintf(fp,"Average cells size: %f\n",average_cellsize);		
  fprintf(fp,"\n\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellData()
{
  FILE *fp;
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell characterisitcs: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell characterisitcs...\n");
  fprintf(fp,"Cell data\n\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  fprintf(fp,"Cell: %d\n",c);
	  fprintf(fp,"size: %d\n",cells.area[c]);
	  fprintf(fp,"meanx: %f\n",cells.shape[c].meanx);
	  fprintf(fp,"meany: %f\n",cells.shape[c].meany);
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellPositions()
{
  FILE *fp;
    
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell position: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell positions...\n");
  fprintf(fp,"Indivdual cell positions\n\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  fprintf(fp,"Surface cell %d\n",c);
	  PLANE(
		if(state[i][j]==c)
		  fprintf(fp,"%d\t%d\n",j,i););
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellOutlines()
{
  FILE *fp;
  int k,newsigma;
  double d_centre;
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open data-file: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell outlines...\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  //find last added cell with that number
	  newsigma=totalcells;
	  while(EXTRAS[newsigma].oldsigma!=c)
	    newsigma--;
	  fprintf(fp,"Outline cell %d\n",c);
	  fprintf(fp,"Index\tX-position\tY-position\tLength\tRel.Length\tDistance.Center\tArea\n");
	  for(k=0;k<=originalcells.contourlength[newsigma];k++) {
	    d_centre=hypot(cells.shape[c].meany-originalcells.contour[newsigma][k].y,cells.shape[c].meanx-originalcells.contour[newsigma][k].x);
	    fprintf(fp,"%d\t%d\t%d\t%f\t%f\t%f\t%d\n",k,(int)originalcells.contour[newsigma][k].x,(int)originalcells.contour[newsigma][k].y,originalcells.contour[newsigma][k].t,originalcells.contour[newsigma][k].t/originalcells.contour[newsigma][originalcells.contourlength[newsigma]].t,d_centre,cells.area[c]);
	  }
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void AssignPerimeter(int celltype,int timepoint,int oldsigma)
{
  int i;

  totalcells++;
  originalcells.celltype[totalcells]=celltype;
  if(originalcells.celltype[totalcells]>=originalcells.maxtypes) {
    fprintf(stderr,"celltype larger or equal than totalcelltypes (%d>=%d). Exitting now!\n",originalcells.celltype[totalcells],originalcells.maxtypes);
    exit(EXIT_FAILURE);
  }

  EXTRAS[totalcells].timepoint=timepoint;
  EXTRAS[totalcells].oldsigma=oldsigma;
  originalcells.area[totalcells]=cells.area[oldsigma];

  if(((difference[totalcells]=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((marginaldifference[totalcells]=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL)) {
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  InitLOCOEFAContour(&originalcells,totalcells,cells.contourlength[oldsigma]);
  
  for(i=0;i<=cells.contourlength[oldsigma];i++) {
    originalcells.contour[totalcells][i].x=cells.contour[oldsigma][i].x;
    originalcells.contour[totalcells][i].y=cells.contour[oldsigma][i].y;

    if(i==0) {//first datapoint
      EXTRAS[totalcells].minx=originalcells.contour[totalcells][i].x;
      EXTRAS[totalcells].maxx=originalcells.contour[totalcells][i].x;
      EXTRAS[totalcells].miny=originalcells.contour[totalcells][i].y;
      EXTRAS[totalcells].maxy=originalcells.contour[totalcells][i].y;
    }
    else {
      EXTRAS[totalcells].minx=min(EXTRAS[totalcells].minx,originalcells.contour[totalcells][i].x);
      EXTRAS[totalcells].maxx=max(EXTRAS[totalcells].maxx,originalcells.contour[totalcells][i].x);
      EXTRAS[totalcells].miny=min(EXTRAS[totalcells].miny,originalcells.contour[totalcells][i].y);
      EXTRAS[totalcells].maxy=max(EXTRAS[totalcells].maxy,originalcells.contour[totalcells][i].y);
    }
  }

  EXTRAS[totalcells].width=EXTRAS[totalcells].maxx-EXTRAS[totalcells].minx+1;
  EXTRAS[totalcells].height=EXTRAS[totalcells].maxy-EXTRAS[totalcells].miny+1;
}

void RunOutline2D(int ac,char *av[])
{
  FILE *fp;

  for (currentz=0;currentz<stacksize;currentz++) {
    ReadSizePNG(&nrow,&ncol,av[currentz+4]);
    PLANE(
	  state[i][j]=0;
	  outline[i][j]=0;
	  );
    PLANE(
	  state[i][j]=image[currentz][i][j];
	  );
    UpdateCFill(state,&cells);
    UpdateCellContour(outline,state,&cells);
    CELLS(cells,
	  if(cells.contourlength[c])
	    fprintf(stdout,"cell %d length %d\n",c,cells.contourlength[c]);
	  );

    UpdateCells();
    SelectCells();
    
    CELLS(cells, 
	  if(cells.contourlength[c]) {
	    AssignPerimeter(1 /*celltype, currently always 1*/,currentz /*timepoint*/,c /*sigma*/);
	    CalculateEFACoefficients(&originalcells,totalcells);
	  }
	  );
    
    if((fp=fopen(outputfilename,"a"))==NULL) {
      fprintf(stderr,"warning: could not open data-file: %s\n",outputfilename);
      exit(EXIT_FAILURE);
    }
    
    if(ncells) {
      fprintf(fp,"Data for cells in: %s\n\n",av[currentz+3]);
      fclose(fp);
      if(saveAverages)
	SaveAverages();
      if(saveCellData)
	SaveCellData();
      if(saveCellPositions)
	SaveCellPositions();
      if(saveCellOutlines)
	SaveCellOutlines();
    }
    else {
      fprintf(fp,"No cells selected in: %s\n\n",av[currentz+3]);
      fclose(fp);
    }
  }   
}

void ReadData(int ac,char *av[])
{
  FILE *fp;
  int i;
  int maxwidth=0,maxheight=0;
  char a_string[STRING];

  //check # of arguments
  if(ac<5) {
    fprintf(stdout,"usage: %s parfile output_dirname orignal_image [segmented images]\n",av[0]);
    exit(EXIT_FAILURE);
  }

  //create outputdir
  if(snprintf(outputdirname,STRING,"Output_DisplayProperties/%s",av[2])>=STRING) {
    outputdirname[0]=toupper(outputdirname[0]);
    fprintf(stderr,"warning: outputdirname too long: %s\n",outputdirname);
  }
  outputdirname[0]=toupper(outputdirname[0]);
  snprintf(a_string,STRING,"mkdir -p %s",outputdirname);
  system(a_string);

  //create outputfilename
  if(snprintf(outputfilename,STRING,"%s/%s.dat",outputdirname,av[2])>=STRING){
    fprintf(stderr,"warning: filename for log too long: %s\n",outputfilename);
  }

  //writing outputfilename header
  if((fp=fopen(outputfilename,"w"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write header: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(fp,"data analyzed with %s\n",av[0]);
  fclose(fp);
  
  //read parameters
  fprintf(stdout,"reading parameters from %s\n",av[1]);
  SetPars(av[1]); 
  
  if(snprintf(a_string,STRING,"%s/%s.par",outputdirname,av[2])>=STRING)
    fprintf(stderr,"warning: parfile name too long: %s\n",a_string);
  else
    SaveOptions(av[1],a_string);
  
  //now we need to know how many cells we will have, as well as setting of nooftimepoints
  //this will be overestimation of totalcells, since each picture will contain cells that are not valid
  //we therefore keep the more serious allocation to later on, limited to cells we are really interested in

  //so first time (after DetermineTotalNoOfCells) totalcells will be bigger than second time (after RunOutline2D)
  totalcells=DetermineTotalNoOfCells(ac,av);
  
  CNew(&originalcells,totalcells+1,totalcelltypes); //maxsigma+1 because we have to count 0 as well
  CNew(&reconstructedcells,totalcells+1,totalcelltypes); //maxsigma+1 because we have to count 0 as well
  if((originalcells.extras=(void *)calloc((size_t)originalcells.maxcells,sizeof(Extras)))==NULL) {
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  InitLOCOEFA(&originalcells);

  //two reasons why each cell in different plane:
  //1) much more easy to determine inside/outside of cell
  //2) reconstruction of different cells could overlap
  //but now we do it one after another instead all all concurrently
  //as it is too expensive 
  if(((original=(TYPE***)calloc((size_t)originalcells.maxcells,sizeof(TYPE**)))==NULL) ||
     ((difference=(double**)calloc((size_t)originalcells.maxcells,sizeof(double*)))==NULL) ||
     ((marginaldifference=(double**)calloc((size_t)originalcells.maxcells,sizeof(double*)))==NULL) ||
     ((reconstructed=(TYPE***)calloc((size_t)(locoefamodes+2),sizeof(TYPE**)))==NULL) ||
     ((xor=(TYPE***)calloc((size_t)(locoefamodes+2),sizeof(TYPE**)))==NULL)) { 
    
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  
  totalcells=0;
  //now really read in
  RunOutline2D(ac,av);
  
  //how big do we need it?
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  if(EXTRAS[c].width>maxwidth)
	    maxwidth=EXTRAS[c].width;
	  if(EXTRAS[c].height>maxheight)
	    maxheight=EXTRAS[c].height;
	}
	);
 
  fprintf(stdout,"maxwidth %d maxheight %d\n",maxwidth,maxheight);
  //let's make the planes twice as large as the longest axis. 
  nrow=2*max(maxwidth,maxheight);
  ncol=nrow;
  fprintf(stdout,"ncol %d and nrow %d\n", ncol,nrow);
 
  //how much sticks out on each side?
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  EXTRAS[c].xoffset=(ncol-EXTRAS[c].width)/2-EXTRAS[c].minx+1;
	  EXTRAS[c].yoffset=(nrow-EXTRAS[c].height)/2-EXTRAS[c].miny+1;
	  original[c]=New();
	}
	);
  for(i=0;i<locoefamodes+2;i++) {
    reconstructed[i]=New();
    xor[i]=New();
  }
} 

void FillOriginalDataPlanes()
{
  int k;
  CELLS(originalcells,
	for(k=0;k<originalcells.contourlength[c];k++)
	  original[c][(EXTRAS[c].yoffset+(int)originalcells.contour[c][k].y)][(EXTRAS[c].xoffset+(int)originalcells.contour[c][k].x)]=c;
	);
  fprintf(stdout,"debug: FillOriginalDataPlanes OK\n");
}

int in_plane(int i, int j)
{
  if ((i > 1) && (i+1 < nrow) && (j > 1) && (j+1 < ncol))
    return 1;
  return 0;
}

int should_be_boundary(TYPE **a, int i, int j)
{
  if (!in_plane(i, j))
    return 0;
  if ((a[i][j] == 0) && (a[i-1][j]==-1 || a[i+1][j]==-1 || a[i][j-1]==-1 || a[i][j+1]==-1))
    return 1;
  else return 0;
}

void flip_if_needed(TYPE **a, int i, int j)
{
  if(should_be_boundary(a, i, j))
    a[i][j] = -1;
}

void ExtendBoundary(TYPE **a, int i, int j)
{
  int ni, nj;
  //fprintf(stdout,"Extending at %d, %d (%d, %d)\n", i, j, nrow, ncol);
  a[i][j] = -1;
  
  for(ni=i;ni<nrow;ni++)
    flip_if_needed(a, ni, j);
  for(ni=i;ni>0;ni--)
    flip_if_needed(a, ni, j);
  for(nj=j;nj<ncol;nj++)
    flip_if_needed(a, i, nj);
  for(nj=j;nj>0;nj--)
    flip_if_needed(a, i, nj);
  
  /*
    if (should_be_boundary(a, i-1, j)) ExtendBoundary(a, i-1, j);
    if (should_be_boundary(a, i+1, j)) ExtendBoundary(a, i+1, j);
    if (should_be_boundary(a, i, j+1)) ExtendBoundary(a, i, j+1);
    if (should_be_boundary(a, i, j-1)) ExtendBoundary(a, i, j-1);
  */
}

void FillShape(TYPE **a,int c)
{
  boundaryvalue=-1;
  Boundaries(a);
  //fprintf(stdout,"debug: FillShape cell %d\n",c);
  PLANE(
	if(!a[i][j] && (a[i-1][j]==-1 || a[i+1][j]==-1 || a[i][j-1]==-1 || a[i][j+1]==-1))
	  ExtendBoundary(a, i, j);
	);
  PLANE(
	if(!a[i][j])//pixel has not been changed into -1, so this is inside cell
	  a[i][j]=c;
	else if(a[i][j]==-1)//pixel has been changed into 0, so this is outside cell
	  a[i][j]=0;
	);
  boundaryvalue=0;
  Boundaries(a);
}

void FillOriginalShape(int cell)
{
  //  CELLS(originalcells,
  if(originalcells.contourlength[cell])
    FillShape(original[cell],cell);
  //	);
  fprintf(stdout,"OK FillOriginalShape of cell %d\n",cell); 
}

void FillReconstructedShape(int cell)
{
  int lobes;

  //  CELLS(originalcells,
  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"filling in cell #%d of %d\n",cell,totalcells);
    for(lobes=1;lobes<locoefamodes+2;lobes++) {
      FillShape(reconstructed[lobes],cell);
    }
  }
  //	);
}

void MindTheGap(int c,int lobes,int oldx,int x,int oldy,int y,double oldt,double t,int level)
{
  double xpoint,ypoint;
  int newx,newy;

  ReconstructContourpoint(&originalcells,c,(oldt+t)/2.,LOCOEFA,0,lobes,&xpoint,&ypoint);
  newx=(int)nearbyint(xpoint);
  newy=(int)nearbyint(ypoint);
  
  reconstructed[lobes][newindex(newy+EXTRAS[c].yoffset,nrow)][newindex(newx+EXTRAS[c].xoffset,ncol)]=c;

  if(level>1)
    fprintf(stdout,"mind the gap: %d %d %d (%d,%d) (%d,%d) (%d,%d) %lf %lf\n",level,c,lobes,oldx,oldy,newx,newy,x,y,oldt,t);

  if((newx-oldx)*(newx-oldx)+(newy-oldy)*(newy-oldy)>2)
    MindTheGap(c,lobes,oldx,newx,oldy,newy,oldt,(oldt+t)/2.,level+1);
  if((x-newx)*(x-newx)+(y-newy)*(y-newy)>2)
    MindTheGap(c,lobes,newx,x,newy,y,(oldt+t)/2.,t,level+1);
}

void DetermineReconstructedShape(int cell)
{
  int i;
  int lobes;
  int x,y;
  double xpoint,ypoint;
  int oldx=0,oldy=0;
  
  //  CELLS(originalcells,
  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"reconstructing cell #%d (%d) of %d\n",cell,EXTRAS[cell].oldsigma,totalcells);
    for(lobes=1;lobes<locoefamodes+2;lobes++) {
      for(i=0;i<=originalcells.contourlength[cell];i++) {
	ReconstructContourpoint(&originalcells,cell,originalcells.contour[cell][i].t,LOCOEFA,0,lobes,&xpoint,&ypoint);
	x=(int)nearbyint(xpoint);
	y=(int)nearbyint(ypoint);
	reconstructed[lobes][newindex(y+EXTRAS[cell].yoffset,nrow)][newindex(x+EXTRAS[cell].xoffset,ncol)]=cell;
	if(i && ((x-oldx)*(x-oldx)+(y-oldy)*(y-oldy)>2)) {//dist squared more than one pixel
	  MindTheGap(cell,lobes,oldx,x,oldy,y,originalcells.contour[cell][i-1].t,originalcells.contour[cell][i].t,1);
	}
	oldx=x;
	oldy=y;
      }
    }
  }
  //	);
}

void SavePictures()
{
  char a_string[STRING];
  int i;
  
  OpenPNG(&png,outputdirname);
  CFPlanePNG(properties[AREA],state,&png,CFMin(properties[AREA],state)+ar_1,CFMax(properties[AREA],state)+ar_2,first,nineth);
  //CFPlanePNG(properties[PERIMETER],state,&png,CFMin(properties[PERIMETER],state)+pe_1,CFMax(properties[PERIMETER],state)+pe_2,first,nineth);
  //CFPlanePNG(properties[ASPECTRATIO],state,&png,CFMin(properties[ASPECTRATIO],state)+as_1,CFMax(properties[ASPECTRATIO],state)+as_2,second,nineth);
  //CFPlanePNG(properties[FORMFACTOR],state,&png,CFMin(properties[FORMFACTOR],state)+fo_1,CFMax(properties[FORMFACTOR],state)+fo_2,first,nineth);
  //CFPlanePNG(properties[ROTATIONANGLE],state,&png,CFMin(properties[ROTATIONANGLE],state)+an_1,CFMax(properties[ROTATIONANGLE],state)+an_2,first,nineth);
  CFPlanePNG(properties[ENTROPY],state,&png,CFMin(properties[ENTROPY],state)+en_1,CFMax(properties[ENTROPY],state)+en_2,first,nineth);
  //CFPlanePNG(properties[L1],state,&png,CFMin(properties[L1],state)+l1_1,CFMax(properties[L1],state)+l1_2,first,nineth);
  //CFPlanePNG(properties[L3],state,&png,CFMin(properties[L3],state)+l3_1,CFMax(properties[L3],state)+l3_2,first,nineth);
  printf("2: %g %g 4: %g %g 5: %g %g 6: %g %g 7: %g %g 8: %g %g\n",CFMin(properties[L2],state),CFMax(properties[L2],state),CFMin(properties[L4],state),CFMax(properties[L4],state),CFMin(properties[L5],state),CFMax(properties[L5],state),CFMin(properties[L6],state),CFMax(properties[L6],state),CFMin(properties[L7],state),CFMax(properties[L7],state),CFMin(properties[L8],state),CFMax(properties[L8],state));
  //CFPlanePNG(properties[L4],state,&png,CFMin(properties[L4],state)+l4_1,CFMax(properties[L4],state)+l4_2,first,nineth);
  //CFPlanePNG(properties[L5],state,&png,CFMin(properties[L5],state)+l5_1,CFMax(properties[L5],state)+l5_2,first,nineth);
  //CFPlanePNG(properties[L6],state,&png,CFMin(properties[L6],state)+l6_1,CFMax(properties[L6],state)+l6_2,first,nineth);
  //CFPlanePNG(properties[L7],state,&png,CFMin(properties[L7],state)+l7_1,CFMax(properties[L7],state)+l7_2,first,nineth);
  CFPlanePNG(properties[L4],state,&png,l4_1,l4_2,first,nineth);
  CFPlanePNG(properties[L5],state,&png,l5_1,l5_2,first,nineth);
  CFPlanePNG(properties[L6],state,&png,l6_1,l6_2,first,nineth);
  CFPlanePNG(properties[L7],state,&png,l7_1,l7_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE1],state,&png,CFMin(properties[MARGINALDIFFERENCE1],state)+m1_1,CFMax(properties[MARGINALDIFFERENCE1],state)+m1_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE2],state,&png,CFMin(properties[MARGINALDIFFERENCE2],state)+m2_1,CFMax(properties[MARGINALDIFFERENCE2],state)+m2_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE3],state,&png,CFMin(properties[MARGINALDIFFERENCE3],state)+m3_1,CFMax(properties[MARGINALDIFFERENCE3],state)+m3_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE4],state,&png,CFMin(properties[MARGINALDIFFERENCE4],state)+m4_1,CFMax(properties[MARGINALDIFFERENCE4],state)+m4_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE5],state,&png,CFMin(properties[MARGINALDIFFERENCE5],state)+m5_1,CFMax(properties[MARGINALDIFFERENCE5],state)+m5_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE6],state,&png,CFMin(properties[MARGINALDIFFERENCE6],state)+m6_1,CFMax(properties[MARGINALDIFFERENCE6],state)+m6_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE7],state,&png,CFMin(properties[MARGINALDIFFERENCE7],state)+m7_1,CFMax(properties[MARGINALDIFFERENCE7],state)+m7_2,first,nineth);
  //CFPlanePNG(properties[MARGINALDIFFERENCE8],state,&png,CFMin(properties[MARGINALDIFFERENCE8],state)+m8_1,CFMax(properties[MARGINALDIFFERENCE8],state)+m8_2,first,nineth);
  CellPNG(state,&cells,&png,0);
  ClosePNG(&png);
  CFPlanePNG(properties[CUMULATIVEDIFFERENCE],state,&png,CFMin(properties[CUMULATIVEDIFFERENCE],state)+cu_1,CFMax(properties[CUMULATIVEDIFFERENCE],state)+cu_2,first,nineth);
  CFPlanePNG(properties[L2],state,&png,l2_1,l2_2,first,nineth);
  CFPlanePNG(properties[MAXDIFFERENCE],state,&png,0,11,2001,2012);
  CFPlanePNG(properties[L8],state,&png,l8_1,l8_2,first,nineth);
  
  for(i=0;i<TOTALPROPERTIES;i++) {
    snprintf(a_string,STRING,"mv %s/%.5d.png %s/%s.png",outputdirname,i,outputdirname,propertynames[i]);
    system(a_string);
  }   
}

void CalculateReconstruction() //Calculate LOCOEFA coeffiencients
{
  int k;
  
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  CalculateLOCOCoefficients(&originalcells,c);

	  EXTRAS[c].Lobe1=originalcells.locoefa[c][1].locoL;
	  EXTRAS[c].Lobe2=originalcells.locoefa[c][2].locoL;
	  EXTRAS[c].Lobe3=originalcells.locoefa[c][3].locoL;
	  EXTRAS[c].Lobe4=originalcells.locoefa[c][4].locoL;
	  EXTRAS[c].Lobe5=originalcells.locoefa[c][5].locoL;
	  EXTRAS[c].Lobe6=originalcells.locoefa[c][6].locoL;
	  EXTRAS[c].Lobe7=originalcells.locoefa[c][7].locoL;
	  EXTRAS[c].Lobe8=originalcells.locoefa[c][8].locoL;
	  
	  fprintf(stdout,"Debug L coeff for cell %d\n",c);
	  for(k=1;k<=locoefamodes+1;k++) {
	    fprintf(stdout,"L= %lf\n",originalcells.locoefa[c][k].locoL);
	  }
	}
	);
}

void Difference(int cell)
{
  int lobes;
  double cumulativedifference;

  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"xor-ing cell #%d of %d\n",cell,totalcells);
    cumulativedifference=0;
    for(lobes=1;lobes<locoefamodes+2;lobes++) {
      Xor(xor[lobes],original[cell],reconstructed[lobes]);
      //PLANE(if(xor[lobes][i][j]){fprintf(stdout,"lobo mal %d\n",xor[lobes][i][j]);break;});
      //multiply by c, because reconstructed contains values c,0,c,c, but xor contains 1,0,1,1 etc
      difference[cell][lobes]=(double)cell*(double)Total(xor[lobes])/(double)Total(original[cell]);
      cumulativedifference+=difference[cell][lobes];
      marginaldifference[cell][lobes]=difference[cell][lobes-1]-difference[cell][lobes];
      if(lobes>1 && marginaldifference[cell][lobes]>EXTRAS[cell].maxdifference) {
	EXTRAS[cell].maxdifference=marginaldifference[cell][lobes];
	EXTRAS[cell].maxdifferencemode=lobes;
      }
      // if(marginaldifference[c][lobes]<=0) marginaldifference[c][lobes]=EPSILON;
      fprintf(stdout,"difference #%d of %d\t lobe %d\tdiff %f\tmarg %f\tculm %f\n",cell,totalcells,lobes,difference[cell][lobes], marginaldifference[cell][lobes],cumulativedifference);
    }
  }
}

void CumulativeDifference() //Cumulative difference [x N]; where x=2,3,4 N=inoofharmonicstoanalyze
{
  int lobes;

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  for(lobes=2;lobes<locoefamodes+2;lobes++)
	    EXTRAS[c].cumulativedifference+=difference[c][lobes];
	  fprintf(stdout,"Cell %d CumulativeDifference %f\n",c,EXTRAS[c].cumulativedifference);
  	}
	);
}

void Entropy() //Taking the L landscape
{
  int lobes;

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  EXTRAS[c].entropy=0;
	  for(lobes=1;lobes<locoefamodes+2;lobes++) {
	    EXTRAS[c].totalproportion+=originalcells.locoefa[c][lobes].locoL;
	  }
	  for(lobes=1;lobes<locoefamodes+2;lobes++) {
	    if(originalcells.locoefa[c][lobes].locoL>EPSILON)
	      EXTRAS[c].entropy+=-((originalcells.locoefa[c][lobes].locoL/EXTRAS[c].totalproportion)*(double)log(originalcells.locoefa[c][lobes].locoL/EXTRAS[c].totalproportion));
	  }
	}
	);
  //fprintf(stdout,"Entropy %f\n",EXTRAS[c].entropy); 
}

void AsignCellProperties(int ac,char *av[])
{
  int i;
  int dummynrow,dummyncol;
  int k;
  
  //what is orginal_image?
  if(snprintf(readimage,STRING,"%s",av[3])>=STRING) {
    fprintf(stderr,"warning: orginal image name too long: %s\n",av[3]);
    exit(EXIT_FAILURE);
  }
  
  //what is reconstructed/segmented_cells?
  if(snprintf(readcells,STRING,"%s",av[4])>=STRING) {
    fprintf(stderr,"warning: segmented cells name too long: %s\n",av[4]);
    exit(EXIT_FAILURE);
  }
  
  //first, make the right colors, starting at e.g. 10000
  CELLS(cells,
	ColorRGB(10000+c,c/(256*256),(c/256)%(256*256),c%256);
	);
 
  //heatmap
  first=3001;second=3126;third=3251;fourth=3376;fifth=3501;sixth=3626;seventh=3751;eighth=3876;nineth=4001;
  ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
  ColorGrad(second,ColorTable(third,third,BLUE));
  ColorGrad(third,ColorTable(fourth,fourth,CYAN));
  ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
  ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
  ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
  ColorGrad(seventh,ColorTable(eighth,eighth,RED));
  ColorGrad(eighth,ColorRGB(nineth,139,0,0));
  ColorName(nineth+1,nineth+1,"black");

  ColorName(2001,2012,"white","black","RebeccaPurple","blue","green","yellow","orange","red","grey","grey","grey","grey");

  //the cells
  ColorName(CELLWALL,CELLWALL,"black");
  ColorName(PAVEMENTCELL,PAVEMENTCELL,"green");
  ColorName(STOMATA,STOMATA,"red");
  ColorName(255,255,"dark blue");

  //how big is the picture?
  if(ReadSizePNG(&nrow,&ncol,readcells)) {
    fprintf(stderr,"warning: reconstructed cell file \"%s\" could not be opened!\n",readcells);
    exit(EXIT_FAILURE);
  } 
  if(ReadSizePNG(&dummynrow,&dummyncol,readimage)) {
    fprintf(stderr,"warning: image file \"%s\" could not be opened!\n",readimage);
    exit(EXIT_FAILURE);
  } 
  
  if((nrow!=dummynrow) || (ncol!=dummyncol)) {
    fprintf(stderr,"warning: the image and the cell reconstruction are of different sizes, check your files!\n");
    exit(EXIT_FAILURE);
  }
  
  imageheight=dummynrow;
  imagewidth=dummyncol;
  
  //Find the max sigma from oldsigma
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  if(EXTRAS[c].oldsigma>maximumsigma)
	    maximumsigma=EXTRAS[c].oldsigma;
	}
	);
  maximumsigma=maximumsigma+1;
  
  state_displaycellproperties=New();
  image_displaycellproperties=New();
  
  for(i=0;i<TOTALPROPERTIES;i++)
    properties[i]=FNew();
    
  ReadPatPNG(state_displaycellproperties,1,1,readcells,10000);
  ReadPatPNG(image_displaycellproperties,1,1,readimage,10000+maximumsigma);
  
  PLANE(
	if(state[i][j]>=maximumsigma)
	  state[i][j]=0;
	);

  CELLS(cells,
	if(c>specialcelltype) {
	  cells.celltype[c]=PAVEMENTCELL;
	  if(c==134710000)
	    cells.celltype[c]=STOMATA;
	}
	else {
	  cells.celltype[c]=CELLWALL;
	}
	);
  
  PLANE(
	k=0;
    
	CELLS(originalcells,
	      if(originalcells.contourlength[c]) {		 
		if (state[i][j]==EXTRAS[c].oldsigma) {
		  k=c;
		}
	      }
	      );
	if(!k)
	  state[i][j]=0;
	else {
	  properties[AREA][i][j]=(FLOAT)originalcells.area[k];
	  //properties[PERIMETER][i][j]=(FLOAT)EXTRAS[k].cellperimeter;	
	  //properties[ASPECTRATIO][i][j]=(FLOAT)(cells.shape[state[i][j]].eigenval1/(cells.shape[state[i][j]].eigenval1+cells.shape[state[i][j]].eigenval2));
	  //properties[FORMFACTOR][i][j]=(FLOAT)((4.*M_PI*(double)EXTRAS[k].cellarea)/(double)(EXTRAS[k].cellperimeter)*(EXTRAS[k].cellperimeter));
	  //properties[ROTATIONANGLE][i][j]=(FLOAT)EXTRAS[k].angle;
	  properties[CUMULATIVEDIFFERENCE][i][j]=(FLOAT)EXTRAS[k].cumulativedifference;
	  properties[ENTROPY][i][j]=(FLOAT)EXTRAS[k].entropy;
	  //properties[L1][i][j]=(FLOAT)EXTRAS[k].Lobe1;
	  properties[L2][i][j]=(FLOAT)EXTRAS[k].Lobe2;
	  //properties[L3][i][j]=(FLOAT)EXTRAS[k].Lobe3;
	  properties[L4][i][j]=(FLOAT)EXTRAS[k].Lobe4;
	  properties[L5][i][j]=(FLOAT)EXTRAS[k].Lobe5;
	  properties[L6][i][j]=(FLOAT)EXTRAS[k].Lobe6;
	  properties[L7][i][j]=(FLOAT)EXTRAS[k].Lobe7;
	  properties[L8][i][j]=(FLOAT)EXTRAS[k].Lobe8;
	  properties[MAXDIFFERENCE][i][j]=(FLOAT)EXTRAS[k].maxdifferencemode;
	  //properties[MARGINALDIFFERENCE1][i][j]=(FLOAT)EXTRAS[k].marginaldifference1;
	  //properties[MARGINALDIFFERENCE2][i][j]=(FLOAT)EXTRAS[k].marginaldifference2;
	  //properties[MARGINALDIFFERENCE3][i][j]=(FLOAT)EXTRAS[k].marginaldifference3;
	  //properties[MARGINALDIFFERENCE4][i][j]=(FLOAT)EXTRAS[k].marginaldifference4;
	  //properties[MARGINALDIFFERENCE5][i][j]=(FLOAT)EXTRAS[k].marginaldifference5;
	  //properties[MARGINALDIFFERENCE6][i][j]=(FLOAT)EXTRAS[k].marginaldifference6;
	  //properties[MARGINALDIFFERENCE7][i][j]=(FLOAT)EXTRAS[k].marginaldifference7;
	  //properties[MARGINALDIFFERENCE8][i][j]=(FLOAT)EXTRAS[k].marginaldifference8;
	}
	);
}
	
void CleanOldCell(int cell)
{
  //This function makes sure the 'space' is cleaned before drawing the next cell.
  int k;
  for(k=0;k<locoefamodes+2;k++) {
    Fill(reconstructed[k],0);
    Fill(xor[k],0);
  }
}

void UpdateCurrentCell(int cell)
{
  //clean up old cell
  CleanOldCell(cell);
  
  //draw the reconstructed cell edges for each lobe number
  DetermineReconstructedShape(cell);
  
  //fill up the cells 
  FillOriginalShape(cell);
  FillReconstructedShape(cell);
  
  //calculate the XOR
  Difference(cell);
}

int main(int argc,char *argv[])
{
  int lobes;
  FILE *fp;

  //read the data from file
  ReadData(argc,argv);

  if(!saveShapeAnalysis)
    return 0;
    
  //draw the segmented cell edges into the planes 
  FillOriginalDataPlanes();
  
  //calculate the modes of EFA and LOCOEFA
  CalculateReconstruction();
  
  //calculate entropy values
  Entropy();
   
  //up to here no data in 4D array, so that's OK
  //instead of tuly allocating a 4D array we will be recycling the allocated cell planes
  CELLS(originalcells,
	UpdateCurrentCell(c);
	);
  
  //Cumulative difference [x N]; where x=2,3,4 N=inoofharmonicstoanalyze
  CumulativeDifference();

  AsignCellProperties(argc,argv);
  SavePictures();
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell characterisitcs: Output_CellShapeAnalysis/%s\n",outputfilename);
    exit(EXIT_FAILURE);
  }

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  //The data of each cell is in each row	  
	  fprintf(fp,"Celltype\tTimepoint\tOldsigma\tCellarea\tContourlength\tLocoefamodes(N=%d)\tCumulativedifference\tEntropy\tXOR_Original_Reconstructed(N=%d)\tLnumbers(N=%d)\n",locoefamodes+1,locoefamodes+1,locoefamodes+1);
	  fprintf(fp,"%d\t%d\t%d\t%d\t%f\t%d\t%f\t%f",originalcells.celltype[c],EXTRAS[c].timepoint,EXTRAS[c].oldsigma,originalcells.area[c],originalcells.contour[c][originalcells.contourlength[c]].t,locoefamodes+1,EXTRAS[c].cumulativedifference,EXTRAS[c].entropy);
	  for(lobes=1;lobes<locoefamodes+2;lobes++)
	    fprintf(fp,"\t%g",difference[c][lobes]);
	  for(lobes=1;lobes<locoefamodes+2;lobes++)
	    fprintf(fp,"\t%g",originalcells.locoefa[c][lobes].locoL);
	  fprintf(fp,"\n");
	}
	);
  fclose(fp);
  printf("\t...done.\n");
  printf("stacksize %d\t totalcells %d\n",stacksize, totalcells);
  return 0;
}
