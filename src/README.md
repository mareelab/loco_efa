# Src

This directory is intended to contain all source code (c-code etc), that has to be compiled before running.

* `cellshapeanalysis.c` : code to calculate *Ln* and other LOCO-EFA metrics
* `cellshapeanalysis_compare_locoefa_efa.c`: code that outputs *Pn*,
  without or with normalisation as proposed in [Diaz et al. 1990](http://www.ncbi.nlm.nih.gov/pubmed/2311369)
* `displaycellproperties.c`: code to display the measured shape
  properties within the images
* `bukowski.c`: code to add bars to images
* `playcolor_07.c`: code to generate colorbars
* `loco-efa_example.c`: the core LOCO-EFA code, as provided with the
paper
* `loco-efa_example-with-additional-checks.c`: same code with some
  additional checks
* `loco-efa_example-compact-code.c`: compact version of the code,
which also shows reciprocal retrieval of  contour points from LOCO-EFA
quadruplets







