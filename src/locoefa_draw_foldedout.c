#include <minexcal.h>

static Cell cells;
static int T;
static int subT;

extern int grace;
extern int locoefamodes;

/********************************************functions*/

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  ReadOptions(name);
  MyInDat(name,"%d","T",&T); 
  MyInDat(name,"%d","subT",&subT);
}

int main(int argc,char *argv[])
{
  double xvalue,yvalue;
  double xvaluesubellipse,yvaluesubellipse;
  double xvaluestart1,yvaluestart1;
  char dirname[STRING];
  char a_string[STRING];
  int framenumber=0;
  int saveit=0;
  int k;
  int pathlength;
  int n,subn;
  double phase,subphase,tmplambda;
  int i;
  
  if(argc!=7 && argc!=8) {
    fprintf(stdout,"usage: %s parameterfile radiusplus_L3 radiusminus_L3 zetaplus_L3 zetaminus_L3 zetaplus_L1 [outputdir]\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  //read the pars
  fprintf(stdout,"reading parameters from %s\n",argv[1]);
  SetPars(argv[1]); 
  
  if(argc==8) { //there is a name to save the output in
    grace=0; //we are not allowed to redraw on the screen (because that messes up scaling in saved output, which is a reported bug in the grace lib)
    //grace bug #1176:
    //redrawing canvas in the free mode might in some cases result in modified graph viewport values (rep. #1176)

    saveit=1;

    //create the dir
    if(snprintf(dirname,STRING,"%s",argv[7])>=STRING) {
      fprintf(stderr,"warning: output name too long: %s\n",argv[7]);
      exit(EXIT_FAILURE);
    }
    pathlength=strlen(dirname);
    while(pathlength && dirname[pathlength-1]=='/') {
      dirname[pathlength-1]='\0';
      pathlength--;
    }
    for(k=pathlength-1;k>=0;k--) {
      if(k==0 || dirname[k-1]=='/') {
	dirname[k]=toupper(dirname[k]);
	break;
      }
    }
    snprintf(a_string,STRING,"mkdir -p %s",dirname);
    system(a_string);
  }

  CNew(&cells,1,1);
  InitLOCOEFA(&cells);

  cells.locoefa[0][3].locolambdaplus=atof(argv[2]);
  cells.locoefa[0][3].locolambdaminus=atof(argv[3]);
  cells.locoefa[0][3].locozetaplus=atof(argv[4])*M_PI/180.;
  cells.locoefa[0][3].locozetaminus=atof(argv[5])*M_PI/180.;
  cells.locoefa[0][1].locolambdaplus=1.;
  cells.locoefa[0][1].locozetaplus=atof(argv[6])*M_PI/180.;
  
  //note that the next line is normally done by CalculateLOCOCoefficients
  //Equation 47: L3
  cells.locoefa[0][3].locoL=sqrt(cells.locoefa[0][3].locolambdaplus*cells.locoefa[0][3].locolambdaplus+cells.locoefa[0][3].locolambdaminus*cells.locoefa[0][3].locolambdaminus+2.*cells.locoefa[0][3].locolambdaplus*cells.locoefa[0][3].locolambdaminus*cos(cells.locoefa[0][3].locozetaplus-cells.locoefa[0][3].locozetaminus-2.*cells.locoefa[0][1].locozetaplus));
  
  //set up grace
  ColorTable(0,8,1,0,2,3,4,5,6,7,8);
  OpenGrace(3,1,NULL);
  //note that it looks misformed on the screen, but saved is OK, known bug
  Grace(GGRAPH,0);
  Grace(GVIEWPORT,0.07,0.82,0.97,0.9);
  Grace(GWORLD,-45.,-.31415927,405.,.31415927);
  Grace(GGRAPH,1);
  Grace(GVIEWPORT,0.07,0.62,0.97,0.7);
  Grace(GWORLD,-45.,-.31415927,405.,.31415927);
  Grace(GGRAPH,2);
  Grace(GVIEWPORT,0.07,0.38,0.97,0.5);
  Grace(GWORLD,-45.,-.4712389,405.,.4712389);

  for(i=0;i<3;i++) {
    Grace(GGRAPH,i);
    Grace(GAXIS,'x');
    Grace(GLABEL,"phase");
    GracePrintf("xaxis tick minor ticks 0");
    GracePrintf("xaxis tick major 45");
    GracePrintf("xaxis tick major size 0.35");
    Grace(GAXIS,'y');
    GracePrintf("yaxis tick minor ticks 0");
    GracePrintf("yaxis tick major 0.2");
    GracePrintf("yaxis tick major size 0.35");
    Grace(GSET,2);
    Grace(GLINE,GCOLOR,ColorName(102,102,"green"));
    Grace(GLINE,GWIDTH,3.);
    Grace(GSET,3);
    Grace(GLINE,GCOLOR,ColorName(103,103,"orange"));
    Grace(GLINE,GWIDTH,3.);
    Grace(GSET,4);
    Grace(GLINE,GCOLOR,ColorName(103,103,"red"));
    Grace(GLINE,GWIDTH,3.);
  }
    
  for(n=0;n<=T;n++) {
    Grace(GGRAPH,0);
    tmplambda=cells.locoefa[0][3].locolambdaplus;
    cells.locoefa[0][3].locolambdaplus=0;
    ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,1,&xvaluestart1,&yvaluestart1);
    ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,3,&xvalue,&yvalue);
    phase=180./M_PI*atan2(yvalue,xvalue);
    if(n>20 && phase<EPSILON)
      phase+=360.;
    if(n>350 && phase<45.)
      phase+=360.;
    Grace(GSET,2);
    Grace(GPOINT,2,phase,hypot(xvalue,yvalue)-1.);
    if(!(n%(T/8))) {
      Grace(GSET,10+n/(T/8));
      Grace(GLINE,GCOLOR,ColorName(102,102,"deep sky blue"));
      Grace(GLINE,GWIDTH,2.);
      for(subn=0;subn<=subT;subn++) { //the circles
	ReconstructContourpoint(&cells,0,(double)subn/(double)subT,LOCOEFA,3,3,&xvaluesubellipse,&yvaluesubellipse);
	xvaluesubellipse+=xvaluestart1;
	yvaluesubellipse+=yvaluestart1;
	subphase=180./M_PI*atan2(yvaluesubellipse,xvaluesubellipse);
	if(n!=0 && subphase<EPSILON)
	  subphase+=360.;
	if(n==360 && subphase<45.)
	  subphase+=360.;
	Grace(GPOINT,2,subphase,hypot(xvaluesubellipse,yvaluesubellipse)-1.);
      }
      Grace(GSET,20+n/(T/8)); //the dot
      Grace(GSYMBOL,GCOLOR,ColorName(103,103,"dark blue"));
      Grace(GLINE,GSTYLE,0);
      Grace(GSYMBOL,GTYPE,1);
      Grace(GSYMBOL,GSIZE,0.4);
      Grace(GSYMBOL,GSTYLE,1);
      Grace(GSYMBOL,GPATTERN,1);
      Grace(GPOINT,2,phase,hypot(xvalue,yvalue)-1.);
    }
    cells.locoefa[0][3].locolambdaplus=tmplambda;

    Grace(GGRAPH,1);
    tmplambda=cells.locoefa[0][3].locolambdaminus;
    cells.locoefa[0][3].locolambdaminus=0;
    ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,1,&xvaluestart1,&yvaluestart1);
    ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,3,&xvalue,&yvalue);
    phase=180./M_PI*atan2(yvalue,xvalue);
    if(n>20 && phase<EPSILON)
      phase+=360.;
    if(n>350 && phase<45.)
      phase+=360.;
    Grace(GSET,4);
    Grace(GPOINT,2,phase,hypot(xvalue,yvalue)-1.);
    if(!(n%(T/8))) {
      Grace(GSET,10+n/(T/8));
      Grace(GLINE,GCOLOR,ColorName(102,102,"deep sky blue"));
      Grace(GLINE,GWIDTH,2.);
      for(subn=0;subn<=subT;subn++) { //the circles
	ReconstructContourpoint(&cells,0,(double)subn/(double)subT,LOCOEFA,3,3,&xvaluesubellipse,&yvaluesubellipse);
	xvaluesubellipse+=xvaluestart1;
	yvaluesubellipse+=yvaluestart1;
	subphase=180./M_PI*atan2(yvaluesubellipse,xvaluesubellipse);
	if(n!=0 && subphase<EPSILON)
	  subphase+=360.;
	if(n==360 && subphase<45.)
	  subphase+=360.;
	Grace(GPOINT,2,subphase,hypot(xvaluesubellipse,yvaluesubellipse)-1.);
      }
      Grace(GSET,20+n/(T/8)); //the dot
      Grace(GSYMBOL,GCOLOR,ColorName(103,103,"dark blue"));
      Grace(GLINE,GSTYLE,0);
      Grace(GSYMBOL,GTYPE,1);
      Grace(GSYMBOL,GSIZE,0.4);
      Grace(GSYMBOL,GSTYLE,1);
      Grace(GSYMBOL,GPATTERN,1);
      Grace(GPOINT,2,phase,hypot(xvalue,yvalue)-1.);
    }
    cells.locoefa[0][3].locolambdaminus=tmplambda;

    Grace(GGRAPH,2);
    ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,3,&xvalue,&yvalue);
    phase=180./M_PI*atan2(yvalue,xvalue);
    if(n>20 && phase<EPSILON)
      phase+=360.;
    if(n>350 && phase<45.)
      phase+=360.;
    Grace(GSET,3);
    Grace(GPOINT,2,phase,hypot(xvalue,yvalue)-1.);
    
    if(saveit) {
      //direct png
      /*
	snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	Grace(GPRINT,"PNG",a_string);
      */
      //eps
      snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
      Grace(GPRINT,"EPS",a_string);
      framenumber++;
    }
    else
      Grace(GREDRAW); 
  }
   
  if(argc==8) {
    snprintf(a_string,STRING,"%s/locoefa_draw_foldout.agr",dirname);
    Grace(GSAVE,a_string);
  }
  if(grace)
    CloseGrace(GDETACH);
  else
    CloseGrace(GCLOSE);
  return 0;
}
