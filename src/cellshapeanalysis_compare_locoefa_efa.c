//libraries
#include <minexcal.h>
//Reconstruct using EFA reconstruction
//Output the Pn contribution described in Diaz et al., instead of the Ln.
//Entropy calculated using Pn

typedef struct extras {
  int timepoint;
  int oldsigma;
  int minx;
  int maxx;
  int miny;
  int maxy;
  int xoffset;
  int yoffset;
  int width;
  int height;
  double cumulativedifference;
  double totalproportion;
  double entropy;
  double *arotate;
  double *brotate;
  double *crotate;
  double *drotate;
  double *pn;
  double *pn_norm;
  double *rot;
  double *ap;
  double *bp;
  double *cp;
  double *dp;
  double *majoraxis;
  double *minoraxis;
} Extras;

#define EXTRAS ((Extras*)(originalcells.extras))

static TYPE ***image;
static TYPE **state;
static TYPE **outline;
static TYPE ***original;
static TYPE ***reconstructed;
static TYPE ***xor;
static double **difference;
static double **marginaldifference;

static int nooftimepoints;
static int totalcelltypes;
static Cell cells;
static Cell originalcells;
static Cell reconstructedcells;
static int totalcells=0;
static char outputdirname[STRING];
static char outputfilename[STRING];
static int maxcells=0;
static int ncells=0;
static int currentz;
static int stacksize;
static int biggestCellOnly;
static int excludeEdgeCells;
static int saveAverages;
static int saveCellData;
static int saveCellPositions;
static int saveCellOutlines;
static int saveShapeAnalysis;
static int saveReconstruction;
static Png reconstructedpng,xorpng;
static int pnnorm;

//general
extern int (*newindex)(int,int);
extern TYPE boundaryvalue;
extern int locoefamodes;
extern int nrow;
extern int ncol;
extern int specialcelltype;

/********************************************functions*/

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  ReadOptions(name);
  MyInDat(name,"%d","inoofharmonicsanalyse",&locoefamodes);
  MyInDat(name,"%d","biggestCellOnly",&biggestCellOnly); 
  MyInDat(name,"%d","excludeEdgeCells",&excludeEdgeCells); 
  MyInDat(name,"%d","saveAverages",&saveAverages);
  MyInDat(name,"%d","saveCellData",&saveCellData);
  MyInDat(name,"%d","saveCellPositions",&saveCellPositions);
  MyInDat(name,"%d","saveCellOutlines",&saveCellOutlines);
  MyInDat(name,"%d","saveShapeAnalysis",&saveShapeAnalysis);
  MyInDat(name,"%d","saveReconstruction",&saveReconstruction);
  MyInDat(name,"%d","totalcelltypes",&totalcelltypes);
  MyInDat(name,"%d","pnnorm",&pnnorm);
}

int DetermineTotalNoOfCells(int ac,char *av[])
{
  int k,c,tmpncol=0,tmpnrow=0;
  TYPE **totaldifferentcells;
  int cellnumber=0;

  //read size of image(s)
  stacksize=ac-3;
  nooftimepoints=stacksize;

  for(k=3;k<ac;k++) {
    ReadSizePNG(&nrow,&ncol,av[k]);
    tmpnrow=max(tmpnrow,nrow);
    tmpncol=max(tmpncol,ncol);
  }
  nrow=tmpnrow;
  ncol=tmpncol;  

  //allocate planes and colors
  image = (TYPE***)calloc((size_t)stacksize,sizeof(TYPE**));
  if (image == NULL) {
    fprintf(stderr,"DetermineTotalNoOfCells: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for(k=0;k<stacksize;k++)
    image[k]=New();

  state=New();
  outline=New();

  //read image(s)
  for(k=3;k<ac;k++) {
    ReadSizePNG(&nrow,&ncol,av[k]);
    ReadPNG(image[k-3],1,1,av[k]);
  }
  nrow=tmpnrow;
  ncol=tmpncol;  
  fprintf(stdout,"max nrow: %d, max ncol: %d\n",nrow,ncol);

  //determine ncells
  ncells=0;
  for(k=0;k<stacksize;k++) {
    ReadSizePNG(&nrow,&ncol,av[k+3]);
    PLANE(ncells=max(ncells,image[k][i][j]););
  }
  nrow=tmpnrow;
  ncol=tmpncol;  
  
  if(!ncells) {
    fprintf(stdout,"Warning: no cells in images, error in SetUp function!\n");
    exit(EXIT_FAILURE); 
  }
  maxcells=ncells+1;
  fprintf(stdout,"max cells in image(s): %d\n",maxcells);
  ncells=0;

  //what is highest number of cells?
  totaldifferentcells=NewPlane(stacksize,maxcells-1);
  //register each cell that exists
  for(k=0;k<stacksize;k++) {
    PLANE(
	  if(image[k][i][j])
	    totaldifferentcells[k][image[k][i][j]]=1;
	  );
  }
  
  for(k=0;k<stacksize;k++)
    for(c=1;c<maxcells;c++)
      if(totaldifferentcells[k][c]) {
	cellnumber++;
      }
  
  //allocate cells
  CNew(&cells,maxcells,totalcelltypes);
  InitCellPosition(&cells);
  InitLOCOEFA(&cells);

  return cellnumber;
}

void UpdateCells()
{
  fprintf(stdout,"Initiating cells...\n");
  UpdateCellPosition(state,&cells);
  
  ncells=0;
  CELLS(cells,
	if(c>specialcelltype && cells.area[c])
	  ncells=c;
	);
  fprintf(stdout,"\t...done, %d cells\n",ncells);
}

void SelectCells()
{
  int largestcell=0;
  int largestarea=0;
  fprintf(stdout,"Selecting cells...\n");
  
  CELLS(cells,
	if(c<=specialcelltype) {
	  cells.contourlength[c]=0;
	}
	);

  if(excludeEdgeCells) {
    PLANE(
	  if(state[i][j] && (i==1 || i==nrow || j==1 || j==ncol))
	    cells.contourlength[state[i][j]]=0;
	  );
  }
  
  if(biggestCellOnly) {
    CELLS(cells,
	  if(cells.contourlength[c]) {
	    if(cells.area[c]>largestarea) {
	      largestcell=c;
	      largestarea=cells.area[c];
	    }
	  }
	  );
    CELLS(cells,
	  if(c!=largestcell) {
	    cells.contourlength[c]=0;
	  }
	  );
  }
  
  ncells=0;
  CELLS(cells,
	if(cells.contourlength[c]) {
	  ncells++;
	}
	);
  fprintf(stdout,"\t...done, %d cells selected in SelectCells.\n",ncells);
}
   
void SaveAverages()
{
  FILE *fp;
  double average_cellsize=0.;
  
  fprintf(stdout,"ncells: %d\n",ncells);
  CELLS(cells,
	if(cells.contourlength[c])
	  average_cellsize+=(double)cells.area[c];
	);
  average_cellsize/=(double)ncells;
  fprintf(stdout,"average cell size: %f\n",average_cellsize);
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write average cell characterisitcs: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing overall cell characteristics...\n");
  fprintf(fp,"Average cell characteristics\n\n");
  fprintf(fp,"Number of cells: %d\n",ncells);
  fprintf(fp,"Average cells size: %f\n",average_cellsize);		
  fprintf(fp,"\n\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellData()
{
  FILE *fp;
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell characterisitcs: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell characterisitcs...\n");
  fprintf(fp,"Cell data\n\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  fprintf(fp,"Cell: %d\n",c);
	  fprintf(fp,"size: %d\n",cells.area[c]);
	  fprintf(fp,"meanx: %f\n",cells.shape[c].meanx);
	  fprintf(fp,"meany: %f\n",cells.shape[c].meany);
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellPositions()
{
  FILE *fp;
    
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell position: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell positions...\n");
  fprintf(fp,"Indivdual cell positions\n\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  fprintf(fp,"Surface cell %d\n",c);
	  PLANE(
		if(state[i][j]==c)
		  fprintf(fp,"%d\t%d\n",j,i););
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void SaveCellOutlines()
{
  FILE *fp;
  int k,newsigma;
  double d_centre;
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open data-file: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Writing individuall cell outlines...\n");
  CELLS(cells, 
	if(cells.contourlength[c]) {
	  //find last added cell with that number
	  newsigma=totalcells;
	  while(EXTRAS[newsigma].oldsigma!=c)
	    newsigma--;
	  fprintf(fp,"Outline cell %d\n",c);
	  fprintf(fp,"Index\tX-position\tY-position\tLength\tRel.Length\tDistance.Center\tArea\n");
	  for(k=0;k<=originalcells.contourlength[newsigma];k++) {
	    d_centre=hypot(cells.shape[c].meany-originalcells.contour[newsigma][k].y,cells.shape[c].meanx-originalcells.contour[newsigma][k].x);
	    fprintf(fp,"%d\t%d\t%d\t%f\t%f\t%f\t%d\n",k,(int)originalcells.contour[newsigma][k].x,(int)originalcells.contour[newsigma][k].y,originalcells.contour[newsigma][k].t,originalcells.contour[newsigma][k].t/originalcells.contour[newsigma][originalcells.contourlength[newsigma]].t,d_centre,cells.area[c]);
	  }
	  fprintf(fp,"\n");
	}
	);
  fprintf(fp,"\n");		
  fclose(fp);
  fprintf(stdout,"\t...done.\n");
}

void AssignPerimeter(int celltype,int timepoint,int oldsigma)
{
  int i;

  totalcells++;
  originalcells.celltype[totalcells]=celltype;
  if(originalcells.celltype[totalcells]>=originalcells.maxtypes) {
    fprintf(stderr,"celltype larger or equal than totalcelltypes (%d>=%d). Exitting now!\n",originalcells.celltype[totalcells],originalcells.maxtypes);
    exit(EXIT_FAILURE);
  }

  EXTRAS[totalcells].timepoint=timepoint;
  EXTRAS[totalcells].oldsigma=oldsigma;
  originalcells.area[totalcells]=cells.area[oldsigma];

  if(((difference[totalcells]=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((marginaldifference[totalcells]=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].arotate=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].brotate=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].crotate=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].drotate=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].ap=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].bp=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].cp=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].dp=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].pn=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].pn_norm=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].rot=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].minoraxis=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL) ||
     ((EXTRAS[totalcells].majoraxis=(double*)calloc((size_t)(locoefamodes+2),sizeof(double)))==NULL)) {
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  InitLOCOEFAContour(&originalcells,totalcells,cells.contourlength[oldsigma]);
  
  for(i=0;i<=cells.contourlength[oldsigma];i++) {
    originalcells.contour[totalcells][i].x=cells.contour[oldsigma][i].x;
    originalcells.contour[totalcells][i].y=cells.contour[oldsigma][i].y;

    if(i==0) {//first datapoint
      EXTRAS[totalcells].minx=originalcells.contour[totalcells][i].x;
      EXTRAS[totalcells].maxx=originalcells.contour[totalcells][i].x;
      EXTRAS[totalcells].miny=originalcells.contour[totalcells][i].y;
      EXTRAS[totalcells].maxy=originalcells.contour[totalcells][i].y;
    }
    else {
      EXTRAS[totalcells].minx=min(EXTRAS[totalcells].minx,originalcells.contour[totalcells][i].x);
      EXTRAS[totalcells].maxx=max(EXTRAS[totalcells].maxx,originalcells.contour[totalcells][i].x);
      EXTRAS[totalcells].miny=min(EXTRAS[totalcells].miny,originalcells.contour[totalcells][i].y);
      EXTRAS[totalcells].maxy=max(EXTRAS[totalcells].maxy,originalcells.contour[totalcells][i].y);
    }
  }

  EXTRAS[totalcells].width=EXTRAS[totalcells].maxx-EXTRAS[totalcells].minx+1;
  EXTRAS[totalcells].height=EXTRAS[totalcells].maxy-EXTRAS[totalcells].miny+1;
}

void RunOutline2D(int ac,char *av[])
{
  FILE *fp;

  for (currentz=0;currentz<stacksize;currentz++) {
    ReadSizePNG(&nrow,&ncol,av[currentz+3]);
    PLANE(
	  state[i][j]=0;
	  outline[i][j]=0;
	  );
    PLANE(
	  state[i][j]=image[currentz][i][j];
	  );
    UpdateCFill(state,&cells);
    UpdateCellContour(outline,state,&cells);
    CELLS(cells,
	  if(cells.contourlength[c])
	    fprintf(stdout,"cell %d length %d\n",c,cells.contourlength[c]);
	  );

    UpdateCells();
    SelectCells();
    
    CELLS(cells, 
	  if(cells.contourlength[c]) {
	    AssignPerimeter(1 /*celltype, currently always 1*/,currentz /*timepoint*/,c /*sigma*/);
	    CalculateEFACoefficients(&originalcells,totalcells);
	  }
	  );
    
    if((fp=fopen(outputfilename,"a"))==NULL) {
      fprintf(stderr,"warning: could not open data-file: %s\n",outputfilename);
      exit(EXIT_FAILURE);
    }
    
    if(ncells) {
      fprintf(fp,"Data for cells in: %s\n\n",av[currentz+3]);
      fclose(fp);
      if(saveAverages)
	SaveAverages();
      if(saveCellData)
	SaveCellData();
      if(saveCellPositions)
	SaveCellPositions();
      if(saveCellOutlines)
	SaveCellOutlines();
    }
    else {
      fprintf(fp,"No cells selected in: %s\n\n",av[currentz+3]);
      fclose(fp);
    }
  }   
}

void ReadData(int ac,char *av[])
{
  FILE *fp;
  int i;
  int maxwidth=0,maxheight=0;
  char a_string[STRING];

  //check # of arguments
  if(ac<4) {
    fprintf(stdout,"usage: %s parfile output_dirname [segmented images]\n",av[0]);
    exit(EXIT_FAILURE);
  }

  //create outputdir
  if(snprintf(outputdirname,STRING,"Output_CellShapeAnalysis/%s",av[2])>=STRING) {
    outputdirname[0]=toupper(outputdirname[0]);
    fprintf(stderr,"warning: outputdirname too long: %s\n",outputdirname);
  }
  outputdirname[0]=toupper(outputdirname[0]);
  snprintf(a_string,STRING,"mkdir -p %s",outputdirname);
  system(a_string);

  //create outputfilename
  if(snprintf(outputfilename,STRING,"%s/%s.dat",outputdirname,av[2])>=STRING){
    fprintf(stderr,"warning: filename for log too long: %s\n",outputfilename);
  }

  //writing outputfilename header
  if((fp=fopen(outputfilename,"w"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write header: %s\n",outputfilename);
    exit(EXIT_FAILURE);
  }
  fprintf(fp,"data analyzed with %s\n",av[0]);
  fclose(fp);
  
  //read parameters
  fprintf(stdout,"reading parameters from %s\n",av[1]);
  SetPars(av[1]); 
  
  if(snprintf(a_string,STRING,"%s/%s.par",outputdirname,av[2])>=STRING)
    fprintf(stderr,"warning: parfile name too long: %s\n",a_string);
  else
    SaveOptions(av[1],a_string);
  
  //now we need to know how many cells we will have, as well as setting of nooftimepoints
  //this will be overestimation of totalcells, since each picture will contain cells that are not valid
  //we therefore keep the more serious allocation to later on, limited to cells we are really interested in

  //so first time (after DetermineTotalNoOfCells) totalcells will be bigger than second time (after RunOutline2D)
  totalcells=DetermineTotalNoOfCells(ac,av);
  
  CNew(&originalcells,totalcells+1,totalcelltypes); //maxsigma+1 because we have to count 0 as well
  CNew(&reconstructedcells,totalcells+1,totalcelltypes); //maxsigma+1 because we have to count 0 as well
  if((originalcells.extras=(void *)calloc((size_t)originalcells.maxcells,sizeof(Extras)))==NULL) {
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }

  InitLOCOEFA(&originalcells);

  //two reasons why each cell in different plane:
  //1) much more easy to determine inside/outside of cell
  //2) reconstruction of different cells could overlap
  //but now we do it one after another instead all all concurrently
  //as it is too expensive 
  if(((original=(TYPE***)calloc((size_t)originalcells.maxcells,sizeof(TYPE**)))==NULL) ||
     ((difference=(double**)calloc((size_t)originalcells.maxcells,sizeof(double*)))==NULL) ||
     ((marginaldifference=(double**)calloc((size_t)originalcells.maxcells,sizeof(double*)))==NULL) ||
     ((reconstructed=(TYPE***)calloc((size_t)(locoefamodes+2),sizeof(TYPE**)))==NULL) ||
     ((xor=(TYPE***)calloc((size_t)(locoefamodes+2),sizeof(TYPE**)))==NULL)) { 
    
    fprintf(stderr,"ReadData: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  
  totalcells=0;
  //now really read in
  RunOutline2D(ac,av);
  
  //how big do we need it?
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  if(EXTRAS[c].width>maxwidth)
	    maxwidth=EXTRAS[c].width;
	  if(EXTRAS[c].height>maxheight)
	    maxheight=EXTRAS[c].height;
	}
	);
 
  fprintf(stdout,"maxwidth %d maxheight %d\n",maxwidth,maxheight);
  //let's make the planes twice as large as the longest axis. 
  nrow=2*max(maxwidth,maxheight);
  ncol=nrow;
  fprintf(stdout,"ncol %d and nrow %d\n", ncol,nrow);
 
  //how much sticks out on each side?
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  EXTRAS[c].xoffset=(ncol-EXTRAS[c].width)/2-EXTRAS[c].minx+1;
	  EXTRAS[c].yoffset=(nrow-EXTRAS[c].height)/2-EXTRAS[c].miny+1;
	  original[c]=New();
	}
	);
  for(i=0;i<locoefamodes+2;i++) {
    reconstructed[i]=New();
    xor[i]=New();
  }
} 

void FillOriginalDataPlanes()
{
  int k;
  CELLS(originalcells,
	for(k=0;k<originalcells.contourlength[c];k++)
	  original[c][(EXTRAS[c].yoffset+(int)originalcells.contour[c][k].y)][(EXTRAS[c].xoffset+(int)originalcells.contour[c][k].x)]=c;
	);
  fprintf(stdout,"debug: FillOriginalDataPlanes OK\n");
}

int in_plane(int i, int j)
{
  if ((i > 1) && (i+1 < nrow) && (j > 1) && (j+1 < ncol))
    return 1;
  return 0;
}

int should_be_boundary(TYPE **a, int i, int j)
{
  if (!in_plane(i, j))
    return 0;
  if ((a[i][j] == 0) && (a[i-1][j]==-1 || a[i+1][j]==-1 || a[i][j-1]==-1 || a[i][j+1]==-1))
    return 1;
  else return 0;
}

void flip_if_needed(TYPE **a, int i, int j)
{
  if(should_be_boundary(a, i, j))
    a[i][j] = -1;
}

void ExtendBoundary(TYPE **a, int i, int j)
{
  int ni, nj;
  //fprintf(stdout,"Extending at %d, %d (%d, %d)\n", i, j, nrow, ncol);
  a[i][j] = -1;
  
  for(ni=i;ni<nrow;ni++)
    flip_if_needed(a, ni, j);
  for(ni=i;ni>0;ni--)
    flip_if_needed(a, ni, j);
  for(nj=j;nj<ncol;nj++)
    flip_if_needed(a, i, nj);
  for(nj=j;nj>0;nj--)
    flip_if_needed(a, i, nj);
  
  /*
    if (should_be_boundary(a, i-1, j)) ExtendBoundary(a, i-1, j);
    if (should_be_boundary(a, i+1, j)) ExtendBoundary(a, i+1, j);
    if (should_be_boundary(a, i, j+1)) ExtendBoundary(a, i, j+1);
    if (should_be_boundary(a, i, j-1)) ExtendBoundary(a, i, j-1);
  */
}

void FillShape(TYPE **a,int c)
{
  boundaryvalue=-1;
  Boundaries(a);
  //fprintf(stdout,"debug: FillShape cell %d\n",c);
  PLANE(
	if(!a[i][j] && (a[i-1][j]==-1 || a[i+1][j]==-1 || a[i][j-1]==-1 || a[i][j+1]==-1))
	  ExtendBoundary(a, i, j);
	);
  PLANE(
	if(!a[i][j])//pixel has not been changed into -1, so this is inside cell
	  a[i][j]=c;
	else if(a[i][j]==-1)//pixel has been changed into 0, so this is outside cell
	  a[i][j]=0;
	);
  boundaryvalue=0;
  Boundaries(a);
}

void FillOriginalShape(int cell)
{
  //  CELLS(originalcells,
  if(originalcells.contourlength[cell])
    FillShape(original[cell],cell);
  //	);
  fprintf(stdout,"OK FillOriginalShape of cell %d\n",cell); 
}

void FillReconstructedShape(int cell)
{
  int lobes;

  //  CELLS(originalcells,
  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"filling in cell #%d of %d\n",cell,totalcells);
    for(lobes=1;lobes<locoefamodes+2;lobes++) {
      FillShape(reconstructed[lobes],cell);
    }
  }
  //	);
}

void MindTheGap(int c,int lobes,int oldx,int x,int oldy,int y,double oldt,double t,int level)
{
  double xpoint,ypoint;
  int newx,newy;

  ReconstructContourpoint(&originalcells,c,(oldt+t)/2.,EFA,0,lobes,&xpoint,&ypoint);
  newx=(int)nearbyint(xpoint);
  newy=(int)nearbyint(ypoint);
  
  reconstructed[lobes][newindex(newy+EXTRAS[c].yoffset,nrow)][newindex(newx+EXTRAS[c].xoffset,ncol)]=c;

  if(level>1)
    fprintf(stdout,"mind the gap: %d %d %d (%d,%d) (%d,%d) (%d,%d) %lf %lf\n",level,c,lobes,oldx,oldy,newx,newy,x,y,oldt,t);

  if((newx-oldx)*(newx-oldx)+(newy-oldy)*(newy-oldy)>2)
    MindTheGap(c,lobes,oldx,newx,oldy,newy,oldt,(oldt+t)/2.,level+1);
  if((x-newx)*(x-newx)+(y-newy)*(y-newy)>2)
    MindTheGap(c,lobes,newx,x,newy,y,(oldt+t)/2.,t,level+1);
}

void DetermineReconstructedShape(int cell)
{
  int i;
  int lobes;
  int x,y;
  double xpoint,ypoint;
  int oldx=0,oldy=0;
  
  //  CELLS(originalcells,
  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"reconstructing cell #%d (%d) of %d\n",cell,EXTRAS[cell].oldsigma,totalcells);
    for(lobes=1;lobes<locoefamodes+1;lobes++) {
      for(i=0;i<=originalcells.contourlength[cell];i++) {
	ReconstructContourpoint(&originalcells,cell,originalcells.contour[cell][i].t,EFA,0,lobes,&xpoint,&ypoint);
	x=(int)nearbyint(xpoint);
	y=(int)nearbyint(ypoint);
	reconstructed[lobes][newindex(y+EXTRAS[cell].yoffset,nrow)][newindex(x+EXTRAS[cell].xoffset,ncol)]=cell;
	if(i && ((x-oldx)*(x-oldx)+(y-oldy)*(y-oldy)>2)) {//dist squared more than one pixel
	  MindTheGap(cell,lobes,oldx,x,oldy,y,originalcells.contour[cell][i-1].t,originalcells.contour[cell][i].t,1);
	}
	oldx=x;
	oldy=y;
      }
    }
  }
  //	);
}

void CalculateReconstruction() //Calculate LOCOEFA coeffiencients
{
  int k;
  
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  CalculateLOCOCoefficients(&originalcells,c);
	  fprintf(stdout,"Debug L coeff for cell %d\n",c);
	  for(k=1;k<=locoefamodes+1;k++) {
	    fprintf(stdout,"L= %lf\n",originalcells.locoefa[c][k].locoL);
	  }
	}
	);
}

void Pncoefficients() //As proposed in Diaz et al., 1990. Recognition of cell surface modulation by elliptic Fourier analysis
{
  int k;
  double tau, starttau;
  
  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  //normalize coeff for starting point of rotation. This will bring the coefficients at one extreme of the semimajor axis of the first ellipse.		 		
	  k=1;
	  tau=atan2(2.*(originalcells.locoefa[c][k].alpha*originalcells.locoefa[c][k].beta+originalcells.locoefa[c][k].gamma*originalcells.locoefa[c][k].delta),originalcells.locoefa[c][k].alpha*originalcells.locoefa[c][k].alpha+originalcells.locoefa[c][k].gamma*originalcells.locoefa[c][k].gamma-originalcells.locoefa[c][k].beta*originalcells.locoefa[c][k].beta-originalcells.locoefa[c][k].delta*originalcells.locoefa[c][k].delta)/2.; 
	  	  
	  //rotating ellipse tau brings semimajor axis to starting point. The rest of coefficients corrected for starting point are: 
	  //Kuhl, cgip82 
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].arotate[k]= cos((double)k*tau)*originalcells.locoefa[c][k].alpha+sin((double)k*tau)*originalcells.locoefa[c][k].beta;
	    EXTRAS[c].brotate[k]=-sin((double)k*tau)*originalcells.locoefa[c][k].alpha+cos((double)k*tau)*originalcells.locoefa[c][k].beta;
	    EXTRAS[c].crotate[k]= cos((double)k*tau)*originalcells.locoefa[c][k].gamma+sin((double)k*tau)*originalcells.locoefa[c][k].delta;
	    EXTRAS[c].drotate[k]=-sin((double)k*tau)*originalcells.locoefa[c][k].gamma+cos((double)k*tau)*originalcells.locoefa[c][k].delta;
	   }
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].ap[k]=EXTRAS[c].arotate[k];
	    EXTRAS[c].bp[k]=EXTRAS[c].brotate[k];
	    EXTRAS[c].cp[k]=EXTRAS[c].crotate[k];
	    EXTRAS[c].dp[k]=EXTRAS[c].drotate[k];
	  }
		
	  //Normalization by rotation brings the semi-major axis parallel to the x-axis
	  starttau=atan2(EXTRAS[c].cp[1],EXTRAS[c].ap[1]);
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].arotate[k]=cos(starttau)*EXTRAS[c].ap[k]+sin(starttau)*EXTRAS[c].cp[k];
	    EXTRAS[c].brotate[k]=cos(starttau)*EXTRAS[c].bp[k]+sin(starttau)*EXTRAS[c].dp[k];
	    EXTRAS[c].crotate[k]=-sin(starttau)*EXTRAS[c].ap[k]+cos(starttau)*EXTRAS[c].cp[k];
	    EXTRAS[c].drotate[k]=-sin(starttau)*EXTRAS[c].bp[k]+cos(starttau)*EXTRAS[c].dp[k];
	  }
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].ap[k]=EXTRAS[c].arotate[k];
	    EXTRAS[c].bp[k]=EXTRAS[c].brotate[k];
	    EXTRAS[c].cp[k]=EXTRAS[c].crotate[k];
	    EXTRAS[c].dp[k]=EXTRAS[c].drotate[k];
	  }
		
	  //Normalization by size
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].ap[k]/=sqrt(originalcells.area[c]);
	    EXTRAS[c].bp[k]/=sqrt(originalcells.area[c]);
	    EXTRAS[c].cp[k]/=sqrt(originalcells.area[c]);
	    EXTRAS[c].dp[k]/=sqrt(originalcells.area[c]);
	  }
		
	  //This is just the shift of the whole cell, just for the reconstruction
	  EXTRAS[c].ap[0]=originalcells.locoefa[c][0].alpha;
	  EXTRAS[c].cp[0]=originalcells.locoefa[c][0].gamma;
	    
	  //Major and minor axis: they can be calculated just after moving the starting point to one extreme.
	  for(k=1;k<=locoefamodes;k++) {
	    EXTRAS[c].majoraxis[k] = sqrt(EXTRAS[c].ap[k]*EXTRAS[c].ap[k] + EXTRAS[c].cp[k]*EXTRAS[c].cp[k]);
	    EXTRAS[c].minoraxis[k] = sqrt(EXTRAS[c].bp[k]*EXTRAS[c].bp[k] + EXTRAS[c].dp[k]*EXTRAS[c].dp[k]);	
	  }	
		
	  // The Pn is the perimeter of each harmonic multuplied by its harmonic number k.
	  for(k=1;k<=locoefamodes;k++) {		
	    EXTRAS[c].pn[k] = k*2*M_PI*sqrt((EXTRAS[c].majoraxis[k]*EXTRAS[c].majoraxis[k] + EXTRAS[c].minoraxis[k]*EXTRAS[c].minoraxis[k])/2.);
	  }
     	
	  //Direction of rotation is given by the determinant of the abcd matrix
	  for(k=1;k<=locoefamodes;k++) {	
	    EXTRAS[c].rot[k] = EXTRAS[c].ap[k]*EXTRAS[c].dp[k] - EXTRAS[c].bp[k]*EXTRAS[c].cp[k];
	    fprintf(stdout,"direction of rotation= %lf\n",EXTRAS[c].rot[k]);
	  }
	  
	  //Shift the k Pn-contribution to k-1 or k+1 position depending on the direction of rotation with respect to the first harmonic. 
	  for(k=2;k<=locoefamodes-1;k++) { //If the direction of rotation of the first harmonic is the same than the k harmonics, shift the position n-1
	    if (EXTRAS[c].rot[1]>0 && EXTRAS[c].rot[k]>0) {
	      EXTRAS[c].pn_norm[k-1] = EXTRAS[c].pn[k] + EXTRAS[c].pn_norm[k-1];	
	    }
	    else if (EXTRAS[c].rot[1]<0 && EXTRAS[c].rot[k]<0) {
	      EXTRAS[c].pn_norm[k-1] = EXTRAS[c].pn[k] + EXTRAS[c].pn_norm[k-1];     	
	    }
	    //If the direction of rotation of the first harmonic is the opposite than the k harmonics, shift the position n+1
	    else if (EXTRAS[c].rot[1]>0 && EXTRAS[c].rot[k]<0) {
	      EXTRAS[c].pn_norm[k+1] = EXTRAS[c].pn[k] + EXTRAS[c].pn_norm[k+1];    	
	    }
	    else if (EXTRAS[c].rot[1]<0 && EXTRAS[c].rot[k]>0) {
	      EXTRAS[c].pn_norm[k+1] = EXTRAS[c].pn[k] + EXTRAS[c].pn_norm[k+1];	
	    }
	    else { //If the determinant is zero
	      EXTRAS[c].pn_norm[k] = EXTRAS[c].pn[k];
	    }
	  }
	  
	  //The number 1
	  EXTRAS[c].pn_norm[1] = EXTRAS[c].pn[1];
	  
	  if(c==1) {
	    fprintf(stdout,"Pn coeff for cell %d,P\n",c);
	    for(k=1;k<=locoefamodes+1;k++) {
	      fprintf(stdout,"k=%d, Pn=%lf\n",k,EXTRAS[c].pn_norm[k]);
	    }
	  }
	}
	);
}

void Difference(int cell)
{
  int lobes;
  double cumulativedifference;
  char a_string[STRING];

  if(saveReconstruction) {
    ColorTable(0,1,WHITE,RED);
    ColorTable(cell,cell,RED);
  }
  
  if(originalcells.contourlength[cell]) {
    fprintf(stdout,"xor-ing cell #%d of %d\n",cell,totalcells);
    if(saveReconstruction) {
      snprintf(a_string,STRING,"%s/Reconstructed_%.5d",outputdirname,cell);
      OpenPNG(&reconstructedpng,a_string);
      snprintf(a_string,STRING,"%s/XOR_%.5d",outputdirname,cell);
      OpenPNG(&xorpng,a_string);
    }
    cumulativedifference=0;
    if(saveReconstruction) {
      PlanePNG(original[cell],&reconstructedpng,0);
      PlanePNG(original[cell],&xorpng,0);
    }
    for(lobes=1;lobes<locoefamodes+2;lobes++) {
      Xor(xor[lobes],original[cell],reconstructed[lobes]);
      //PLANE(if(xor[lobes][i][j]){fprintf(stdout,"lobo mal %d\n",xor[lobes][i][j]);break;});
      //multiply by c, because reconstructed contains values c,0,c,c, but xor contains 1,0,1,1 etc
      if(saveReconstruction) {
	PlanePNG(reconstructed[lobes],&reconstructedpng,0);
	PlanePNG(xor[lobes],&xorpng,0);
      }
      difference[cell][lobes]=(double)cell*(double)Total(xor[lobes])/(double)Total(original[cell]);
      cumulativedifference+=difference[cell][lobes];
      marginaldifference[cell][lobes]=difference[cell][lobes-1]-difference[cell][lobes];
      // if(marginaldifference[c][lobes]<=0) marginaldifference[c][lobes]=EPSILON;
      fprintf(stdout,"difference #%d of %d\t lobe %d\tdiff %f\tmarg %f\tculm %f\n",cell,totalcells,lobes,difference[cell][lobes], marginaldifference[cell][lobes],cumulativedifference);
    }
  }
}

void CumulativeDifference() //Cumulative difference [x N]; where x=2,3,4 N=inoofharmonicstoanalyze
{
  int lobes;

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  for(lobes=2;lobes<locoefamodes+2;lobes++)
	    EXTRAS[c].cumulativedifference+=difference[c][lobes];
	  fprintf(stdout,"Cell %d CumulativeDifference %f\n",c,EXTRAS[c].cumulativedifference);
  	}
	);
}

void Entropy() //Taking the Pn landscape
{
  int lobes;

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  EXTRAS[c].entropy=0;
	  for(lobes=1;lobes<=locoefamodes;lobes++) {
	    EXTRAS[c].totalproportion+=EXTRAS[c].pn_norm[lobes];
	  }
	  for(lobes=1;lobes<=locoefamodes;lobes++) {
	    if(EXTRAS[c].pn_norm[lobes]>EPSILON)
	      EXTRAS[c].entropy+=-((EXTRAS[c].pn_norm[lobes]/EXTRAS[c].totalproportion)*(double)log(EXTRAS[c].pn_norm[lobes]/EXTRAS[c].totalproportion));
	  }
	}
	);
  //fprintf(stdout,"Entropy %f\n",EXTRAS[c].entropy); 
}

void CleanOldCell(int cell)
{
  //This function makes sure the 'space' is cleaned before drawing the next cell.
  int k;
  for(k=0;k<locoefamodes+2;k++) {
    Fill(reconstructed[k],0);
    Fill(xor[k],0);
  }
}

void UpdateCurrentCell(int cell)
{
  //clean up old cell
  CleanOldCell(cell);
  
  //draw the reconstructed cell edges for each lobe number
  DetermineReconstructedShape(cell);
  
  //fill up the cells 
  FillOriginalShape(cell);
  FillReconstructedShape(cell);
  
  //calculate the XOR
  Difference(cell);
}

int main(int argc,char *argv[])
{
  int lobes;
  FILE *fp;

  //read the data from file
  ReadData(argc,argv);

  if(!saveShapeAnalysis)
    return 0;
    
  //draw the segmented cell edges into the planes 
  FillOriginalDataPlanes();
  
  //calculate the modes of EFA and LOCOEFA
  CalculateReconstruction();
  
  //calculate the Pn coefficients
  Pncoefficients();

  //calculate entropy values
  Entropy();
   
  //up to here no data in 4D array, so that's OK
  //instead of tuly allocating a 4D array we will be recycling the allocated cell planes
  CELLS(originalcells,
	UpdateCurrentCell(c);
	);
  
  //Cumulative difference [x N]; where x=2,3,4 N=inoofharmonicstoanalyze
  CumulativeDifference();
  
  if((fp=fopen(outputfilename,"a"))==NULL) {
    fprintf(stderr,"warning: could not open a file to write individual cell characterisitcs: Output_CellShapeAnalysis/%s\n",outputfilename);
    exit(EXIT_FAILURE);
  }

  CELLS(originalcells,
	if(originalcells.contourlength[c]) {
	  //The data of each cell is in each row	  
	  fprintf(fp,"Celltype\tTimepoint\tOldsigma\tCellarea\tContourlength\tLocoefamodes(N=%d)\tCumulativedifference\tEntropy\tXOR_Original_Reconstructed(N=%d)\tLnumbers(N=%d)\n",locoefamodes+1,locoefamodes+1,locoefamodes+1);
	  fprintf(fp,"%d\t%d\t%d\t%d\t%f\t%d\t%f\t%f",originalcells.celltype[c],EXTRAS[c].timepoint,EXTRAS[c].oldsigma,originalcells.area[c],originalcells.contour[c][originalcells.contourlength[c]].t,locoefamodes+1,EXTRAS[c].cumulativedifference,EXTRAS[c].entropy);
	  for(lobes=1;lobes<locoefamodes+2;lobes++)
	    fprintf(fp,"\t%g",difference[c][lobes]);
	  for(lobes=1;lobes<locoefamodes+2;lobes++) {
	    if(pnnorm)
	      fprintf(fp,"\t%g",EXTRAS[c].pn_norm[lobes]); //moving the position n+1 and n-1
	    else
	      fprintf(fp,"\t%g",EXTRAS[c].pn[lobes]); //without normalization
	  }
	  fprintf(fp,"\n");
	}
	);
  fclose(fp);
  printf("\t...done.\n");
  printf("stacksize %d\t totalcells %d\n",stacksize, totalcells);
  return 0;
}
