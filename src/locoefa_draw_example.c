#include <minexcal.h>

static Cell cells;
static int T;
static int p;
static int showcircles;
static double worldscale;

extern int grace;
extern int locoefamodes;

/********************************************functions*/

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  ReadOptions(name);
  MyInDat(name,"%d","T",&T); 
  MyInDat(name,"%d","p",&p);
  MyInDat(name,"%d","showcircles",&showcircles);
  MyInDat(name,"%lf","worldscale",&worldscale);
}

int main(int argc,char *argv[])
{
  double xvalue,yvalue;
  double xvaluesubellipse,yvaluesubellipse;
  double xvaluestart1,yvaluestart1;
  char dirname[STRING];
  char a_string[STRING];
  int framenumber=0;
  int saveit=0;
  int k;
  int pathlength;
  int n,mode;
  double zeta3;
  
  if(argc!=7 && argc!=8) {
    fprintf(stdout,"usage: %s parameterfile radiusplus_L3 radiusminus_L3 zetaplus_L3 zetaminus_L3 zetaplus_L1 [outputdir]\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  //read the pars
  fprintf(stdout,"reading parameters from %s\n",argv[1]);
  SetPars(argv[1]); 
  
  if(argc==8) { //there is a name to save the output in
    grace=0; //we are not allowed to redraw on the screen (because that messes up scaling in saved output, which is a reported bug in the grace lib)
    //grace bug #1176:
    //redrawing canvas in the free mode might in some cases result in modified graph viewport values (rep. #1176)

    saveit=1;

    //create the dir
    if(snprintf(dirname,STRING,"%s",argv[7])>=STRING) {
      fprintf(stderr,"warning: output name too long: %s\n",argv[7]);
      exit(EXIT_FAILURE);
    }
    pathlength=strlen(dirname);
    while(pathlength && dirname[pathlength-1]=='/') {
      dirname[pathlength-1]='\0';
      pathlength--;
    }
    for(k=pathlength-1;k>=0;k--) {
      if(k==0 || dirname[k-1]=='/') {
	dirname[k]=toupper(dirname[k]);
	break;
      }
    }
    snprintf(a_string,STRING,"mkdir -p %s",dirname);
    system(a_string);
  }

  CNew(&cells,1,1);
  InitLOCOEFA(&cells);

  cells.locoefa[0][3].locolambdaplus=atof(argv[2]);
  cells.locoefa[0][3].locolambdaminus=atof(argv[3]);
  cells.locoefa[0][3].locozetaplus=atof(argv[4])*M_PI/180.;
  cells.locoefa[0][3].locozetaminus=atof(argv[5])*M_PI/180.;
  cells.locoefa[0][1].locolambdaplus=1.;
  cells.locoefa[0][1].locozetaplus=atof(argv[6])*M_PI/180.;
  
  //note that the next line is normally done by CalculateLOCOCoefficients
  //Equation 47: L3
  cells.locoefa[0][3].locoL=sqrt(cells.locoefa[0][3].locolambdaplus*cells.locoefa[0][3].locolambdaplus+cells.locoefa[0][3].locolambdaminus*cells.locoefa[0][3].locolambdaminus+2.*cells.locoefa[0][3].locolambdaplus*cells.locoefa[0][3].locolambdaminus*cos(cells.locoefa[0][3].locozetaplus-cells.locoefa[0][3].locozetaminus-2.*cells.locoefa[0][1].locozetaplus));
  //Equation 46b: zeta3
  zeta3=atan2(cells.locoefa[0][3].locolambdaplus*sin(cells.locoefa[0][3].locozetaplus-(3+1)*cells.locoefa[0][1].locozetaplus)+cells.locoefa[0][3].locolambdaminus*sin(cells.locoefa[0][3].locozetaminus-(3-1)*cells.locoefa[0][1].locozetaplus),cells.locoefa[0][3].locolambdaplus*cos(cells.locoefa[0][3].locozetaplus-(3+1)*cells.locoefa[0][1].locozetaplus)+cells.locoefa[0][3].locolambdaminus*cos(cells.locoefa[0][3].locozetaminus-(3-1)*cells.locoefa[0][1].locozetaplus));
  
  //set up grace
  ColorTable(0,8,1,0,2,3,4,5,6,7,8);
  OpenGrace(1,1,NULL);
  Grace(GGRAPH,0);
  //note that it looks rectangular on the screen, but the saved output is a perfect square
  Grace(GVIEWPORT,0.15,0.15,0.85,0.85);
  Grace(GWORLD,-worldscale,-worldscale,worldscale,worldscale);

  Grace(GAXIS,'x');
  Grace(GLABEL,"X");
  GracePrintf("xaxis tick minor ticks 0");
  Grace(GAXIS,'y');
  Grace(GLABEL,"Y");
  GracePrintf("yaxis tick minor ticks 0");
  Grace(GSET,1);
  Grace(GLINE,GCOLOR,ColorName(101,101,"dark blue"));
  Grace(GLINE,GWIDTH,2.);
  Grace(GSET,2);
  Grace(GLINE,GCOLOR,ColorName(102,102,"green"));
  Grace(GLINE,GWIDTH,3.);
  Grace(GSET,3);
  Grace(GLINE,GCOLOR,ColorName(103,103,"orange"));
  Grace(GLINE,GWIDTH,3.);
  Grace(GSET,4);
  Grace(GLINE,GCOLOR,ColorName(104,104,"red"));
  Grace(GLINE,GWIDTH,3.);
  Grace(GSET,5);
  Grace(GLINE,GWIDTH,1.);
  Grace(GLINE,GCOLOR,ColorName(100,100,"dark blue"));
  Grace(GSET,40);
  Grace(GLINE,GCOLOR,ColorName(105,105,"magenta"));

  //draw the first LOCOEFA mode
  if(p>=1) {
    for(n=0;n<=T;n++) {
      mode=1;
      ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,mode,&xvalue,&yvalue);
      Grace(GSET,5);
      Grace(GPOINT,2,xvalue,yvalue);
      Grace(GSET,1);
      Grace(GPOINT,2,(1.+cells.locoefa[0][3].locoL*cos(6.*M_PI*((double)n/(double)T)+zeta3))*cos(2.*M_PI*(double)n/(double)T),(1.+cells.locoefa[0][3].locoL*cos(6.*M_PI*((double)n/(double)T)+zeta3))*sin(2.*M_PI*(double)n/(double)T));

      if(saveit) {
	//direct png
	/*
	  snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	  Grace(GPRINT,"PNG",a_string);
	*/
	//eps
	snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
	Grace(GPRINT,"EPS",a_string);
	framenumber++;
      }
      else
	Grace(GREDRAW); 
    }
  }
    
  //draw the third LOCOEFA mode
  if(p>=3) {
    for(n=0;n<=T;n++) {
      ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,1,3,&xvalue,&yvalue);
      if(cells.locoefa[0][3].locolambdaminus>EPSILON && cells.locoefa[0][3].locolambdaplus<EPSILON)
	Grace(GSET,2);
      else if(cells.locoefa[0][3].locolambdaminus>EPSILON && cells.locoefa[0][3].locolambdaplus>EPSILON)
	Grace(GSET,3);
     else //if(cells.locoefa[0][3].locolambdaminus<EPSILON && cells.locoefa[0][3].locolambdaplus>EPSILON)
 	Grace(GSET,4);
      Grace(GPOINT,2,xvalue,yvalue);
      if(showcircles) {
	ReconstructContourpoint(&cells,0,0.,LOCOEFA,1,1,&xvaluestart1,&yvaluestart1);
	ReconstructContourpoint(&cells,0,(double)n/(double)T,LOCOEFA,3,3,&xvaluesubellipse,&yvaluesubellipse);
	Grace(GSET,40);
	Grace(GPOINT,2,xvaluestart1+xvaluesubellipse,yvaluestart1+yvaluesubellipse);
      }

      if(saveit) {
	//direct png
	/*
	  snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	  Grace(GPRINT,"PNG",a_string);
	*/
	//eps
	snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
	Grace(GPRINT,"EPS",a_string);
	framenumber++;
      }
      else
	Grace(GREDRAW);
    } 
  }

  if(p>3) {
    fprintf(stdout,"please rewrite this code to draw the pattern for an arbitrarily number of modes\n");
  }
  
  if(argc==8) {
    snprintf(a_string,STRING,"%s/locoefa_draw_example.agr",dirname);
    Grace(GSAVE,a_string);
  }
  if(grace)
    CloseGrace(GDETACH);
  else
    CloseGrace(GCLOSE);
  return 0;
}
