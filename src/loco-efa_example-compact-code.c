//run using the following command:
//loco-efa_example_minexcal cell_outline.csv
#include <minexcal.h>

static Cell cells;
extern int locoefamodes;

/********************************************functions*/

int main(int argc,char *argv[])
{
  int k;
  double xpoint,ypoint;
  double maxdiff=0.;
  
  locoefamodes=100;
  
  CNew(&cells,1,1);
  InitLOCOEFA(&cells);
  if(argc<2) {
    fprintf(stderr,"Usage: %s cell_outline.csv\n",argv[0]);
    exit(EXIT_FAILURE);
  }
  ReadContour(&cells,0,argv[1]);
  CalculateEFACoefficients(&cells,0);
  CalculateLOCOCoefficients(&cells,0);
  for(k=0;k<=cells.contourlength[0];k++) {
    ReconstructContourpoint(&cells,0,cells.contour[0][k].t,LOCOEFA,-1,-1,&xpoint,&ypoint);
    printf("time %g original (%lf,%lf) new (%lf,%lf) diff (%g,%g)\n",cells.contour[0][k].t/cells.contour[0][cells.contourlength[0]].t,cells.contour[0][k].x,cells.contour[0][k].y,xpoint,ypoint,fabs(cells.contour[0][k].x-xpoint),fabs(cells.contour[0][k].y-ypoint));
    if(hypot(cells.contour[0][k].x-xpoint,cells.contour[0][k].y-ypoint)>maxdiff)
      maxdiff=hypot(cells.contour[0][k].x-xpoint,cells.contour[0][k].y-ypoint);
  }
  printf("maximum error in reconstruction is %g\n",maxdiff);
  return 0;
}  
