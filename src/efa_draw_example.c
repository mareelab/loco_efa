#include <minexcal.h>

static Cell cells;
static int T;
static int subT;
static int p;
static int showcircles;
static double worldscale;
static double secondradius;
static int offset=0;

extern int grace;
extern int locoefamodes;

/********************************************functions*/

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  ReadOptions(name);
  MyInDat(name,"%d","T",&T); 
  MyInDat(name,"%d","subT",&subT); 
  MyInDat(name,"%d","p",&p);
  MyInDat(name,"%d","showcircles",&showcircles);
  MyInDat(name,"%lf","worldscale",&worldscale);
  InDat(name,"%lf","secondradius",&secondradius);
  InDat(name,"%d","offset",&offset);
}

int main(int argc,char *argv[])
{
  double xvalue,yvalue;
  double xvaluesubellipse,yvaluesubellipse;
  char dirname[STRING];
  char a_string[STRING];
  int framenumber=0;
  int saveit=0;
  int k;
  int pathlength;
  int n,subn,mode;
  
  if(argc==1) {
    fprintf(stdout,"usage: %s parameterfile [outputdir]\n",argv[0]);
    exit(EXIT_FAILURE);
  }
  else if (argc>=2) {
    //read the pars
    fprintf(stdout,"reading parameters from %s\n",argv[1]);
    SetPars(argv[1]); 
  }
  if(argc>2) { //there is a name to save the output in
    grace=0; //we are not allowed to redraw on the screen (because that messes up scaling in saved output, which is a reported bug in the grace lib)
    //grace bug #1176:
    //redrawing canvas in the free mode might in some cases result in modified graph viewport values (rep. #1176)

    saveit=1;

    //create the dir
    if(snprintf(dirname,STRING,"%s",argv[2])>=STRING) {
      fprintf(stderr,"warning: output name too long: %s\n",argv[2]);
      exit(EXIT_FAILURE);
    }
    pathlength=strlen(dirname);
    while(pathlength && dirname[pathlength-1]=='/') {
      dirname[pathlength-1]='\0';
      pathlength--;
    }
    for(k=pathlength-1;k>=0;k--) {
      if(k==0 || dirname[k-1]=='/') {
	dirname[k]=toupper(dirname[k]);
	break;
      }
    }
    snprintf(a_string,STRING,"mkdir -p %s",dirname);
    system(a_string);
  }
  
  CNew(&cells,1,1);
  InitLOCOEFA(&cells);

  for(k=0;k<=locoefamodes;k++) {
    snprintf(a_string,STRING,"a[%d]",k);
    InDat(argv[1],"%lf",a_string,&cells.locoefa[0][k].alpha);
    snprintf(a_string,STRING,"b[%d]",k);
    InDat(argv[1],"%lf",a_string,&cells.locoefa[0][k].beta);
    snprintf(a_string,STRING,"c[%d]",k);
    InDat(argv[1],"%lf",a_string,&cells.locoefa[0][k].gamma);
    snprintf(a_string,STRING,"d[%d]",k);
    InDat(argv[1],"%lf",a_string,&cells.locoefa[0][k].delta);
  }

  if(secondradius>DBL_EPSILON) {
    cells.locoefa[0][2].alpha=secondradius;
    cells.locoefa[0][2].delta=secondradius;
  }
  
  CalculateLOCOCoefficients(&cells,0);

  //set up grace
  ColorTable(0,8,1,0,2,3,4,5,6,7,8);
  OpenGrace(1,1,NULL);
  Grace(GGRAPH,0);
  //note that it looks rectangular on the screen, but the saved output is a perfect square
  Grace(GVIEWPORT,0.15,0.15,0.85,0.85);
  Grace(GWORLD,-worldscale,-worldscale,worldscale,worldscale);

  Grace(GAXIS,'x');
  Grace(GLABEL,"X");
  GracePrintf("xaxis tick minor ticks 0");
  Grace(GAXIS,'y');
  Grace(GLABEL,"Y");
  GracePrintf("yaxis tick minor ticks 0");
  Grace(GSET,1);
  Grace(GLINE,GCOLOR,ColorName(101,101,"dark green"));
  Grace(GLINE,GWIDTH,2.);
  Grace(GSET,2);
  Grace(GLINE,GCOLOR,ColorName(102,102,"green"));
  Grace(GLINE,GWIDTH,3.);
  Grace(GSET,3);
  Grace(GLINE,GCOLOR,ColorName(103,103,"blue"));
  Grace(GLINE,GWIDTH,3.);

  //draw the first EFA mode
  if(p>=1) {
    for(n=0;n<=T;n++) {
      mode=1;
      ReconstructContourpoint(&cells,0,(double)n/(double)T,EFA,1,mode,&xvalue,&yvalue);
      Grace(GSET,1);
      Grace(GLINE,GCOLOR,ColorName(100,100,"dark blue"));
      if(offset) //this is specifically for role of second mode plot
	  Grace(GPOINT,2,xvalue+secondradius,yvalue);
      else
	  Grace(GPOINT,2,xvalue,yvalue);
      if(saveit) {
	//direct png
	/*
	  snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	  Grace(GPRINT,"PNG",a_string);
	*/
	//eps
	snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
	Grace(GPRINT,"EPS",a_string);
	framenumber++;
      }
      else
	Grace(GREDRAW); 
    }
  }
  
  //draw the second EFA mode going around the first mode and leaving a sumation of the first and the second
  if(p>=2) {
    for(n=0;n<=T;n++) {
      for(mode=1;mode<3;mode++) {
	ReconstructContourpoint(&cells,0,(double)n/(double)T,EFA,1,mode,&xvalue,&yvalue);
	if(mode==1) {
	  Grace(GSET,0);
	  Grace(GKILL);
	  Grace(GLINE,GCOLOR,ColorName(102,102,"red"));
	  Grace(GLINE,GWIDTH,2.);
	  for(subn=0;subn<=subT;subn++) {
	    ReconstructContourpoint(&cells,0,(double)subn/(double)subT,EFA,mode+1,mode+1,&xvaluesubellipse,&yvaluesubellipse);
	    xvaluesubellipse+=xvalue;
	    yvaluesubellipse+=yvalue;
	    Grace(GPOINT,2,xvaluesubellipse,yvaluesubellipse); //this is the second mode going around the first
	  }
	}
	else /*if mode==2*/ {
	  Grace(GSET,mode);
	  Grace(GPOINT,2,xvalue,yvalue);//this is the plot of the sumation for the first and second mode
	}
	if(saveit) {
	  //direct png
	  /*
	    snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	    Grace(GPRINT,"PNG",a_string);
	  */
	  //eps
	  snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
	  Grace(GPRINT,"EPS",a_string);
	  framenumber++;
	}
	else
	  Grace(GREDRAW);
      } 
    }
  }
  
  //draw the third EFA mode around the second mode, leaving a sumation of the first, second and third
  if(p>=3) {
    for(n=0;n<=T;n++) {
      for(mode=1;mode<4;mode++) {
	ReconstructContourpoint(&cells,0,(double)n/(double)T,EFA,1,mode,&xvalue,&yvalue);
	Grace(GSET,mode);
	Grace(GPOINT,2,xvalue,yvalue);//this is the plot of the sumations
	if(mode==1) {
	  Grace(GSET,0);
	  Grace(GKILL);
	  Grace(GLINE,GWIDTH,2.);
	  Grace(GLINE,GCOLOR,102);
	  for(subn=0;subn<=subT;subn++) {
	    ReconstructContourpoint(&cells,0,(double)subn/(double)subT,EFA,mode+1,mode+1,&xvaluesubellipse,&yvaluesubellipse);
	    xvaluesubellipse+=xvalue;
	    yvaluesubellipse+=yvalue;
	    Grace(GPOINT,2,xvaluesubellipse,yvaluesubellipse); //this is the second mode going around the first
	  }
	}
	if(mode==2) {
	  Grace(GSET,4);
	  Grace(GKILL);
	  Grace(GLINE,GWIDTH,2.);
	  Grace(GLINE,GCOLOR,ColorName(104,104,"dark orange"));
	  for(subn=0;subn<=subT;subn++) {
	    ReconstructContourpoint(&cells,0,(double)subn/(double)subT,EFA,mode+1,mode+1,&xvaluesubellipse,&yvaluesubellipse);
	    xvaluesubellipse+=xvalue;
	    yvaluesubellipse+=yvalue;
	    Grace(GPOINT,2,xvaluesubellipse,yvaluesubellipse);
	  }
	}
	if(saveit) {
	  //direct png
	  /*
	    snprintf(a_string,STRING,"%s/%.5d.png",dirname,framenumber);
	    Grace(GPRINT,"PNG",a_string);
	  */
	  //eps
	  snprintf(a_string,STRING,"%s/%.5d.eps",dirname,framenumber);
	  Grace(GPRINT,"EPS",a_string);
	  framenumber++;
	}
	else
	  Grace(GREDRAW);
      } 
    }
  }

  if(p>3) {
    fprintf(stdout,"please rewrite this code to draw the pattern for an arbitrarily number of modes\n");
  }
  
  if(argc>2) {
    snprintf(a_string,STRING,"%s/efa_draw_example.agr",dirname);
    Grace(GSAVE,a_string);
  }
  if(grace)
    CloseGrace(GDETACH);
  else
    CloseGrace(GCLOSE);
  return 0;
}
