#include <minexcal.h>

extern int nrow;
extern int ncol;

static TYPE **state;
static Png png1;
static int latest;
static char a_string[STRING];

/********************************************functions*/

void ColorTables(int table)
{
  int first=1,second=126,third=251,fourth=376,fifth=501,sixth=626,seventh=751,eighth=876,nineth=1001;
  FLOAT hred,sred,vred;
  FLOAT hblue,sblue,vblue;

  //note: always start at 1!
  switch(table) {
  case 1:
    ColorTable(1,7,WHITE,GREEN,RED,YELLOW,BLUE,CYAN,GREY);
    ColorName(8,8,"purple");
    ColorName(9,9,"pink");
    ColorName(10,10,"lime green");
    latest=10;
    break;
  case 2:
    ColorGrad(ColorRGB(1,0,1,0),ColorTable(6,6,GREEN));
    ColorGrad(6,ColorTable(21,21,BLUE));
    ColorGrad(21,ColorTable(39,39,RED));
    ColorGrad(39,ColorTable(41,41,WHITE));
    latest=41;
    break;
  case 3:
    //mixture of all kinds of colorbars, look at it with
    //playcolor_05 50 11000 3 tijdelijk.png
    ColorRamp(0,48,GREEN);
    ColorRamp(49,240,RED);
    ColorRamp(321,340,BLUE);
    ColorRamp(1000,1200,BLUE);
    ColorGrad(ColorRGB(1500,22,22,72),ColorRGB(2000,17,116,157));
    ColorGrad(2000,ColorRGB(3500,201,199,212));
    ColorGrad(ColorRGB(4000,255,255,255),ColorRGB(40010,205,185,0));
    ColorGrad(4010,ColorRGB(4020,97,190,0));
    ColorGrad(4020,ColorRGB(4040,0,12,132));
    ColorGrad(ColorRGB(1000,255,255,255),ColorRGB(1500,201,199,212));
    ColorGrad(1500,ColorRGB(5500,17,116,157));
    ColorGrad(ColorRGB(1000,201,199,212),ColorRGB(5500,17,116,157));
    ColorGrad(5500,ColorRGB(10000,23,33,90));
    ColorGrad(10000,ColorRGB(11000,22,22,72));
    latest=11000;
    break;
  case 4:
    first=1;second=126;third=251;fourth=376;fifth=501;sixth=626;seventh=751;eighth=876;nineth=1001;
    ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
    ColorGrad(second,ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    ColorGrad(eighth,ColorRGB(nineth,139,0,0));
    latest=1001;
    break;
  case 5:
    ColorRamp(1,1001,RED);
    latest=1001;
    break;
  case 6:
    second=1;third=126;fourth=251;fifth=376;sixth=501;seventh=626;eighth=751;
    ColorGrad(ColorRGB(second,79,0,95),ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    latest=eighth;
    break;
  case 7:
    eighth=1;nineth=126;
    ColorGrad(ColorTable(eighth,eighth,RED),ColorRGB(nineth,139,0,0));
    latest=nineth;
    break;
  case 8:
    first=1;second=501;third=751;
    ColorGrad(ColorRGB(first,0,0,0),ColorRGB(second,255,0,0));
    ColorGrad(second,ColorRGB(third,255,255,0));
    ColorGrad(third,ColorRGB(1001,255,255,255));
    latest=1001;
    break;
  case 9:
    ColorRamp(0,1001,RED);
    latest=1001;
    break;
  case 10:
    //make a colortable from 0 to 20, where 0.5 (1/40th) is second,79,0,95
    first=1;second=126;third=251;fourth=376;fifth=501;sixth=626;seventh=751;eighth=876;nineth=1001;
    //first=1;second=26;third=165;fourth=305;fifth=444;sixth=583;seventh=722;eighth=862;nineth=1001;
    ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
    ColorGrad(second,ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    ColorGrad(eighth,ColorRGB(nineth,139,0,0));
    latest=nineth;
    break;
  case 11:
    second=1;third=144;fourth=287;fifth=430;sixth=573;seventh=715;eighth=858;nineth=1001;
    ColorGrad(ColorRGB(second,79,0,95),ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    ColorGrad(eighth,ColorRGB(nineth,139,0,0));
    latest=nineth;
    break;
  case 12:
    second=1;third=101;fourth=201;fifth=451;sixth=601;seventh=751;eighth=901;nineth=1001;
    ColorGrad(ColorRGB(second,79,0,95),ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    ColorGrad(eighth,ColorRGB(nineth,200,0,0));
    latest=nineth;
    break;
  case 13:
    first=1;second=51;third=151;fourth=251;fifth=501;sixth=751;seventh=851;eighth=951;nineth=1001;
    ColorGrad(ColorRGB(first,79,0,95),ColorTable(second,second,MAGENTA));
    ColorGrad(second,ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    ColorGrad(eighth,ColorRGB(nineth,200,0,0));
    latest=nineth;
    break;
  case 14:
    first=1;second=101;third=201;fourth=301;fifth=401;sixth=601;seventh=801;eighth=1001;
    latest=eighth;
    ColorGrad(ColorRGB(first,248,252,251),ColorRGB(second,227,244,248));
    ColorGrad(second,ColorRGB(third,172,246,252));
    ColorGrad(third,ColorRGB(fourth,128,224,250));
    ColorGrad(fourth,ColorRGB(fifth,36,130,251));
    ColorGrad(fifth,ColorRGB(sixth,4,55,194));
    ColorGrad(sixth,ColorRGB(seventh,4,2,78));
    ColorGrad(seventh,ColorRGB(eighth,4,3,10));
    latest=eighth;
    break;
  case 15: //Scarecrow (equal to 6)
    second=1;third=126;fourth=251;fifth=376;sixth=501;seventh=626;eighth=751;
    latest=eighth;
    ColorGrad(ColorRGB(second,79,0,95),ColorTable(third,third,BLUE));
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    break;
  case 16: //bmb
    latest=1001;
    //the colours (240,66,34) and (61,81,163) and are picked from WP_PDE_XPP.pdf using gcolor2
    //we need extra colour in the middle, otherwise the gradient goes from red to blue via magenta instead of via green
    rgb2hsv(240,66,34,&hred,&sred,&vred);
    rgb2hsv(61,81,163,&hblue,&sblue,&vblue);
    //printf("%f %f %f\n",hred,sred,vred);
    //printf("%f %f %f\n",hblue,sblue,vblue);
    ColorGrad(ColorRGB(1,240,66,34),ColorHSV(501,(hred+hblue)/2.,(sred+sblue)/2.,(vred+vblue)/2.));
    ColorGrad(501,ColorRGB(latest,61,81,163));
 /* 
    //to find colour in the middle
 */ 
    break;
  case 17: //fruit review
    latest=10001;
    ColorGrad(ColorRGB(1,255,255,255),ColorRGB(201,201,199,212));
    ColorGrad(201,ColorRGB(1001,17,116,157));
    ColorGrad(1001,ColorRGB(9001,23,33,90));
    ColorGrad(9001,ColorRGB(10001,22,22,72));
    break;
  case 18: //fruit review
    third=1,fourth=144,fifth=287,sixth=430,seventh=572,eighth=715;
    latest=eighth;
    ColorTable(third,third,BLUE);
    ColorGrad(third,ColorTable(fourth,fourth,CYAN));
    ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
    ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
    ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
    ColorGrad(seventh,ColorTable(eighth,eighth,RED));
    break;
  case 19: //gruit review
    latest=1001;
    ColorGrad(ColorRGB(1,128,0,0),ColorRGB(501,120,223,0));
    ColorGrad(501,ColorRGB(1001,128,255,0));
    break;
  case 20: //transition zone, lowest part
    first=1;second=126;
    ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
    latest=second;
    break;
  default:
    break;
  }
}

int main(int argc,char *argv[])
{
  if(argc!=5) {
    fprintf(stdout,"usage: %s height width colortable output.png\n",argv[0]);
    exit(EXIT_FAILURE);
  }
  
  nrow=atoi(argv[1]);
  ncol=atoi(argv[2]);
  
  state = New();

  ColorTables(atoi(argv[3]));

  OpenPNG(&png1,"/tmp/pict");
  PLANE(
        state[i][j]=1+(j-1)*(latest)/(ncol);
        );
  
  PlanePNG(state,&png1,0);
  snprintf(a_string,STRING,"mv %s/%.5d.png %s",png1.dirname,png1.nframes-1,argv[4]);
  system(a_string);
  snprintf(a_string,STRING,"rm -rf /tmp/pict");
  system(a_string);
  return 0;
}
