README Figure D3
=======

1. Geometrical shapes A-I and N-Q were generated using the superformula by [Gielis, 2003](http://dx.doi.org/10.3732/ajb.90.3.333), and then segmented.

2. The shape shown in R is a resultant cell shape of the combination 3 of the *in silico* cell shapes.


