# Documentation

This directory is intended to contain documentation related to the project, e.g. experimental methodologies, details of analysis scripts etc.

* Information regarding the creation of and interaction with the repository itself can be found [here](./repository.md).

* Additional information regarding the figures which cannot be understood from the [script](../scripts/create_all_the_images.sh) that generates all the figure panels can be found [here for Fig1](./README-Figure1D.md); [here for Fig3](./README-Figure3D.md); [here for Fig4](./README-Figure4D.md); and [here for FigS3](./README-FigureS3D.md). 