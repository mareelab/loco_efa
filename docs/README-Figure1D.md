README Figure D1
=======

1. The raw microscope files showing wavy cells are: 

	* Wild type pavement cells: **Example_wt_02.lif** 

	* *spch* pavement cells: **ExpID2943_spch4_13DAS_10.lif** 

	* Amnioscerosa cells: **ExpID038-DEcad-single-embryo2-2.lsm**

2. z-projections were generated using [Imaris](http://http://www.bitplane.com/imaris) for the amnioscerosa cells and [Merryproj](http://doi.org/10.1111/j.1365-313X.2005.02576.x) for the wt and *spch* pavement cells.

3. Regarding the traditional metrics:

	* Average length of lobes was done manually

	* Average width of necks was done manually

	* The skeleton end points were calculated using the following Matlab implementation: [better-skeletonization](http://www.mathworks.com/matlabcentral/fileexchange/11123-better-skeletonization). You can find a copy of this code in this repository as well. 
