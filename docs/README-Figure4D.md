README Figure D4
=======
1. Cell01 was cropped from the long time-lapse ExpID03002. 

2. The images included in the figure are a subset of the full time-lapse and correspond to the following time points:
	* ExpID3002_spch4_TL007_plantD_cell01.png
	* ExpID3002_spch4_TL008_plantD_cell01.png
	* ExpID3002_spch4_TL009_plantD_cell01.png
	* ExpID3002_spch4_TL011_plantD_cell01.png
	* ExpID3002_spch4_TL014_plantD_cell01.png
	* ExpID3002_spch4_TL016_plantD_cell01.png
	* ExpID3002_spch4_TL019_plantD_cell01.png
	
3. The exact time intervals between the images can be found in Yara's thesis.  