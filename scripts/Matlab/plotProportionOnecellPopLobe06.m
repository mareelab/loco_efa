% Plot the proportion of the difference between a single cell and the population. Any parameter by the single cell is the 1.0 and the population change is expressed as a proportion of it.  

%% Some options

% clear the working space
clear

NoOfDifferentCells=2; %Number of images or files, it assumes all combinations will have the same number of files
NoOfCombinations=3;
Resolution=0;
arearestriction=0;
sizethreshold=600;

plot_standarerror=0;


%% Some preallocation

NoOfCells=zeros(NoOfDifferentCells,NoOfCombinations);
inoofharmonicsanalyse=cell(1,NoOfDifferentCells); %all will be analyzed using the same number of harmonics
difference1=cell(1,NoOfDifferentCells);
difference3=cell(1,NoOfDifferentCells);
L_1=cell(1,NoOfDifferentCells);
L_3=cell(1,NoOfDifferentCells);
marginaldifference1=cell(1,NoOfDifferentCells);
marginaldifference3=cell(1,NoOfDifferentCells);
cumulativedifference_x1=cell(1,NoOfDifferentCells);
cumulativedifference_x3=cell(1,NoOfDifferentCells);

%% Load the data and extract the cell data 
%combination1 (SCS1)
for d = 1:NoOfDifferentCells
    
    % Specified shape lobe 06 combination 1
    [header, data1] = mhdrload(['../../output/Images/Final/Fig5/data/Cellshapeanalysis_main_CPM_lobe06_comb1_or0_el1_bo0_' int2str(d) '.dat']);
    
    NoOfCells(d,1)=size(data1,3);
    for j=1:NoOfCells(d,1)
        %cellnumber{1,d}(j,1)=data2(1,1,j);
        cellarea1{1,d}(j,1)=data1(1,4,j);
        perimeter1{1,d}(j,1)=data1(1,5,j);
        inoofharmonicsanalyse{1,d}(j,1)=data1(1,6,j);
        cumulativedifference1{1,d}(j,1)=data1(1,7,j);
        entropy1{1,d}(j,1)=data1(1,8,j);
        lobenumber1=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
        difference1{1,d}(j,1:lobenumber1+1)=data1(1,9:lobenumber1+9,j); % XOR
        L_1{1,d}(j,1:lobenumber1)=data1(1,60:end,j);
    end
end

%combination3 (SCS3)
for d = 1:NoOfDifferentCells
    % Specified shape lobe 06 combination 3
    [header, data3] = mhdrload(['../../output/Images/Final/Fig5/data/Cellshapeanalysis_main_CPM_lobe06_comb3_or0_el1_bo0_' int2str(d) '.dat']);
    
    NoOfCells(d,3)=size(data3,3);
    for j=1:NoOfCells(d,3)
        %cellnumber{1,d}(j,1)=data2(1,1,j);
        cellarea3{1,d}(j,1)=data3(1,4,j);
        perimeter3{1,d}(j,1)=data3(1,5,j);
        inoofharmonicsanalyse{1,d}(j,1)=data3(1,6,j);
        cumulativedifference3{1,d}(j,1)=data3(1,7,j);
        entropy3{1,d}(j,1)=data3(1,8,j);
        lobenumber3=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
        difference3{1,d}(j,1:lobenumber3+1)=data3(1,9:lobenumber3+9,j); %XOR
        L_3{1,d}(j,1:lobenumber3)=data3(1,60:end,j);
    end
end


%% Calculate the marginal difference
for d=1:NoOfDifferentCells
    %combination1
    for j=1:NoOfCells(d,1)
        for i=2:22 %because no element at (1,0)
            marginaldifference1{d}(j,i)=difference1{d}(j,i-1)-difference1{d}(j,i);
        end
    end
    %     %combination2
    %      for j=1:NoOfCells(d,2)
    %         for i=2:22
    %         marginaldifference2{d}(j,i)=difference2{d}(j,i-1)-difference2{d}(j,i);
    %         end
    %     end 
    %combination3
    for j=1:NoOfCells(d,3)
        for i=2:22
            marginaldifference3{d}(j,i)=difference3{d}(j,i-1)-difference3{d}(j,i);
        end
    end
end

%% Make a simple matrix with the cells from thre three repetitions.
%Combination1
%PopXORdiff1 = vertcat(difference1{1,2},difference1{1,3},difference1{1,4});
%PopMarginaldiff1 =  vertcat(marginaldifference1{1,2},marginaldifference1{1,3},marginaldifference1{1,4});
%PopLn1 = vertcat(L_1{1,2},L_1{1,3},L_1{1,4});
PopXORdiff1 = difference1{1,2};
PopMarginaldiff1 =  marginaldifference1{1,2};
PopLn1 = L_1{1,2};

%Combination3
%PopXORdiff3 = vertcat(difference3{1,2},difference3{1,3},difference3{1,4});
%PopMarginaldiff3 =  vertcat(marginaldifference3{1,2},marginaldifference3{1,3},marginaldifference3{1,4});
%PopLn3 = vertcat(L_3{1,2},L_3{1,3},L_3{1,4});
PopXORdiff3 = difference3{1,2};
PopMarginaldiff3 =  marginaldifference3{1,2};
PopLn3 = L_3{1,2};

%Make the same matrix for a single cell, so that the division can be done
%element-by-element
OneXORdiff1 = repmat(difference1{1,1},size(PopXORdiff1,1),1);%repmat(A,n) 
OneMarginaldiff1 = repmat(marginaldifference1{1,1},size(PopMarginaldiff1,1),1);
OneLn1 = repmat(L_1{1,1}, size(PopLn1,1),1);

OneXORdiff3 = repmat(difference3{1,1},size(PopXORdiff3,1),1);%repmat(A,n) 
OneMarginaldiff3 = repmat(marginaldifference3{1,1},size(PopMarginaldiff3,1),1);
OneLn3 = repmat(L_3{1,1},size(PopLn3,1),1);

%% Calculate the proportion of XOR,Marginal difference and Ln of each cell in the population compared with the single cell: OneCell/Pop ratio
proportionXORcomb1 = OneXORdiff1./PopXORdiff1;
proportionMDcomb1 =  OneMarginaldiff1./PopMarginaldiff1;
proportionLncomb1 = OneLn1./PopLn1;

proportionXORcomb3 = OneXORdiff3./PopXORdiff3;
proportionMDcomb3 =  OneMarginaldiff3./PopMarginaldiff3;
proportionLncomb3 = OneLn3./PopLn3;

%% Plot proportionXORcomb1(cellx,ilobenumber): cells in rows and selected Lobe04, Lobe05, Lobe06, Lobe07 (columns)
%Combination1
plotppXOR1 = horzcat(proportionXORcomb1(:,4),proportionXORcomb1(:,5),proportionXORcomb1(:,6),proportionXORcomb1(:,7));
plotppMD1 =  horzcat(proportionMDcomb1(:,4),proportionMDcomb1(:,5),proportionMDcomb1(:,6),proportionMDcomb1(:,7));
plotppL1  =  horzcat(proportionLncomb1(:,4),proportionLncomb1(:,5),proportionLncomb1(:,6),proportionLncomb1(:,7));

%Combination3
plotppXOR3 = horzcat(proportionXORcomb3(:,4),proportionXORcomb3(:,5),proportionXORcomb3(:,6),proportionXORcomb3(:,7));
plotppMD3 =  horzcat(proportionMDcomb3(:,4),proportionMDcomb3(:,5),proportionMDcomb3(:,6),proportionMDcomb3(:,7));
plotppL3  =  horzcat(proportionLncomb3(:,4),proportionLncomb3(:,5),proportionLncomb3(:,6),proportionLncomb3(:,7));



%% box plots
scrsz = get(0,'ScreenSize');
cc=hsv(8);
size=5;
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/4],'Colormap',rand(128,3)); 

subplot(1,6,1)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppL1,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(2,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('Ln comb1')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1) 
set(gca,'Fontname','Arial')
set(gca,'YScale','log') 
set(gca, 'YMinorGrid','on')
h1=findobj(gca,'tag','Outliers');
delete(h1) 
%axis ([0 5 0 5.3]);
pbaspect([1 1.3 1])
axis ([0 5 0.1 3.162]);

subplot(1,6,2)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppXOR1,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(4,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('XOR comb1')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1) 
set(gca,'Fontname','Arial')
set(gca,'YScale','log')
set(gca, 'YMinorGrid','on')
h1=findobj(gca,'tag','Outliers');
delete(h1) 
%axis ([0 5 0.4 3.8]);
pbaspect([1 1.3 1])
axis ([0 5 0.31 3.162]);

subplot(1,6,3)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppMD1,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(6,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('MD comb1')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1)
set(gca,'Fontname','Arial')
%set(gca,'YScale','log')
h1=findobj(gca,'tag','Outliers');
delete(h1)
pbaspect([1 1.3 1])
axis ([0 5 -1.0 4]);
%axis ([0 5 0.01 100]);

subplot(1,6,4)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppL3,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(2,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('Ln comb3')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1)
set(gca,'Fontname','Arial')
set(gca,'YScale','log')
set(gca, 'YMinorGrid','on')
h1=findobj(gca,'tag','Outliers');
delete(h1) 
pbaspect([1 1.3 1])
axis ([0 5 0.0316 10]);

subplot(1,6,5)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppXOR3,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(4,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('XOR comb3')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1)
set(gca,'Fontname','Arial')
set(gca,'YScale','log')
set(gca, 'YMinorGrid','on')
h1=findobj(gca,'tag','Outliers');
delete(h1) 
pbaspect([1 1.3 1])
axis ([0 5 0.5623 5.623]);

subplot(1,6,6)
plot([0:6],[1,1,1,1,1,1,1],'--','LineWidth',0.7,'color','r');
hold on
h = boxplot(plotppMD3,'Notch','on','Labels',{'','','',''},'Widths',0.5,'OutlierSize',3,'Colors',cc(6,:));
% xlabel('XOR Difference','FontSize',20,'FontName', 'Arial')
% ylabel('Percentange of change in populations','FontSize',20,'FontName', 'Arial')
%title('MD comb3')
set(h,{'linew'},{2})
set(gca,'fontsize',12,'LineWidth',1)
set(gca,'Fontname','Arial')
%set(gca,'YScale','log')
h1=findobj(gca,'tag','Outliers');
delete(h1) 
pbaspect([1 1.3 1])
axis ([0 5 -2.0 5]);
%axis ([0 5 0.01 100]);


% Get dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
% print to save as
saveas(gcf, '../../output/Images/Final/Fig5/panel5HI','epsc');



drawnow
set(gcf,'Units',oldscreenunits,'PaperUnits',oldpaperunits,'PaperPosition',oldpaperpos)

