% This script plots the L landscape,Difference(delta Area),Marginal difference, cumulative difference[x,N],entropy of hypothetical cells
% Figure of explanation in the main text of the paper: geometrical shapes.

% clear the working space
clc

%Number of images or files
NoOfDifferentCells=9; 

%Some preallocation
NoOfCells=zeros(1,NoOfDifferentCells);
cellnumber=cell(1,NoOfDifferentCells);
cellarea=cell(1,NoOfDifferentCells);
inoofharmonicsanalyse=cell(1,NoOfDifferentCells);
cumulativedifference=cell(1,NoOfDifferentCells);
entropy=cell(1,NoOfDifferentCells);
difference=cell(1,NoOfDifferentCells);
L=cell(1,NoOfDifferentCells);
marginaldifference=cell(1,NoOfDifferentCells);
cumulativediference_x=cell(1,NoOfDifferentCells);
average_difference=cell(1,NoOfDifferentCells);
std_difference=cell(1,NoOfDifferentCells);
average_marginaldifference=cell(1,NoOfDifferentCells);
std_marginaldifference=cell(1,NoOfDifferentCells);
average_cd=cell(1,NoOfDifferentCells);
std_cd=cell(1,NoOfDifferentCells);
average_L=cell(1,NoOfDifferentCells);
std_L=cell(1,NoOfDifferentCells);
average_entropy=cell(1,NoOfDifferentCells);
std_entropy=cell(1,NoOfDifferentCells);


%% Extract the cell data 
%Load the file with the data
%for d = 1:NoOfDifferentCells
for d = 1:NoOfDifferentCells
[header, data]=  mhdrload(['../../output/Fig3/Output_CellShapeAnalysis/LOCO_geometricalshapes/LOCO_geometricalshapes.dat']);

 NoOfCells(d)=size(data,3);
 
 for j=1:NoOfCells(d)
 cellnumber{1,d}(j,1)=data(1,1,j);
 %timepoint{1,d}(1,j)=data(1,2,j);
 %cellarea(d)=data(:,4)*0.3292; %resolution is 0.5737um per pixel, then 0.5^2 is per pixel^2
 cellarea{1,d}(j,1)=data(1,4,j);
 perimeter2{1,d}(j,1)=data(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data(1,6,j);
 cumulativedifference{1,d}(j,1)=data(1,7,j); %in cellshapeanalysis_08, cumulative difference [x, N], x=2.
 entropy{1,d}(j,1)=data(1,8,j);
 lobenumber=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number of harmonics
 difference{1,d}(j,1:lobenumber+1)=data(1,9:lobenumber+9,j);
 L{1,d}(j,1:lobenumber)=data(1,60:end,j);
 end
end

%% Calculate marginal difference and CD
%The marginal difference
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=2:22
        marginaldifference{d}(j,i)=difference{d}(j,i-1)-difference{d}(j,i);
        end
    end
end

%Cumulative difference [x,N]. x=2, N=50
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=1:8
cumulativediference_x{1,d}(j,i)=sum(difference{1,d}(j,i:lobenumber-1));
        end
    end
end   

%Calculate average and stardar dev for XOR,Marginal difference,CD,L,
%entropy
if NoOfCells(d)>1
for d=1:NoOfDifferentCells

     for j=1:NoOfCells(d)
         for i=1: lobenumber-1
       average_difference{1,d}(1,i)=mean(difference{1,d}(:,i));
       std_difference{1,d}(1,i)=std(difference{1,d}(:,i));
         end
         for i=1: 22
       average_marginaldifference{1,d}(1,i)=mean(marginaldifference{1,d}(:,i));
       std_marginaldifference{1,d}(1,i)=std(marginaldifference{1,d}(:,i));
         end
       average_cd{1,d}(1,1)=mean(cumulativedifference{1,d}(:,1));
       std_cd{1,d}(1,1)=std(cumulativedifference{1,d}(:,1));
       
       for i=1: lobenumber-2
       average_L{1,d}(1,i)=mean(L{1,d}(:,i));
       std_L{1,d}(1,i)=std(L{1,d}(:,i));
       end
       
       average_entropy{1,d}(1,1)=mean(entropy{1,d}(:,1));
       std_entropy{1,d}(1,1)=std(entropy{1,d}(:,1));
     end  
end
end

%% PLOTS: L numbers, XOR,CD,MarginalD,entropy

% crsz = get(0,'ScreenSize');
% figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3)); 
% cc=hsv(10);
k=[0:1:NoOfCells+1];
scrsz = get(0,'ScreenSize');
line_width=2;
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3)); 
cc=hsv(9);  

for d=1:NoOfDifferentCells 
 %L landscape
    subplot(1,4,1)
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(2:15,L{1,d}(j,2:15),'-o','LineWidth',line_width,'color',cc(j,:))
    %xlabel('Ln mode','fontSize', 14,'FontName', 'Arial')
    %ylabel('Ln','fontSize', 14,'FontName', 'Arial')
    %title('L landscape','fontSize', 14,'FontWeight','bold')
    set(gca,'XTick',1:1:15)
    set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
    xlim([2 15])
    box on
%     %h1=legend('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I');
%     rect = [0.001, 0.2, 0.1, 0.6];
%     set(h1, 'Position', rect)
%     set(h1,'Interpreter','none')
%     %set(h1,'Location','NorthWestOutside')
%     set(h1,'FontName', 'Arial','FontSize',14);
    end
    
   %XOR Difference 
    subplot(1,4,2)
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(1:1:15,difference{1,d}(j,1:1:15),'-o','LineWidth',line_width,'color',cc(j,:));  
%     xlabel('Ln mode','FontSize',14,'FontName', 'Arial')
%     ylabel('XOR Difference','FontSize',14,'FontName', 'Arial')
    set(gca,'XTick',1:1:15)
    %title('XOR Original and Reconstructed','FontSize',14,'FontWeight','bold')
    set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
    axis ([0 14 -0.02 0.52]);
    box on
    end
    
 %Marginal difference
   subplot(1,4,3)
    for j=1:NoOfCells(d)
    hold all
    plot(1:1:14,marginaldifference{d}(j,1:14),'-o','LineWidth',line_width,'color',cc(j,:));
%     xlabel('Ln mode','FontSize',14,'FontName', 'Arial')
%     ylabel('Marginal Difference','FontSize',14,'FontName', 'Arial')
    %title('Marginal Difference ({\itd(XOR)} / {\itdL})','FontSize',14,'FontWeight','bold')
    set(gca,'XTick',1:1:14)
    set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
    axis ([0 14 -0.02 0.48]);
    box on
    end
    
 %Cumulative difference and entropy in two different axis
   subplot(1,4,4) 
    for j=1:NoOfCells(d)
         hold all
        plot(0,0, 'w')
        plot(1,cumulativedifference{d}(j),'ro','LineWidth',line_width,'color',cc(j,:),'MarkerFaceColor',cc(j,:));
        hold all
        plot(2,entropy{d}(j),'rs','LineWidth',line_width,'color',cc(j,:),'MarkerFaceColor',cc(j,:));
        set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
        hold all
        plot(3,0, 'w')
        box on
       % Set Ticks
        set(gca, 'XTick',0:NoOfCells(d)+1, 'XTickLabel',{'' 'cd' 'entropy' ''});
    
    end

hax = axes('Position', [.2, .68, .07, .18]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height   
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(1:7,L{1,d}(j,1:7),'-o','LineWidth',1,'color',cc(j,:));
    %xlabel('Lobe number','fontSize', 10)
    %ylabel('L (amplitude)','fontSize', 10)
    %title('L landscape','fontSize', 10,'FontWeight','bold')
    set(gca,'XTick',1:1:7)
   % set(gca,'fontsize',9,'LineWidth',2,'FontName', 'Arial')
    xlim([1 7])
    box on
    end
 
end

% Get dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
% print to save as
%print('-dpng', '../../output/fig3JM.png', '-r100');
saveas(gcf,'../../output/fig3JM.eps','epsc');

% drawnow
% set(gcf,'Units',oldscreenunits,'PaperUnits',oldpaperunits,'PaperPosition',oldpaperpos)
