% This script plots the L landscape,Difference(delta Area),Marginal difference, cumulative difference[x,N],entropy.
% Figure of explanation in the main text of the paper: ExpID3002-cell01.
% Note: Generate 3D plots of the Ln landscape AND marginal difference, no need to comment/uncomment (lines
% 235-242).

% Note: checks for comparison using LOCO-EFA and EFA no
% aligned

% Note: 3D plot for
% Marginal Difference with new scale, to avoid white space in front.

% clear the working space
clc

%Number of images or files
NoOfDifferentCells=7; %number of cells


%Some preallocation
NoOfCells=zeros(1,NoOfDifferentCells);
cellnumber=cell(1,NoOfDifferentCells);
cellarea=cell(1,NoOfDifferentCells);
inoofharmonicsanalyse=cell(1,NoOfDifferentCells);
cumulativedifference=cell(1,NoOfDifferentCells);
entropy=cell(1,NoOfDifferentCells);
difference=cell(1,NoOfDifferentCells);
L=cell(1,NoOfDifferentCells);
marginaldifference=cell(1,NoOfDifferentCells);
cumulativediference_x=cell(1,NoOfDifferentCells);
average_difference=cell(1,NoOfDifferentCells);
std_difference=cell(1,NoOfDifferentCells);
average_marginaldifference=cell(1,NoOfDifferentCells);
std_marginaldifference=cell(1,NoOfDifferentCells);
average_cd=cell(1,NoOfDifferentCells);
std_cd=cell(1,NoOfDifferentCells);
average_L=cell(1,NoOfDifferentCells);
std_L=cell(1,NoOfDifferentCells);
average_entropy=cell(1,NoOfDifferentCells);
std_entropy=cell(1,NoOfDifferentCells);


%% Extract the cell data 
%Load the file with the data
%for d = 1:NoOfDifferentCells
for d = 1:NoOfDifferentCells
[header, data]=  mhdrload(['../../output/FigS5/Output_CellShapeAnalysis/LOCO-EFA-Ln-spch1-cell02/LOCO-EFA-Ln-spch1-cell02.dat']);

 NoOfCells(d)=size(data,3);
 
 for j=1:NoOfCells(d)
 cellnumber{1,d}(j,1)=data(1,1,j);
 %timepoint{1,d}(1,j)=data(1,2,j);
 %cellarea(d)=data(:,4)*0.3292; %resolution is 0.5737um per pixel, then 0.5^2 is per pixel^2
 cellarea{1,d}(j,1)=data(1,4,j);
 perimeter2{1,d}(j,1)=data(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data(1,6,j);
 cumulativedifference{1,d}(j,1)=data(1,7,j); %in cellshapeanalysis_08, cumulative difference [x, N], x=2.
 entropy{1,d}(j,1)=data(1,8,j);
 lobenumber=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number of harmonics
 difference{1,d}(j,1:lobenumber+1)=data(1,9:lobenumber+9,j);
 L{1,d}(j,1:lobenumber)=data(1,60:end,j);
 end
end

%% Calculate marginal difference and CD
%The marginal difference
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=2:22
        marginaldifference{d}(j,i)=difference{d}(j,i-1)-difference{d}(j,i);
        end
    end
end

%Cumulative difference [x,N]. x=2, N=50
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=1:8
cumulativediference_x{1,d}(j,i)=sum(difference{1,d}(j,i:lobenumber-1));
        end
    end
end   

%Calculate average and stardar dev for XOR,Marginal difference,CD,L,
%entropy
if NoOfCells(d)>1
for d=1:NoOfDifferentCells

     for j=1:NoOfCells(d)
         for i=1: lobenumber-1
       average_difference{1,d}(1,i)=mean(difference{1,d}(:,i));
       std_difference{1,d}(1,i)=std(difference{1,d}(:,i));
         end
         for i=1: 22
       average_marginaldifference{1,d}(1,i)=mean(marginaldifference{1,d}(:,i));
       std_marginaldifference{1,d}(1,i)=std(marginaldifference{1,d}(:,i));
         end
       average_cd{1,d}(1,1)=mean(cumulativedifference{1,d}(:,1));
       std_cd{1,d}(1,1)=std(cumulativedifference{1,d}(:,1));
       
       for i=1: lobenumber-2
       average_L{1,d}(1,i)=mean(L{1,d}(:,i));
       std_L{1,d}(1,i)=std(L{1,d}(:,i));
       end
       
       average_entropy{1,d}(1,1)=mean(entropy{1,d}(:,1));
       std_entropy{1,d}(1,1)=std(entropy{1,d}(:,1));
     end  
end
end



%% PLOTS: L numbers, XOR,CD,MarginalD,entropy

k=[0:1:NoOfCells+1];
scrsz = get(0,'ScreenSize');
line_width=2;
figure('Position',[0 0 2000 360],'Colormap',rand(128,3)); 
cc=hsv(NoOfDifferentCells);  

for d=1:NoOfDifferentCells
 %L landscape
    subplot(1,4,1)
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(2:15,L{1,d}(j,2:15),'-o','LineWidth',line_width,'color',cc(j,:))
    xlabel('Ln mode','fontSize', 14,'FontName', 'Arial')
    ylabel('Ln','fontSize', 14,'FontName', 'Arial')
    %title('L landscape','fontSize', 14,'FontWeight','bold')
    set(gca,'XTick',1:1:15)
    set(gca,'fontsize',14,'LineWidth',2)
    xlim([2 15])
    box on
%     h1=legend('A', 'B', 'C', 'D', 'E', 'F','G');
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWest')
%     set(h1,'FontName', 'Arial','FontSize',14);
    end
   
   %XOR Difference 
    subplot(1,4,2)
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(1:1:15,difference{1,d}(j,1:1:15),'-o','LineWidth',line_width,'color',cc(j,:));  
    xlabel('Ln mode','FontSize',14,'FontName', 'Arial')
    ylabel('XOR Difference','FontSize',14,'FontName', 'Arial')
    set(gca,'XTick',1:1:15)
    %title('XOR Original and Reconstructed','FontSize',14,'FontWeight','bold')
    set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
     box on
    end
    
 %Marginal difference
   subplot(1,4,3)
    for j=1:NoOfCells(d)
    hold all
    plot(1:1:14,marginaldifference{d}(j,1:14),'-o','LineWidth',line_width,'color',cc(j,:));
    xlabel('Ln mode','FontSize',14,'FontName', 'Arial')
    ylabel('Marginal Difference','FontSize',14,'FontName', 'Arial')
    %title('Marginal Difference ({\itd(XOR)} / {\itdL})','FontSize',14,'FontWeight','bold')
    set(gca,'XTick',1:1:14)
    set(gca,'fontsize',14,'LineWidth',2,'FontName', 'Arial')
     box on
    end
    
 %Cumulative difference and entropy in two different axis
   subplot(1,4,4) 
    for j=1:NoOfCells(d)
         hold all
        plot(0,0, 'w')
        plot(1,cumulativedifference{d}(j),'ro','LineWidth',line_width,'color',cc(j,:),'MarkerFaceColor',cc(j,:));
        hold all
        plot(2,entropy{d}(j),'rs','LineWidth',line_width,'color',cc(j,:),'MarkerFaceColor',cc(j,:));
        set(gca,'fontsize',14,'LineWidth',2)
        plot(3,0, 'w')
        box on
       % Set Ticks
        set(gca, 'XTick',0:NoOfCells(d)+1, 'XTickLabel',{'' 'cd' 'entropy' '' 'D' 'E' 'F' 'G' 'H' 'I' ''});
    
    end

hax = axes('Position', [.2, .68, .07, .18]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height   
    for j=1:NoOfCells(d)
    %for j=a
    hold all
    plot(1:7,L{1,d}(j,1:7),'-o','LineWidth',1,'color',cc(j,:));
    %xlabel('Lobe number','fontSize', 10)
    %ylabel('L (amplitude)','fontSize', 10)
    %title('L landscape','fontSize', 10,'FontWeight','bold')
    set(gca,'XTick',1:1:7)
   % set(gca,'fontsize',9,'LineWidth',2,'FontName', 'Arial')
    xlim([1 7])
    box on
    end
 
end

% Get dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);

saveas(gcf,'../../output/figS5I.eps','epsc');

% %% Make a 3D plot
% % In X is the lobe number or Ln-mode (1:15)
% % In Z is the value in each lobe number data_Ln{i}.av_Ln(1,1:20)
% % In Y the number of time-points: 1 to 7.
% 
% numtoplot=15; %number of modes to consider
% numberoftimepoints=7;
% 
% %Define x values
% x = (2:1:numtoplot).';
% xMat =  repmat (x,1,numberoftimepoints);
% 
% %Define y values
% y = 1:1:numberoftimepoints;
% yMat = repmat(y, numel(x), 1);
% 
% % OPTION1: Plot Ln or Pn
% 
% % % %Plot Ln or Pn
% z1 = L{1,1}(1,2:numtoplot);
% z2 = L{1,1}(2,2:numtoplot);
% z3 = L{1,1}(3,2:numtoplot);
% z4 = L{1,1}(4,2:numtoplot);
% z5 = L{1,1}(5,2:numtoplot); 
% z6 = L{1,1}(6,2:numtoplot);
% z7 = L{1,1}(7,2:numtoplot);
% 
% %concateante the z, for 3D plot
% zMat = [z1; z2; z3; z4; z5; z6; z7]';
% 
% 
% %colors
% cc=hsv(numberoftimepoints);
% figure
% for g=1:numberoftimepoints
%     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
%      plot3(xMat(:,g), yMat(:,g), zMat(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
%      hold all
%     set(gca,'XTick',2:1:15)    
%     set(gca,'fontsize',18,'LineWidth',1)
%     set(gca,'Fontname','Arial') 
%     
% grid on;
% %xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% %ylabel('y'); 
% %zlabel('z');
% view(10,50); %// Adjust viewing angle so you can clearly see data
% % h1=legend('A','B','C','D','E','F', 'G',6);
% %     set(h1,'Interpreter','none')
% %     set(h1,'Location','NorthWestOutside')
% %     set(0,'defaultAxesFontName', 'Arial')
% %     set(0,'defaultTextFontName', 'Arial')
% end 
% 
% %saveas(gcf,'figS5.png')
% %saveas(gcf,'../../output/figS51.eps','epsc');
% 
% %% OPTION2: Plot Marginaldiff(Ln) or Marginaldiff(Pn)
% 
% % %Plot Marginal difference
% z1m = marginaldifference{1,1}(1,2:numtoplot);
% z2m = marginaldifference{1,1}(2,2:numtoplot);
% z3m = marginaldifference{1,1}(3,2:numtoplot);
% z4m = marginaldifference{1,1}(4,2:numtoplot);
% z5m = marginaldifference{1,1}(5,2:numtoplot); 
% z6m = marginaldifference{1,1}(6,2:numtoplot);
% z7m = marginaldifference{1,1}(7,2:numtoplot);
% 
% %concateante the z, for 3D plot
% zMatMarginalDiff = [z1m; z2m; z3m; z4m; z5m; z6m; z7m]';
% 
% figure
% 
% for g=1:numberoftimepoints
%     plot3(xMat(:,g), yMat(:,g), zMatMarginalDiff(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
%     hold all
%     set(gca,'XTick',2:1:15)   
%     set(gca,'fontsize',18,'LineWidth',1)
%     set(gca,'Fontname','Arial') 
%     axis ([2 15 2 7 -0.15 0.15]);
%     
% grid on;
% view(10,50); %// Adjust viewing angle so you can clearly see data
% % h1=legend('A','B','C','D','E','F', 'G',6);
% %     set(h1,'Interpreter','none')
% %     set(h1,'Location','NorthWestOutside')
% %     set(0,'defaultAxesFontName', 'Arial')
% %     set(0,'defaultTextFontName', 'Arial')
% end 
% 
% 
% %saveas(gcf,'figS52.eps')
% %saveas(gcf,'../../output/figS52.eps','epsc');
% 
