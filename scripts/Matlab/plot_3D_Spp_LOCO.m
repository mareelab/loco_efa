%% Make a 3D figure of Mode, Ln (value) and specified shape with different number of lobes
% Load the av_Ln, av_Md and av_XOR of 3 specified shapes for specified shapes 3 to 10 number of lobes. 
% Note: script separated to generate individual panels of
% Figure S7D

% clear the working space
clc

NumberOfDiffLobes= 8; %From specified shape with 3 lobes to shape with 10 lobes.
combination1=1; %SCS1
combination2=2; %SCS2
combination3=3; %SCS3

for i=1:NumberOfDiffLobes
data_Ln{i}=load(['../../output/FigS7/data/difference_CPM_onecell_pop_lobe0' int2str(i+2) '_3comb.mat']);
end

%Make a 3D plot
% In X is the lobe number (1:20)
% In Z is the value in each lobe number data_Ln{i}.av_Ln(1,1:20)
% In Y the number of lobes: 3 to 10.

numtoplot=12;
%Define x values
x = (2:1:numtoplot).';
xMat =  repmat (x,1,NumberOfDiffLobes);

%Define y values
y = 3:1:10;
yMat = repmat(y, numel(x), 1);


%% Define z values. Ln  

%Plot <Ln> - av.Ln  Combination 1
z3_Ln_1 = data_Ln{1}.av_Ln(combination1,2:numtoplot);
z4_Ln_1 = data_Ln{2}.av_Ln(combination1,2:numtoplot);
z5_Ln_1 = data_Ln{3}.av_Ln(combination1,2:numtoplot);
z6_Ln_1 = data_Ln{4}.av_Ln(combination1,2:numtoplot);
z7_Ln_1 = data_Ln{5}.av_Ln(combination1,2:numtoplot); 
z8_Ln_1 = data_Ln{6}.av_Ln(combination1,2:numtoplot);
z9_Ln_1 = data_Ln{7}.av_Ln(combination1,2:numtoplot);
z10_Ln_1= data_Ln{8}.av_Ln(combination1,2:numtoplot);

%Plot <Ln> - av.Ln  Combination 2
z3_Ln_2 = data_Ln{1}.av_Ln(combination2,2:numtoplot);
z4_Ln_2 = data_Ln{2}.av_Ln(combination2,2:numtoplot);
z5_Ln_2 = data_Ln{3}.av_Ln(combination2,2:numtoplot);
z6_Ln_2 = data_Ln{4}.av_Ln(combination2,2:numtoplot);
z7_Ln_2 = data_Ln{5}.av_Ln(combination2,2:numtoplot); 
z8_Ln_2 = data_Ln{6}.av_Ln(combination2,2:numtoplot);
z9_Ln_2 = data_Ln{7}.av_Ln(combination2,2:numtoplot);
z10_Ln_2= data_Ln{8}.av_Ln(combination2,2:numtoplot);

%Plot <Ln> - av.Ln  Combination 3
z3_Ln_3 = data_Ln{1}.av_Ln(combination3,2:numtoplot);
z4_Ln_3 = data_Ln{2}.av_Ln(combination3,2:numtoplot);
z5_Ln_3 = data_Ln{3}.av_Ln(combination3,2:numtoplot);
z6_Ln_3 = data_Ln{4}.av_Ln(combination3,2:numtoplot);
z7_Ln_3 = data_Ln{5}.av_Ln(combination3,2:numtoplot); 
z8_Ln_3 = data_Ln{6}.av_Ln(combination3,2:numtoplot);
z9_Ln_3 = data_Ln{7}.av_Ln(combination3,2:numtoplot);
z10_Ln_3= data_Ln{8}.av_Ln(combination3,2:numtoplot);

%concateante the z, for 3D plot
zMat_Ln_1 = [z3_Ln_1; z4_Ln_1; z5_Ln_1; z6_Ln_1; z7_Ln_1; z8_Ln_1; z9_Ln_1; z10_Ln_1]';
zMat_Ln_2 = [z3_Ln_2; z4_Ln_2; z5_Ln_2; z6_Ln_2; z7_Ln_2; z8_Ln_2; z9_Ln_2; z10_Ln_2]';
zMat_Ln_3 = [z3_Ln_3; z4_Ln_3; z5_Ln_3; z6_Ln_3; z7_Ln_3; z8_Ln_3; z9_Ln_3; z10_Ln_3]';

%% Define z values. XOR
%Plot <XOR>-av.xor Combination 1
z3_XOR_1 = data_Ln{1}.av_xor(combination1,2:numtoplot);
z4_XOR_1 = data_Ln{2}.av_xor(combination1,2:numtoplot);
z5_XOR_1 = data_Ln{3}.av_xor(combination1,2:numtoplot);
z6_XOR_1 = data_Ln{4}.av_xor(combination1,2:numtoplot);
z7_XOR_1 = data_Ln{5}.av_xor(combination1,2:numtoplot); 
z8_XOR_1 = data_Ln{6}.av_xor(combination1,2:numtoplot);
z9_XOR_1 = data_Ln{7}.av_xor(combination1,2:numtoplot);
z10_XOR_1 = data_Ln{8}.av_xor(combination1,2:numtoplot);

%Plot <XOR>-av.xor Combination 2
z3_XOR_2 = data_Ln{1}.av_xor(combination2,2:numtoplot);
z4_XOR_2 = data_Ln{2}.av_xor(combination2,2:numtoplot);
z5_XOR_2 = data_Ln{3}.av_xor(combination2,2:numtoplot);
z6_XOR_2 = data_Ln{4}.av_xor(combination2,2:numtoplot);
z7_XOR_2 = data_Ln{5}.av_xor(combination2,2:numtoplot); 
z8_XOR_2 = data_Ln{6}.av_xor(combination2,2:numtoplot);
z9_XOR_2 = data_Ln{7}.av_xor(combination2,2:numtoplot);
z10_XOR_2 = data_Ln{8}.av_xor(combination2,2:numtoplot);

%Plot <XOR>-av.xor Combination 3
z3_XOR_3 = data_Ln{1}.av_xor(combination3,2:numtoplot);
z4_XOR_3 = data_Ln{2}.av_xor(combination3,2:numtoplot);
z5_XOR_3 = data_Ln{3}.av_xor(combination3,2:numtoplot);
z6_XOR_3 = data_Ln{4}.av_xor(combination3,2:numtoplot);
z7_XOR_3 = data_Ln{5}.av_xor(combination3,2:numtoplot); 
z8_XOR_3 = data_Ln{6}.av_xor(combination3,2:numtoplot);
z9_XOR_3 = data_Ln{7}.av_xor(combination3,2:numtoplot);
z10_XOR_3 = data_Ln{8}.av_xor(combination3,2:numtoplot);

%concateante the z, for 3D plot
zMat_XOR_1 = [z3_XOR_1; z4_XOR_1; z5_XOR_1; z6_XOR_1; z7_XOR_1; z8_XOR_1; z9_XOR_1; z10_XOR_1]';
zMat_XOR_2 = [z3_XOR_2; z4_XOR_2; z5_XOR_2; z6_XOR_2; z7_XOR_2; z8_XOR_2; z9_XOR_2; z10_XOR_2]';
zMat_XOR_3 = [z3_XOR_3; z4_XOR_3; z5_XOR_3; z6_XOR_3; z7_XOR_3; z8_XOR_3; z9_XOR_3; z10_XOR_3]';

%% Define z values. Marginal difference
% %Plot <Marginal difference> - av.md  Combination 1
z3_Md_1 = data_Ln{1}.av_md(combination1,2:numtoplot);
z4_Md_1 = data_Ln{2}.av_md(combination1,2:numtoplot);
z5_Md_1 = data_Ln{3}.av_md(combination1,2:numtoplot);
z6_Md_1 = data_Ln{4}.av_md(combination1,2:numtoplot);
z7_Md_1 = data_Ln{5}.av_md(combination1,2:numtoplot); 
z8_Md_1 = data_Ln{6}.av_md(combination1,2:numtoplot);
z9_Md_1 = data_Ln{7}.av_md(combination1,2:numtoplot);
z10_Md_1 = data_Ln{8}.av_md(combination1,2:numtoplot);

% %Plot <Marginal difference> - av.md  Combination 2
z3_Md_2 = data_Ln{1}.av_md(combination2,2:numtoplot);
z4_Md_2 = data_Ln{2}.av_md(combination2,2:numtoplot);
z5_Md_2 = data_Ln{3}.av_md(combination2,2:numtoplot);
z6_Md_2 = data_Ln{4}.av_md(combination2,2:numtoplot);
z7_Md_2 = data_Ln{5}.av_md(combination2,2:numtoplot); 
z8_Md_2 = data_Ln{6}.av_md(combination2,2:numtoplot);
z9_Md_2 = data_Ln{7}.av_md(combination2,2:numtoplot);
z10_Md_2 = data_Ln{8}.av_md(combination2,2:numtoplot);

% %Plot <Marginal difference> - av.md  Combination 3
z3_Md_3 = data_Ln{1}.av_md(combination3,2:numtoplot);
z4_Md_3 = data_Ln{2}.av_md(combination3,2:numtoplot);
z5_Md_3 = data_Ln{3}.av_md(combination3,2:numtoplot);
z6_Md_3 = data_Ln{4}.av_md(combination3,2:numtoplot);
z7_Md_3 = data_Ln{5}.av_md(combination3,2:numtoplot); 
z8_Md_3 = data_Ln{6}.av_md(combination3,2:numtoplot);
z9_Md_3 = data_Ln{7}.av_md(combination3,2:numtoplot);
z10_Md_3 = data_Ln{8}.av_md(combination3,2:numtoplot);

%concateante the z, for 3D plot
zMat_Md_1 = [z3_Md_1; z4_Md_1; z5_Md_1; z6_Md_1; z7_Md_1; z8_Md_1; z9_Md_1; z10_Md_1]';
zMat_Md_2 = [z3_Md_2; z4_Md_2; z5_Md_2; z6_Md_2; z7_Md_2; z8_Md_2; z9_Md_2; z10_Md_2]';
zMat_Md_3 = [z3_Md_3; z4_Md_3; z5_Md_3; z6_Md_3; z7_Md_3; z8_Md_3; z9_Md_3; z10_Md_3]';


%% Figures Ln
%colors
cc=hsv(NumberOfDiffLobes);

% Combination 1 
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Ln_1(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7A.eps','epsc')

% Combination 2
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Ln_2(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7B.eps','epsc')

% Combination 3
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Ln_3(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7C.eps','epsc')

%% Figures XOR
%colors
cc=hsv(NumberOfDiffLobes);

% Combination 1 
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_XOR_1(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7D.eps','epsc')

% Combination 2
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_XOR_2(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7E.eps','epsc')

% Combination 3
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_XOR_3(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7F.eps','epsc')

%% Figures Marginal difference
%colors
cc=hsv(NumberOfDiffLobes);

% Combination 1 
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Md_1(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7G.eps','epsc')

% Combination 2
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Md_2(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7H.eps','epsc')

% Combination 3
figure
for g=1:NumberOfDiffLobes
     %plot3(xMat, yMat, zMat, '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:)); %plot3 plots a 3D line plot
     plot3(xMat(:,g), yMat(:,g), zMat_Md_3(:,g), '-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
     hold all
    set(gca,'XTick',2:1:19)
    set(gca,'fontsize',18,'LineWidth',1)
    set(gca,'Fontname','Arial') %// Make all traces blue
    
grid on;
% xlabel('{\itLn}-harmonic','fontSize', 11,'Fontname','Arial')
% ylabel('y'); 
% zlabel('z');
view(10,50); %// Adjust viewing angle so you can clearly see data
% h1=legend('Number of lobes=03','Number of lobes=04','Number of lobes=05','Number of lobes=06','Number of lobes=07','Number of lobes=08', 'Number of lobes=09','Number of lobes=10',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWestOutside')
%     set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
end 

saveas(gcf,'../../output/figS7I.eps','epsc')


