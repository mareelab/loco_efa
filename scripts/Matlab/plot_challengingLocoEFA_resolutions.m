% This script plots the L landscape,Difference(delta Area),Marginal difference, cumulative difference[x,N],entropy of hypothetical cells (there is not time
% implicit).
% Figure of explanation in the main text of the paper:
% challenging LOCO-EFA (rotations)


% clear the working space
clc

NoOfDifferentCells=1; %Number of images or files
Resolution=0;
forslide=0;
DifferentPopulationSameLobes=1;


%Some preallocation
NoOfCells=zeros(1,NoOfDifferentCells);
cellnumber=cell(1,NoOfDifferentCells);
cellarea2=cell(1,NoOfDifferentCells);
cellarea=cell(1,NoOfDifferentCells); %After resolution correction
perimeter2=cell(1,NoOfDifferentCells);
perimeter=cell(1,NoOfDifferentCells); %After resolution correction
inoofharmonicsanalyse=cell(1,NoOfDifferentCells);
cumulativedifference=cell(1,NoOfDifferentCells);
entropy=cell(1,NoOfDifferentCells);
neighbournumber=cell(1,NoOfDifferentCells);
difference=cell(1,NoOfDifferentCells);
L=cell(1,NoOfDifferentCells);
marginaldifference=cell(1,NoOfDifferentCells);
cumulativediference_x=cell(1,NoOfDifferentCells);
average_difference=cell(1,NoOfDifferentCells);
std_difference=cell(1,NoOfDifferentCells);
average_marginaldifference=cell(1,NoOfDifferentCells);
std_marginaldifference=cell(1,NoOfDifferentCells);
average_cd=cell(1,NoOfDifferentCells);
std_cd=cell(1,NoOfDifferentCells);
average_L=cell(1,NoOfDifferentCells);
std_L=cell(1,NoOfDifferentCells);
average_entropy=cell(1,NoOfDifferentCells);
std_entropy=cell(1,NoOfDifferentCells);



%% %Extract the cell data %Load the file with the data
for d = 1:NoOfDifferentCells

 [header, data]=mhdrload(['../../output/FigS10/Output_CellShapeAnalysis/diverse-effects-loco-efa-resolution/diverse-effects-loco-efa-resolution.dat']);
 NoOfCells(d)=size(data,3);
 for j=1:NoOfCells(d)
 cellnumber{1,d}(j,1)=data(1,1,j);
 %timepoint{1,d}(1,j)=data(1,2,j);
 %cellarea(d)=data(:,4)*0.3292; %resolution is 0.5737um per pixel, then 0.5^2 is per pixel^2
 cellarea2{1,d}(j,1)=data(1,4,j);
 perimeter2{1,d}(j,1)=data(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data(1,6,j);
 cumulativedifference{1,d}(j,1)=data(1,7,j);
 entropy{1,d}(j,1)=data(1,8,j);
 lobenumber=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
 difference{1,d}(j,1:lobenumber+1)=data(1,9:lobenumber+9,j);
 L{1,d}(j,1:lobenumber)=data(1,60:end,j);
 end
end
   
%% Adjust the area accordingly with the resolution.Resolution (pixels per micron)

%The marginal difference
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=2:22
        marginaldifference{d}(j,i)=difference{d}(j,i-1)-difference{d}(j,i);
%         if marginaldifference{d}(j,i)<0
%             marginaldifference{d}(j,i)=0;
%         end
        end
    end
end

%Cumulative difference [x,N]. x=1, N=50
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d)
        for i=1:8
cumulativediference_x{1,d}(j,i)=sum(difference{1,d}(j,i:lobenumber-1));
        end
    end
end   

%Calculate average and stardar dev for XOR,Marginal difference,CD,L,
%entropy
if NoOfCells(d)>1
for d=1:NoOfDifferentCells

     for j=1:NoOfCells(d)
         for i=1: lobenumber-1
       average_difference{1,d}(1,i)=mean(difference{1,d}(:,i));
       std_difference{1,d}(1,i)=std(difference{1,d}(:,i));
         end
         for i=1: 22
       average_marginaldifference{1,d}(1,i)=mean(marginaldifference{1,d}(:,i));
       std_marginaldifference{1,d}(1,i)=std(marginaldifference{1,d}(:,i));
         end
       average_cd{1,d}(1,1)=mean(cumulativedifference{1,d}(:,1));
       std_cd{1,d}(1,1)=std(cumulativedifference{1,d}(:,1));
       
       for i=1: lobenumber-2
       average_L{1,d}(1,i)=mean(L{1,d}(:,i));
       std_L{1,d}(1,i)=std(L{1,d}(:,i));
       end
       
       average_entropy{1,d}(1,1)=mean(entropy{1,d}(:,1));
       std_entropy{1,d}(1,1)=std(entropy{1,d}(:,1));
     end  
end

%Calculate the difference between single cell shape descriptors and
%populations
for d=2:NoOfDifferentCells % d=1 is the single cell

     for j=1:NoOfCells(d)
         %XOR
         for i=1: lobenumber-1
             diff_xor_onecell_pop{1,d}(1,i)=average_difference{1,d}(1,i)-difference{1,1}(1,i);
         end
         %Marginaldifference
         for i=1: 22
             diff_margdiff_onecell_pop{1,d}(1,i)=average_marginaldifference{1,d}(1,i)-marginaldifference{1,1}(1,i);
         end
         
          %L
         for i=1: lobenumber-2
             diff_Ln_onecell_pop{1,d}(1,i)=average_L{1,d}(1,i)-L{1,1}(1,i);
         end
          %CD
             diff_cd_onecell_pop{1,d}(1,1)=average_cd{1,d}(:,1)-cumulativedifference{1,1}(1,1);
         
         %entropy
            diff_en_onecell_pop{1,d}(1,1)=average_entropy{1,d}(:,1)-entropy{1,1}(1,1);
         
     end    
end
end

%% plot

for d=1:NoOfDifferentCells
scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3));
  cc=hsv(4);  
  ww=2;
    subplot(1,3,1)
     for j=1:4   %only the first 4 cells 
    hold all
    plot(1:15,L{1,d}(j,1:15),'-o','markerfacecolor',cc(j,:),'LineWidth',ww,'color',cc(j,:))
    xlabel('LOCO-EFA mode','fontSize', 14,'FontName', 'Arial')
    ylabel('Ln','fontSize', 14,'FontName', 'Arial')
    %title('L landscape','fontSize', 14,'FontWeight','bold')
    set(gca,'XTick',0:1:15)
    set(gca,'fontsize',14,'LineWidth',2)  
    end
    
   subplot(1,3,2)
     for j=1:4   
    hold all
    plot(1:1:15,difference{1,d}(j,1:1:15),'-o','markerfacecolor',cc(j,:),'LineWidth',ww,'color',cc(j,:));  
    xlabel('LOCO-EFA mode','fontSize', 14,'FontName', 'Arial')
    ylabel('XOR Difference','fontSize', 14,'FontName', 'Arial')
    set(gca,'XTick',1:1:15)
    %title('XOR Original and Reconstructed','FontSize',14,'FontWeight','bold')
    set(gca,'fontsize',14,'LineWidth',2)  
    end
 
   subplot(1,3,3)
     for j=1:4
    hold all
    plot(1:1:15,marginaldifference{d}(j,1:15),'-o','markerfacecolor',cc(j,:),'LineWidth',ww,'color',cc(j,:));
    xlabel('LOCO-EFA mode','fontSize', 14,'FontName', 'Arial')
    ylabel('Marginal Difference','fontSize', 14,'FontName', 'Arial')
    %title('Marginal Difference ({\itd(XOR)} / {\itdL})','FontSize',14,'FontWeight','bold')
    set(gca,'XTick',1:1:15)
    set(gca,'fontsize',14,'LineWidth',2)  
   end

end

% Get dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
% print to save as
%print('-dpng', '../../output/figS10B.png', '-r100');
saveas(gcf,'../../output/figS10D.eps','epsc');
% drawnow
% set(gcf,'Units',oldscreenunits,'PaperUnits',oldpaperunits,'PaperPosition',oldpaperpos)
