%plot the difference between one cell and the population for 3 different
%runs of the same model
%The first image correspond to the single cell. For example:Cellshapeanalysis_CPMcells_lobe02_comb1_or0_el1_bo0_1.data
%The second and until the fourth are individual populations:Cellshapeanalysis_CPMcells_lobe02_comb1_or0_el1_bo0_2.data, etc.

% The script is giving as an output the difference between the average of the population and the single cell (stored in the variables av_Ln,
% av_Md, av_XOR, and their standar deviation st_Ln, st_Md, st_XOR). It
% takes a particular number of lobe (X= 3,4,5,6,7,8,9,10) for the three
% combinations. The output is reading again to make a 3D plot.

%% Some options

% clear the working space
clc

NoOfDifferentCells=4; %Number of images or files, it assumes all combinations will have the same number of files
NoOfCombinations=3;
noofpopulations=3;
Resolution=0;
arearestriction=0;
sizethreshold=600;

plot_standarerror=0;


%% Some preallocation

NoOfCells=zeros(NoOfDifferentCells,NoOfCombinations);
cellarea1=cell(1,NoOfDifferentCells);
cellarea2=cell(1,NoOfDifferentCells);
cellarea3=cell(1,NoOfDifferentCells);
%cellarea=cell(1,NoOfDifferentCells); %After resolution correction
perimeter1=cell(1,NoOfDifferentCells);
perimeter2=cell(1,NoOfDifferentCells);
perimeter3=cell(1,NoOfDifferentCells);
%perimeter=cell(1,NoOfDifferentCells); %After resolution correction
inoofharmonicsanalyse=cell(1,NoOfDifferentCells); %all will be analyzed using the same number of harmonics
cumulativedifference1=cell(1,NoOfDifferentCells);
cumulativedifference2=cell(1,NoOfDifferentCells);
cumulativedifference3=cell(1,NoOfDifferentCells);
entropy1=cell(1,NoOfDifferentCells);
entropy2=cell(1,NoOfDifferentCells);
entropy3=cell(1,NoOfDifferentCells);
neighbournumber1=cell(1,NoOfDifferentCells); 
neighbournumber2=cell(1,NoOfDifferentCells);
neighbournumber3=cell(1,NoOfDifferentCells);
difference1=cell(1,NoOfDifferentCells);
difference2=cell(1,NoOfDifferentCells);
difference3=cell(1,NoOfDifferentCells);
L_1=cell(1,NoOfDifferentCells);
L_2=cell(1,NoOfDifferentCells);
L_3=cell(1,NoOfDifferentCells);
marginaldifference1=cell(1,NoOfDifferentCells);
marginaldifference2=cell(1,NoOfDifferentCells);
marginaldifference3=cell(1,NoOfDifferentCells);
cumulativedifference_x1=cell(1,NoOfDifferentCells);
cumulativedifference_x2=cell(1,NoOfDifferentCells);
cumulativedifference_x3=cell(1,NoOfDifferentCells);
average_difference1=cell(1,NoOfDifferentCells);
average_difference2=cell(1,NoOfDifferentCells);
average_difference3=cell(1,NoOfDifferentCells);
std_difference1=cell(1,NoOfDifferentCells);
std_difference2=cell(1,NoOfDifferentCells);
std_difference3=cell(1,NoOfDifferentCells);
average_marginaldifference1=cell(1,NoOfDifferentCells);
average_marginaldifference2=cell(1,NoOfDifferentCells);
average_marginaldifference3=cell(1,NoOfDifferentCells);
std_marginaldifference1=cell(1,NoOfDifferentCells);
std_marginaldifference2=cell(1,NoOfDifferentCells);
std_marginaldifference3=cell(1,NoOfDifferentCells);
average_cd1=cell(1,NoOfDifferentCells);
average_cd2=cell(1,NoOfDifferentCells);
average_cd3=cell(1,NoOfDifferentCells);
std_cd1=cell(1,NoOfDifferentCells);
std_cd2=cell(1,NoOfDifferentCells);
std_cd3=cell(1,NoOfDifferentCells);
average_L1=cell(1,NoOfDifferentCells);
average_L2=cell(1,NoOfDifferentCells);
average_L3=cell(1,NoOfDifferentCells);
std_L1=cell(1,NoOfDifferentCells);
std_L2=cell(1,NoOfDifferentCells);
std_L3=cell(1,NoOfDifferentCells);
average_entropy1=cell(1,NoOfDifferentCells);
average_entropy2=cell(1,NoOfDifferentCells);
average_entropy3=cell(1,NoOfDifferentCells);
std_entropy1=cell(1,NoOfDifferentCells);
std_entropy2=cell(1,NoOfDifferentCells);
std_entropy3=cell(1,NoOfDifferentCells);

%% Load the data and extract the cell data 
%combination1
for d = 1:NoOfDifferentCells
  
% Specified shape lobe 03 combination 1
    [header, data1] = mhdrload(['../../output/FigS7/data/Cellshapeanalysis_CPM_lobe03_comb1_or0_el1_bo0_' int2str(d) '.dat']);
        
 NoOfCells(d,1)=size(data1,3);
 for j=1:NoOfCells(d,1)
 %cellnumber{1,d}(j,1)=data2(1,1,j);
 cellarea1{1,d}(j,1)=data1(1,4,j);
 perimeter1{1,d}(j,1)=data1(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data1(1,6,j);
 cumulativedifference1{1,d}(j,1)=data1(1,7,j);
 entropy1{1,d}(j,1)=data1(1,8,j);
 lobenumber1=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
 difference1{1,d}(j,1:lobenumber1+1)=data1(1,9:lobenumber1+9,j);
 L_1{1,d}(j,1:lobenumber1)=data1(1,60:end,j);
 end
end

%combination2
for d = 1:NoOfDifferentCells

% Specified shape lobe 03 combination 2
     [header, data2] = mhdrload(['../../output/FigS7/data/Cellshapeanalysis_CPM_lobe03_comb2_or0_el1_bo0_' int2str(d) '.dat']);
  
NoOfCells(d,2)=size(data2,3);
 for j=1:NoOfCells(d,2)
 %cellnumber{1,d}(j,1)=data2(1,1,j);
 cellarea2{1,d}(j,1)=data2(1,4,j);
 perimeter2{1,d}(j,1)=data2(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data2(1,6,j);
 cumulativedifference2{1,d}(j,1)=data2(1,7,j);
 entropy2{1,d}(j,1)=data2(1,8,j);
 lobenumber2=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
 difference2{1,d}(j,1:lobenumber2+1)=data2(1,9:lobenumber2+9,j);
 L_2{1,d}(j,1:lobenumber2)=data2(1,60:end,j);
 end
end

%combination3
for d = 1:NoOfDifferentCells
% Specified shape lobe 03 combination 3
      [header, data3] = mhdrload(['../../output/FigS7/data/Cellshapeanalysis_CPM_lobe03_comb3_or0_el1_bo0_' int2str(d) '.dat']);
     
 NoOfCells(d,3)=size(data3,3);
 for j=1:NoOfCells(d,3)
 %cellnumber{1,d}(j,1)=data2(1,1,j);
 cellarea3{1,d}(j,1)=data3(1,4,j);
 perimeter3{1,d}(j,1)=data3(1,5,j);
 inoofharmonicsanalyse{1,d}(j,1)=data3(1,6,j);
 cumulativedifference3{1,d}(j,1)=data3(1,7,j);
 entropy3{1,d}(j,1)=data3(1,8,j);
 lobenumber3=inoofharmonicsanalyse{1,1}(1,1); %Assuming that all the cells were processed with the same number
 difference3{1,d}(j,1:lobenumber3+1)=data3(1,9:lobenumber3+9,j);
 L_3{1,d}(j,1:lobenumber3)=data3(1,60:end,j);
 end
end


%% Calculate averages for the populations


%The marginal difference
for d=1:NoOfDifferentCells
    %combination1
    for j=1:NoOfCells(d,1)
        for i=2:22 %because no element at (1,0)
        marginaldifference1{d}(j,i)=difference1{d}(j,i-1)-difference1{d}(j,i);
        end
    end
    %combination2
     for j=1:NoOfCells(d,2)
        for i=2:22
        marginaldifference2{d}(j,i)=difference2{d}(j,i-1)-difference2{d}(j,i);
        end
    end 
     %combination3
    for j=1:NoOfCells(d,3)
        for i=2:22
        marginaldifference3{d}(j,i)=difference3{d}(j,i-1)-difference3{d}(j,i);
        end
    end
     
end

%Cumulative difference [x,N]. x=1, N=50
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d,1)
        for i=1:8 %In eac
cumulativedifference_x1{1,d}(j,i)=sum(difference1{1,d}(j,i:lobenumber1-1));
        end
    end
end   

for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d,2)
        for i=1:8 %In eac
cumulativedifference_x2{1,d}(j,i)=sum(difference2{1,d}(j,i:lobenumber2-1));
        end
    end
end 
for d=1:NoOfDifferentCells
    for j=1:NoOfCells(d,3)
        for i=1:8 %In eac
cumulativedifference_x3{1,d}(j,i)=sum(difference3{1,d}(j,i:lobenumber3-1));
        end
    end
end 
%Calculate average and stardar dev for XOR,Marginal difference,CD,L,
%entropy

%if NoOfCells(d,:)>1
    
 if (arearestriction==1)
%     %Find the index of cellarea>600
    for d=1:NoOfDifferentCells
        %comb1
       for j=1:NoOfCells(d,1)
       if (cellarea1{1,d}(j) > sizethreshold) 
           biggercells1{d}(j)=1;
       else
           biggercells1{d}(j)=0;
       end
       end
       %comb2
       for j=1:NoOfCells(d,2)
       if (cellarea2{1,d}(j) > sizethreshold) 
           biggercells2{d}(j)=1;
       else
           biggercells2{d}(j)=0;
       end
       end 
       
       %comb3
         for j=1:NoOfCells(d,3)
       if (cellarea3{1,d}(j) > sizethreshold) 
           biggercells3{d}(j)=1;
       else
           biggercells3{d}(j)=0;
       end
         end
       
    end 
    
    for d=1:NoOfDifferentCells
        ind1{1,d}=find(biggercells1{d}(:)==1);
        ind2{1,d}=find(biggercells2{d}(:)==1);
        ind3{1,d}=find(biggercells3{d}(:)==1);
    end  
    
   for d=1:NoOfDifferentCells
%Combination1  %average_difference{1,d}(1,i)=mean(difference{1,d}(ind{d}(:),i));
    for j=1:NoOfCells(d,1)
         
         for i=1: lobenumber1-1 %XOR
       average_difference1{1,d}(1,i)=mean(difference1{1,d}(ind1{d}(:),i));
       std_difference1{1,d}(1,i)=std(difference1{1,d}(ind1{d}(:),i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference1{1,d}(1,i)=mean(marginaldifference1{1,d}(ind1{d}(:),i));
       std_marginaldifference1{1,d}(1,i)=std(marginaldifference1{1,d}(ind1{d}(:),i));
         end
       average_cd1{1,d}(1,1)=mean(cumulativedifference_x1{1,d}(ind1{d}(:),1));
       std_cd1{1,d}(1,1)=std(cumulativedifference_x1{1,d}(ind1{d}(:),1));
       
       for i=1: lobenumber1-2
       average_L1{1,d}(1,i)=mean(L_1{1,d}(ind1{d}(:),i));
       std_L1{1,d}(1,i)=std(L_1{1,d}(ind1{d}(:),i));
       end
       
       average_entropy1{1,d}(1,1)=mean(entropy1{1,d}(ind1{d}(:),1));
       std_entropy1{1,d}(1,1)=std(entropy1{1,d}(ind1{d}(:),1));
    end
 %Combination2
    for j=1:NoOfCells(d,2)
         
         for i=1: lobenumber2-1 %XOR
       average_difference2{1,d}(1,i)=mean(difference2{1,d}(ind2{d}(:),i));
       std_difference2{1,d}(1,i)=std(difference2{1,d}(ind2{d}(:),i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference2{1,d}(1,i)=mean(marginaldifference2{1,d}(ind2{d}(:),i));
       std_marginaldifference2{1,d}(1,i)=std(marginaldifference2{1,d}(ind2{d}(:),i));
         end
       average_cd2{1,d}(1,1)=mean(cumulativedifference_x2{1,d}(ind2{d}(:),1));
       std_cd2{1,d}(1,1)=std(cumulativedifference_x2{1,d}(ind2{d}(:),1));
       
       for i=1: lobenumber2-2
       average_L2{1,d}(1,i)=mean(L_2{1,d}(ind2{d}(:),i));
       std_L2{1,d}(1,i)=std(L_2{1,d}(ind2{d}(:),i));
       end
       
       average_entropy2{1,d}(1,1)=mean(entropy2{1,d}(ind2{d}(:),1));
       std_entropy2{1,d}(1,1)=std(entropy2{1,d}(ind2{d}(:),1));
    end
    
 %Combination3
   for j=1:NoOfCells(d,3)
         
         for i=1: lobenumber3-1 %XOR
       average_difference3{1,d}(1,i)=mean(difference3{1,d}(ind3{d}(:),i));
       std_difference3{1,d}(1,i)=std(difference3{1,d}(ind3{d}(:),i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference3{1,d}(1,i)=mean(marginaldifference3{1,d}(ind3{d}(:),i));
       std_marginaldifference3{1,d}(1,i)=std(marginaldifference3{1,d}(ind3{d}(:),i));
         end
       average_cd3{1,d}(1,1)=mean(cumulativedifference_x3{1,d}(ind3{d}(:),1));
       std_cd3{1,d}(1,1)=std(cumulativedifference_x3{1,d}(ind3{d}(:),1));
       
       for i=1: lobenumber3-2
       average_L3{1,d}(1,i)=mean(L_3{1,d}(ind3{d}(:),i));
       std_L3{1,d}(1,i)=std(L_3{1,d}(ind3{d}(:),i));
       end
       
       average_entropy3{1,d}(1,1)=mean(entropy3{1,d}(ind3{d}(:),1));
       std_entropy3{1,d}(1,1)=std(entropy3{1,d}(ind3{d}(:),1));
   end 
 end
 else
    
 for d=1:NoOfDifferentCells
%Combination1
    for j=1:NoOfCells(d,1)
         
         for i=1: lobenumber1-1 %XOR
       average_difference1{1,d}(1,i)=mean(difference1{1,d}(:,i));
       std_difference1{1,d}(1,i)=std(difference1{1,d}(:,i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference1{1,d}(1,i)=mean(marginaldifference1{1,d}(:,i));
       std_marginaldifference1{1,d}(1,i)=std(marginaldifference1{1,d}(:,i));
         end
       average_cd1{1,d}(1,1)=mean(cumulativedifference_x1{1,d}(:,1));
       std_cd1{1,d}(1,1)=std(cumulativedifference_x1{1,d}(:,1));
       
       for i=1: lobenumber1-2
       average_L1{1,d}(1,i)=mean(L_1{1,d}(:,i));
       std_L1{1,d}(1,i)=std(L_1{1,d}(:,i));
       end
       
       average_entropy1{1,d}(1,1)=mean(entropy1{1,d}(:,1));
       std_entropy1{1,d}(1,1)=std(entropy1{1,d}(:,1));
    end
 %Combination2
    for j=1:NoOfCells(d,2)
         
         for i=1: lobenumber2-1 %XOR
       average_difference2{1,d}(1,i)=mean(difference2{1,d}(:,i));
       std_difference2{1,d}(1,i)=std(difference2{1,d}(:,i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference2{1,d}(1,i)=mean(marginaldifference2{1,d}(:,i));
       std_marginaldifference2{1,d}(1,i)=std(marginaldifference2{1,d}(:,i));
         end
       average_cd2{1,d}(1,1)=mean(cumulativedifference_x2{1,d}(:,1));
       std_cd2{1,d}(1,1)=std(cumulativedifference_x2{1,d}(:,1));
       
       for i=1: lobenumber2-2
       average_L2{1,d}(1,i)=mean(L_2{1,d}(:,i));
       std_L2{1,d}(1,i)=std(L_2{1,d}(:,i));
       end
       
       average_entropy2{1,d}(1,1)=mean(entropy2{1,d}(:,1));
       std_entropy2{1,d}(1,1)=std(entropy2{1,d}(:,1));
    end
    
 %Combination3
   for j=1:NoOfCells(d,3)
         
         for i=1: lobenumber3-1 %XOR
       average_difference3{1,d}(1,i)=mean(difference3{1,d}(:,i));
       std_difference3{1,d}(1,i)=std(difference3{1,d}(:,i));
         end
         for i=1: 22 %Marginal difference
       average_marginaldifference3{1,d}(1,i)=mean(marginaldifference3{1,d}(:,i));
       std_marginaldifference3{1,d}(1,i)=std(marginaldifference3{1,d}(:,i));
         end
       average_cd3{1,d}(1,1)=mean(cumulativedifference_x3{1,d}(:,1));
       std_cd3{1,d}(1,1)=std(cumulativedifference_x3{1,d}(:,1));
       
       for i=1: lobenumber3-2
       average_L3{1,d}(1,i)=mean(L_3{1,d}(:,i));
       std_L3{1,d}(1,i)=std(L_3{1,d}(:,i));
       end
       
       average_entropy3{1,d}(1,1)=mean(entropy3{1,d}(:,1));
       std_entropy3{1,d}(1,1)=std(entropy3{1,d}(:,1));
    end 
 end
end



%% Calculate the differences between one cell and populations
%Calculate the difference between single cell shape descriptors and
%populations
lobenumber=lobenumber1; % assuming that all lobenumber is the same
for d=1:NoOfDifferentCells % d=1 is the single cell, so the difference with itself must be 0
     
     %for j=1:NoOfCells(d) %combination(1-3), population(1-4),n (1-inoofharmonics or truncate before).
         %XOR
         for i=1: lobenumber-1
             diff_xor_onecell_pop(1,d,i)=average_difference1{1,d}(1,i)-difference1{1,1}(1,i);
             diff_xor_onecell_pop(2,d,i)=average_difference2{1,d}(1,i)-difference2{1,1}(1,i);
             diff_xor_onecell_pop(3,d,i)=average_difference3{1,d}(1,i)-difference3{1,1}(1,i);
         end
         %Marginaldifference
         for i=1: 22 %combination(1-3), population(1-4),n (1-inoofharmonics or truncate before).
             diff_margdiff_onecell_pop(1,d,i)=average_marginaldifference1{1,d}(1,i)-marginaldifference1{1,1}(1,i);
             diff_margdiff_onecell_pop(2,d,i)=average_marginaldifference2{1,d}(1,i)-marginaldifference2{1,1}(1,i);
             diff_margdiff_onecell_pop(3,d,i)=average_marginaldifference3{1,d}(1,i)-marginaldifference3{1,1}(1,i);
         end
         
          %L
         for i=1: lobenumber-2 %combination(1-3), population(1-4),n (1-inoofharmonics or truncate before).
             diff_Ln_onecell_pop(1,d,i)=average_L1{1,d}(1,i)-L_1{1,1}(1,i);
             diff_Ln_onecell_pop(2,d,i)=average_L2{1,d}(1,i)-L_2{1,1}(1,i);
             diff_Ln_onecell_pop(3,d,i)=average_L3{1,d}(1,i)-L_3{1,1}(1,i);
         end
          %CD
             diff_cd_onecell_pop(1,d)=average_cd1{1,d}(:,1)-cumulativedifference_x1{1,1}(1,1);
             diff_cd_onecell_pop(2,d)=average_cd2{1,d}(:,1)-cumulativedifference_x2{1,1}(1,1);
             diff_cd_onecell_pop(3,d)=average_cd3{1,d}(:,1)-cumulativedifference_x3{1,1}(1,1);
         %entropy
             diff_en_onecell_pop(1,d)=average_entropy1{1,d}(:,1)-entropy1{1,1}(1,1);
             diff_en_onecell_pop(2,d)=average_entropy2{1,d}(:,1)-entropy2{1,1}(1,1);
             diff_en_onecell_pop(3,d)=average_entropy3{1,d}(:,1)-entropy3{1,1}(1,1);
     %end    
end

%% Calculate the differences among populations, mean of the difference between a single cell and three populations (runs of the same model)
for g=1:3 %combinations
    %for p=2:noofpopulations+1 %combination(1-3), population(1-4),n (1-inoofharmonics or truncate before ie. 20 here).
        for i=1:20
av_Ln(g,i)=mean(diff_Ln_onecell_pop(g,2:4,i));
av_xor(g,i)=mean(diff_xor_onecell_pop(g,2:4,i));
av_md(g,i)=mean(diff_margdiff_onecell_pop(g,2:4,i));
std_xor(g,i)=std(diff_xor_onecell_pop(g,2:4,i));
std_Ln(g,i)=std(diff_Ln_onecell_pop(g,2:4,i));
std_md(g,i)=std(diff_margdiff_onecell_pop(g,2:4,i));
        end
av_cd(g)=mean(diff_cd_onecell_pop(g,2:4));
av_en(g)=mean(diff_en_onecell_pop(g,2:4));
std_cd(g)=std(diff_cd_onecell_pop(g,2:4));
std_en(g)=std(diff_en_onecell_pop(g,2:4));
    %end
end

%standar error= std / sqrt(n-1)
for g=1:3 %combinations 
        for i=1:20
se_xor(g,i)=std_xor(g,i)/sqrt(noofpopulations-1);%noofpopulations-1, because it is a sample.
se_Ln(g,i)=std_Ln(g,i)/sqrt(noofpopulations-1);
se_md(g,i)=std_md(g,i)/sqrt(noofpopulations-1);
        end 
se_cd(g)=std_cd(g)/sqrt(noofpopulations-1);
se_en(g)=std_en(g)/sqrt(noofpopulations-1);
end

%% Save the variables to be read by the Matlab script to make 3D plots: plot3DSppLoco_XOR.m, etc.

save('../../output/FigS7/data/difference_CPM_onecell_pop_lobe03_3comb','av_Ln','av_xor','av_md')



%% Plot differences for combination in one single plot: L numbers, XOR,CD,MarginalD,entropy,Plot differences for combination in one single plot
if (plot_standarerror==1)
    size=16;
scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3)); 
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
%cc=hsv(noofpopulations);
cc=[1 0 0; 0.4 0.8 0.2; 0.2 0.2 0.8];
k=[0:1:noofpopulations+1];

for g=1:NoOfCombinations
    %L landscape
    subplot(1,4,1)
    hold all
    errorbar(2:15,av_Ln(g,2:15),se_Ln(g,2:15),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:))
%     xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
%     ylabel('{\itLn}','fontSize', 14,'Fontname','Arial')
    %title('Difference one cell and population L landscape','fontSize', 9,'FontWeight','bold')
    set(gca,'XTick',0:1:15)
    set(gca,'fontsize',size,'LineWidth',1)
    set(gca,'Fontname','Arial')
    axis tight
    h1=legend('ssp1','ssp2','ssp3',6);
    set(h1,'Interpreter','none')
    set(h1,'Location','NorthWestOutside')
    set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
    box on
    hold off
  
    %XOR Difference
    subplot(1,4,2)
    hold all
    errorbar(1:1:15,av_xor(g,1:1:15),se_xor(g,1:1:15),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
%     xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
%     ylabel(' XOR difference','FontSize',14,'Fontname','Arial')
    set(gca,'XTick',1:1:15)
    axis tight
    %title('Difference one cell and populations XOR','FontSize',9,'FontWeight','bold')
    set(gca,'fontsize',size,'LineWidth',1)
    set(gca,'Fontname','Arial')
    set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
    box on
    %set(gca,'fontsize',12,'LineWidth',2)
%     set(gca,'fontsize',10,'LineWidth',2)
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWest')
    hold off
    
 %Marginal difference
    subplot(1,4,3)
    hold all
    errorbar(1:1:14,av_md(g,1:14),se_md(g,1:14),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
    %xlabel('Lobe number','FontSize',14,'Fontname','Arial')
%     xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
%     ylabel(' \itd(XOR) / \itdL \rmdifference','FontSize',14,'Fontname','Arial')
    %title('Difference one cell and population ({\itd(XOR)} / {\itdL})','FontSize',8,'FontWeight','bold')
    set(gca,'XTick',1:1:14)
    set(gca,'fontsize',size,'LineWidth',1)
    set(gca,'Fontname','Arial')
    box on
    set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWest')
    hold off
    
 %Cumulative difference
   subplot(1,4,4)
   hold all
   %plot(0,0, 'w')
   errorbar(1, av_cd(g),se_cd(g),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
   %plot(0,0, 'w') 
   %title('Difference one cell and population cd and entropy','FontSize',9,'FontWeight','bold')
   %ylabel('Cumulative difference','FontSize',14)
   box on
   set(gca, 'XTick',0:noofpopulations, 'XTickLabel',{ '' 'cd' 'entropy' '' },'FontSize',11);
   set(gca,'fontsize',size,'LineWidth',1)
   set(gca,'Fontname','Arial')
     set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
   hold off
    
    %Entropy
    %subplot(1,5,5)
   % plot(0,0, 'w')
    hold all
    %plot(0,0, 'w')
    errorbar(2,av_en(g),se_en(g),'-o','LineWidth',0.5,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
    %plot(0,0, 'w') 
    %title('Difference one cell and population entropy','FontSize',9,'FontWeight','bold')
    %ylabel('Entropy','FontSize',14)
    box on
%     set(gca,'fontsize',12,'LineWidth',2)
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(gca,'fontsize',10,'LineWidth',2);
%     set(gca, 'XTick',0:noofpopulations, 'XTickLabel',{ '' 'cd' 'entropy' '' },'FontSize',11);
    set(gca,'fontsize',size,'LineWidth',1)
    set(gca,'Fontname','Arial')
      set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
   axis ([0 3 -3 0.2]);
    hold off
   
   
    
end
% hax = axes('Position', [.235, .725, .05, .17]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height
hax = axes('Position', [.22, .72, .05, .17]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height
% hax = axes('Position', [.15, .25, .05, .17]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height
for g=1:NoOfCombinations
    hold all   
    errorbar(1:7,av_Ln(g,1:7),se_Ln(g,1:7),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:))
    set(gca,'XTick',1:1:7)
    set(gca,'fontsize',11,'LineWidth',1)
    set(gca,'Fontname','Arial')
    xlim([1 7])
    box on
    hold off
end

else
 scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3)); 
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
%cc=hsv(noofpopulations);
cc=[1 0 0; 0.4 0.8 0.2; 0.2 0.2 0.8];
k=[0:1:noofpopulations+1];
for g=1:NoOfCombinations
    %L landscape
    subplot(1,4,1)
    hold all
    errorbar(2:15,av_Ln(g,2:15),std_Ln(g,2:15),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:))
    xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
    ylabel('{\itLn}','fontSize', 14,'Fontname','Arial')
    %title('Difference one cell and population L landscape','fontSize', 9,'FontWeight','bold')
    set(gca,'XTick',0:1:15)
    set(gca,'fontsize',14,'LineWidth',1)
    set(gca,'Fontname','Arial')
    axis tight
    h1=legend('ssp1','ssp2','ssp3',6);
    set(h1,'Interpreter','none')
    set(h1,'Location','NorthWestOutside')
    box on
    hold off
  
    %XOR Difference
    subplot(1,4,2)
    hold all
    errorbar(1:1:15,av_xor(g,1:1:15),std_xor(g,1:1:15),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
    xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
    ylabel(' XOR difference','FontSize',14,'Fontname','Arial')
    set(gca,'XTick',1:1:15)
    axis tight
    %title('Difference one cell and populations XOR','FontSize',9,'FontWeight','bold')
    set(gca,'fontsize',14,'LineWidth',1)
    set(gca,'Fontname','Arial')
    box on
    %set(gca,'fontsize',12,'LineWidth',2)
%     set(gca,'fontsize',10,'LineWidth',2)
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWest')
    hold off
    
 %Marginal difference
    subplot(1,4,3)
    hold all
    errorbar(1:1:14,av_md(g,1:14),std_md(g,1:14),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
    %xlabel('Lobe number','FontSize',14,'Fontname','Arial')
    xlabel('{\itLn}-harmonic','fontSize', 14,'Fontname','Arial')
    ylabel(' \itd(XOR) / \itdL \rmdifference','FontSize',14,'Fontname','Arial')
    %title('Difference one cell and population ({\itd(XOR)} / {\itdL})','FontSize',8,'FontWeight','bold')
    set(gca,'XTick',1:1:14)
    set(gca,'fontsize',14,'LineWidth',1)
    set(gca,'Fontname','Arial')
    box on
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(h1,'Interpreter','none')
%     set(h1,'Location','NorthWest')
    hold off
    
 %Cumulative difference
   subplot(1,4,4)
   hold all
   %plot(0,0, 'w')
   errorbar(1, av_cd(g),std_cd(g),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
   %plot(0,0, 'w') 
   %title('Difference one cell and population cd and entropy','FontSize',9,'FontWeight','bold')
   %ylabel('Cumulative difference','FontSize',14)
   box on
   set(gca, 'XTick',0:noofpopulations, 'XTickLabel',{ '' 'cd' 'entropy' '' },'FontSize',11);
   set(gca,'fontsize',14,'LineWidth',1)
   set(gca,'Fontname','Arial')
   hold off
    
    %Entropy
    %subplot(1,5,5)
   % plot(0,0, 'w')
    hold all
    %plot(0,0, 'w')
    errorbar(2,av_en(g),std_en(g),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:));
    %plot(0,0, 'w') 
    %title('Difference one cell and population entropy','FontSize',9,'FontWeight','bold')
    %ylabel('Entropy','FontSize',14)
    box on
%     set(gca,'fontsize',12,'LineWidth',2)
%     h1=legend('OneCell','Pop01','Pop02','Pop03','Pop04','Pop05',6);
%     set(gca,'fontsize',10,'LineWidth',2);
    set(gca, 'XTick',0:noofpopulations, 'XTickLabel',{ '' 'cd' 'entropy' '' },'FontSize',11);
    set(gca,'fontsize',14,'LineWidth',1)
    set(gca,'Fontname','Arial')
    hold off
   
   
    
end
%hax = axes('Position', [.21, .50, .05, .17]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height
hax = axes('Position', [.22, .5, .05, .17]); % box location 1: left-rigth   2:up-down   box size 3: width   4:height
for g=1:NoOfCombinations
    hold all   
    errorbar(1:7,av_Ln(g,1:7),std_Ln(g,1:7),'-o','LineWidth',1,'color',cc(g,:),'MarkerFaceColor',cc(g,:))
    set(gca,'XTick',1:1:7)
    set(gca,'fontsize',11,'LineWidth',1)
    set(gca,'Fontname','Arial')
    xlim([1 7])
    box on
    hold off
end
end
