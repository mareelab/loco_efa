%% This script plots the cell outlines of similar shapes and different amplitude

% clear the working space
clc
 
%New on 10/07
[header,data]=mhdrload('../../output/Fig3/Output_CellShapeAnalysis/LOCO_geometricaloutlines_amplitude/LOCO_geometricaloutlines_amplitude.dat');

res=0.36*0.36;
resolution=0;
smoothing=1;
areanorm=1;
plotcells=1;
centerOfMass=0;

%EXTRACT THE DATA FROM THE SPM OUTPUT
sigma = sscanf(header(1,:,:),'%*s %*s %d');
maxsigma=max(sigma);
NoOfCells=length(header(1,1,:));
%NoOfCells=1
index= data(:,1,1:NoOfCells);
for i=1:NoOfCells
ind(:,i)= index(:,1,i);
end

x= data(:,2,1:NoOfCells);
for i=1:NoOfCells
cellx(:,i)= x(:,1,i);
end

y= data(:,3,1:NoOfCells);
for i=1:NoOfCells
celly(:,i)= y(:,1,i);
end

 
%this a vector containing the number of data points

maximo= max(ind);
numeromaximo=ind(1:end,1);
nummax=length(numeromaximo);


%Create an cell array and erase the zeros

dimensions= ones(1,NoOfCells);
%Create an cell array x
cx=mat2cell(cellx,[nummax],dimensions); 

%%eliminate the zeros of each cell
for i=1:NoOfCells
cx{i}(maximo(i)+1:end)=[];
cx_original{i}=cx{i};
end

%Create an cell array y
cy=mat2cell(celly,[nummax],dimensions); 

%eliminate the zeros of each cell
for i=1:NoOfCells
cy{i}(maximo(i)+1:end)=[];
cy_original{i}=cy{i};
end

%perimeter
cellperimeters= data(:,6,1:NoOfCells);
for i=1:NoOfCells
cellperimeter(i,:)= cellperimeters(maximo(1,i),1,i);
end
 
cellareas= data(:,7,1:NoOfCells);
 for i=1:NoOfCells
cellarea(i,:)= cellareas(1,1,i);
 end
 
  if resolution==1
     for i=1:NoOfCells
   cellarea(i,:)=cellarea(i,:)*res; 
   cellperimeter(i,:)=cellperimeter(i,:)*sqrt(res);
     end 
  
   end
 %%Formfactor o circularity
 
 for j=1:NoOfCells
     formfactor(j,:)=(cellperimeter(j,1)*cellperimeter(j,1))/(4*pi*cellarea(j,1));
 end

%Normalize by area (?)

if areanorm==1
for j=1:NoOfCells
    for i=1:maximo(j)
    cx{1,j}(i,1)=cx{1,j}(i,1)/sqrt(cellarea(j,1));
    cy{1,j}(i,1)=cy{1,j}(i,1)/sqrt(cellarea(j,1));
    end
end    
end


        for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end

% % % Smoothing the outline
  %Smoothting the outline (once)
  NoOfSmoothing=5;
       

 if smoothing==1
     for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end

  for j=1:NoOfCells
  xs{1,j}(1,1)=cx{1,j}(iNoOfPoints(j)+1,1)/4 + cx{1,j}(1,1)/2 + cx{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,1)=cx{1,j}(i-1,1)/4 + cx{1,j}(i,1)/2 + cx{1,j}(i+1,1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,1)=cx{1,j}(maximo(j),1)/4 + cx{1,j}(iNoOfPoints(j)+1,1)/2 + cx{1,j}(1,1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,1)=xs{1,j}(1,1);
  
  ys{1,j}(1,1)=cy{1,j}(iNoOfPoints(j)+1,1)/4 + cy{1,j}(1,1)/2 + cy{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  ys{1,j}(i,1)=cy{1,j}(i-1,1)/4 + cy{1,j}(i,1)/2 + cy{1,j}(i+1,1)/4;
  end
  ys{1,j}(iNoOfPoints(j)+1,1)=cy{1,j}(iNoOfPoints(j),1)/4 + cy{1,j}(iNoOfPoints(j)+1,1)/2 + cy{1,j}(1,1)/4;
  %Close the contour
  ys{1,j}(iNoOfPoints(j)+2,1)=ys{1,j}(1,1);

  end
  
   %Smoothting the outline (several times)
   for j=1:NoOfCells
   for g=2:NoOfSmoothing
    xs{1,j}(1,g)= xs{1,j}(iNoOfPoints(j)+1,g-1)/4 + xs{1,j}(1,g-1)/2 + xs{1,j}(2,g-1)/4;
    ys{1,j}(1,g)= ys{1,j}(iNoOfPoints(j)+1,g-1)/4 + ys{1,j}(1,g-1)/2 + ys{1,j}(2,g-1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,g)=xs{1,j}(i-1,g-1)/4 + xs{1,j}(i,g-1)/2 + xs{1,j}(i+1,g-1)/4;
  ys{1,j}(i,g)=ys{1,j}(i-1,g-1)/4 + ys{1,j}(i,g-1)/2 + ys{1,j}(i+1,g-1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,g)=xs{1,j}(iNoOfPoints(j),g-1)/4 + xs{1,j}(iNoOfPoints(j)+1,g-1)/2 + xs{1,j}(2,g-1)/4;
  ys{1,j}(iNoOfPoints(j)+1,g)=ys{1,j}(iNoOfPoints(j),g-1)/4 + ys{1,j}(iNoOfPoints(j)+1,g-1)/2 + ys{1,j}(2,g-1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,g)=xs{1,j}(1,g);
  ys{1,j}(iNoOfPoints(j)+2,g)=ys{1,j}(1,g);
   end
 
   %the smooth outline came back and it is taken as original
   cx{1,j}=xs{1,j}(:,NoOfSmoothing);
   cy{1,j}=ys{1,j}(:,NoOfSmoothing);
   end
 end 


 if centerOfMass==1     
     for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end
   %Shift x and y, for each real cell to have its center at the origin
  for j=1:NoOfCells
    sumX{1,j}(1) = sum(cx{1,j});
    sumY{1,j}(1) = sum(cy{1,j});
    xshift{1,j}(1)=sumX{1,j}(1)/iNoOfPoints(1,j)+1;
    yshift{1,j}(1)=sumY{1,j}(1)/iNoOfPoints(1,j)+1;
  end
  
  for j=1:NoOfCells
      for i= 1: iNoOfPoints(1,j) + 1
  cx{1,j}(i,1)=cx{1,j}(i,1)-xshift{1,j}(1);
  cy{1,j}(i,1)=cy{1,j}(i,1)-yshift{1,j}(1);
      end
  end
 end
 
cc=hsv(6);
ww=1;
figure('position',[10, 10, 700,80])

subplot(1,NoOfCells,1)
plot(cx{1,1},cy{1,1}, 'color',cc(1,:),'LineWidth',ww)
axis ([0 2.5 0.5 3]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])
hold on

subplot(1,NoOfCells,2)
plot(cx{1,2},cy{1,2}, 'color',cc(2,:),'LineWidth',ww)
axis ([0 2.5 0.5 3]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])
hold on


subplot(1,NoOfCells,3)
plot(cx{1,3},cy{1,3}, 'color',cc(3,:),'LineWidth',ww)
hold on
axis ([0 2.5 0.4 2.9]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])
hold on

subplot(1,NoOfCells,4)
plot(cx{1,4},cy{1,4}, 'color',cc(4,:),'LineWidth',ww)
hold on
axis ([0 2.5 0.25 2.75]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])
hold on

subplot(1,NoOfCells,5)
plot(cx{1,5},cy{1,5}, 'color',cc(5,:),'LineWidth',ww)
hold on
axis ([0 2.5 -0.6 1.9]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])
hold on

subplot(1,NoOfCells,NoOfCells)
plot(cx{1,6},cy{1,6}, 'color',cc(6,:),'LineWidth',ww)
%axis ([0 2 0 2]);
axis ([0 2.5 -0.25 2.25]);
axis equal
ax=gca;
PlotBoxAspectRatio.ax=[1 1 1];
set(ax,'fontsize',6)
set(gca,'xtick',[])
set(gca,'ytick',[])


% Get dimensions of the figure correctly
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/fig3NS','epsc');


