
%% This code draws the cell09 
% clear the working space
clc
%Load the data with headers throughout the file
[header, data]=mhdrload('../../output/FigS3/FigS3C/Output_CellShapeAnalysis/star_outline/star_outline.dat');

resolution=0;
smoothing=1;
NoOfSmoothing=5;
areanorm=1;
plotcells=1;
centerOfMass=0;

%% EXTRACT THE DATA FROM THE OUTPUT
sigma = sscanf(header(1,:,:),'%*s %*s %d');
maxsigma=max(sigma);
NoOfCells=length(header(1,1,:));
index= data(:,1,1:NoOfCells);
for i=1:NoOfCells
ind(:,i)= index(:,1,i);
end

x= data(:,2,1:NoOfCells);
for i=1:NoOfCells
cellx(:,i)= x(:,1,i);
end

y= data(:,3,1:NoOfCells);
for i=1:NoOfCells
celly(:,i)= y(:,1,i);
end

 
%this a vector containing the number of data points

maximo= max(ind);
numeromaximo=ind(1:end,1);
nummax=length(numeromaximo);


%Create an cell array and erase the zeros

dimensions= ones(1,NoOfCells);
%Create an cell array x
cx=mat2cell(cellx,[nummax],dimensions); 

%%eliminate the zeros of each cell
for i=1:NoOfCells
cx{i}(maximo(i)+1:end)=[];
cx_original{i}=cx{i};
end

%Create an cell array y
cy=mat2cell(celly,[nummax],dimensions); 

%eliminate the zeros of each cell
for i=1:NoOfCells
cy{i}(maximo(i)+1:end)=[];
cy_original{i}=cy{i};
end

%perimeter
cellperimeters= data(:,6,1:NoOfCells);
for i=1:NoOfCells
cellperimeter(i,:)= cellperimeters(maximo(1,i),1,i);
end
 
cellareas= data(:,7,1:NoOfCells);
 for i=1:NoOfCells
cellarea(i,:)= cellareas(1,1,i);
 end
 
  if resolution==1
     for i=1:NoOfCells
   cellarea(i,:)=cellarea(i,:)*res; 
   cellperimeter(i,:)=cellperimeter(i,:)*sqrt(res);
     end 
  
   end


%Normalize by area 

if areanorm==1
for j=1:NoOfCells
    for i=1:maximo(j)
    cx{1,j}(i,1)=cx{1,j}(i,1)/sqrt(cellarea(j,1));
    cy{1,j}(i,1)=cy{1,j}(i,1)/sqrt(cellarea(j,1));
    end
end    
end

%% Smoothing the outline

 if smoothing==1
     for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end

  for j=1:NoOfCells
  xs{1,j}(1,1)=cx{1,j}(iNoOfPoints(j)+1,1)/4 + cx{1,j}(1,1)/2 + cx{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,1)=cx{1,j}(i-1,1)/4 + cx{1,j}(i,1)/2 + cx{1,j}(i+1,1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,1)=cx{1,j}(maximo(j),1)/4 + cx{1,j}(iNoOfPoints(j)+1,1)/2 + cx{1,j}(1,1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,1)=xs{1,j}(1,1);
  
  ys{1,j}(1,1)=cy{1,j}(iNoOfPoints(j)+1,1)/4 + cy{1,j}(1,1)/2 + cy{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  ys{1,j}(i,1)=cy{1,j}(i-1,1)/4 + cy{1,j}(i,1)/2 + cy{1,j}(i+1,1)/4;
  end
  ys{1,j}(iNoOfPoints(j)+1,1)=cy{1,j}(iNoOfPoints(j),1)/4 + cy{1,j}(iNoOfPoints(j)+1,1)/2 + cy{1,j}(1,1)/4;
  %Close the contour
  ys{1,j}(iNoOfPoints(j)+2,1)=ys{1,j}(1,1);

  end
  
   %Smoothting the outline (several times)
   for j=1:NoOfCells
   for g=2:NoOfSmoothing
    xs{1,j}(1,g)= xs{1,j}(iNoOfPoints(j)+1,g-1)/4 + xs{1,j}(1,g-1)/2 + xs{1,j}(2,g-1)/4;
    ys{1,j}(1,g)= ys{1,j}(iNoOfPoints(j)+1,g-1)/4 + ys{1,j}(1,g-1)/2 + ys{1,j}(2,g-1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,g)=xs{1,j}(i-1,g-1)/4 + xs{1,j}(i,g-1)/2 + xs{1,j}(i+1,g-1)/4;
  ys{1,j}(i,g)=ys{1,j}(i-1,g-1)/4 + ys{1,j}(i,g-1)/2 + ys{1,j}(i+1,g-1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,g)=xs{1,j}(iNoOfPoints(j),g-1)/4 + xs{1,j}(iNoOfPoints(j)+1,g-1)/2 + xs{1,j}(2,g-1)/4;
  ys{1,j}(iNoOfPoints(j)+1,g)=ys{1,j}(iNoOfPoints(j),g-1)/4 + ys{1,j}(iNoOfPoints(j)+1,g-1)/2 + ys{1,j}(2,g-1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,g)=xs{1,j}(1,g);
  ys{1,j}(iNoOfPoints(j)+2,g)=ys{1,j}(1,g);
   end
 
   %the smooth outline came back and it is taken as original
   cx{1,j}=xs{1,j}(:,NoOfSmoothing);
   cy{1,j}=ys{1,j}(:,NoOfSmoothing);
   end
 end 

%% Make the plot
for j=1:NoOfCells 
plot(cx{1,j},cy{1,j}, '-','LineWidth',4) 
hold on
set(gca, 'visible', 'on'); 
axis equal
axis([0.5 3 0.25 2.75]);
end
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/figS3C_inset.eps','epsc');
