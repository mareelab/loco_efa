% Generate geometrical shapes using the superformula by Johan Gielis
% Shorter version of superformula_shapes2.m created 16/02/2016. 
% This script generates the geometrical shapes colored.

% clear the working space
clc

%Create angles
r=0:(1*pi/180):(360*pi/180);

% Set parameters as in the paper Gielis et al., 2003
p_circle=[0, 1000,2,2];
p_triangle=[3,1000,1980,1980];
p_square=[4,1000,1000,1000];
p_pentagon=[5,1000,620,620];
p_hexagon=[6,1000,390,390];
p_heptagon=[7,1000,320,320];
p_octagon=[8,1000,250,250];
p_ninegon=[9,1000,200,200]; % p_ninegon=[9,1000,250,250]===makes it pointy! 
p_decagon=[10,1000,150,150];

for i=1:361
circle_r(i)=1/(((abs(cos(r(i)*(p_circle(1)/4))))^p_circle(3) + (abs(sin(r(i)*(p_circle(1)/4))))^p_circle(4))^(1/p_circle(2)));
triangle_r(i)=1/((abs(cos(r(i)*3/4))^1980 + (abs(sin(r(i)*3/4))^1980))^(1/1000));
square_r(i)=1/(((abs(cos(r(i)*p_square(1)/4)))^p_square(3) + (abs(sin(r(i)*p_square(1)/4)))^p_square(4))^(1/p_square(2)));
pentagon_r(i)=1/(((abs(cos(r(i)*(p_pentagon(1)/4))))^p_pentagon(3) + (abs(sin(r(i)*(p_pentagon(1)/4))))^p_pentagon(4))^(1/p_pentagon(2)));
hexagon_r(i)=1/(((abs(cos(r(i)*(p_hexagon(1)/4))))^p_hexagon(3) + (abs(sin(r(i)*(p_hexagon(1)/4))))^p_hexagon(4))^(1/p_hexagon(2)));
heptagon_r(i)=1/(((abs(cos(r(i)*(p_heptagon(1)/4))))^p_heptagon(3) + (abs(sin(r(i)*(p_heptagon(1)/4))))^p_heptagon(4))^(1/p_heptagon(2)));
octagon_r(i)=1/(((abs(cos(r(i)*(p_octagon(1)/4))))^p_octagon(3) + (abs(sin(r(i)*(p_octagon(1)/4))))^p_octagon(4))^(1/p_octagon(2)));
ninegon_r(i)=1/(((abs(cos(r(i)*(p_ninegon(1)/4))))^p_ninegon(3) + (abs(sin(r(i)*(p_ninegon(1)/4))))^p_ninegon(4))^(1/p_ninegon(2)));
decagon_r(i)=1/(((abs(cos(r(i)*(p_decagon(1)/4))))^p_decagon(3) + (abs(sin(r(i)*(p_decagon(1)/4))))^p_decagon(4))^(1/p_decagon(2)));
end

%Get x and y coordinates: x_circle(i)=circle(i)*cos(r(i)) and y_circle(i)=circle(i)*sin(r(i));
for i=1:361
%x    
circle(1,i)=cos(r(i))*circle_r(i);
triangle(1,i)=cos(r(i))*triangle_r(i);
square(1,i)=cos(r(i))*square_r(i);
pentagon(1,i)=cos(r(i))*pentagon_r(i);
hexagon(1,i)=cos(r(i))*hexagon_r(i);
heptagon(1,i)=cos(r(i))*heptagon_r(i);
octagon(1,i)=cos(r(i))*octagon_r(i);
ninegon(1,i)=cos(r(i))*ninegon_r(i);
decagon(1,i)=cos(r(i))*decagon_r(i);
%y
circle(2,i)=sin(r(i))*circle_r(i);
triangle(2,i)=sin(r(i))*triangle_r(i);
square(2,i)=sin(r(i))*square_r(i);
pentagon(2,i)=sin(r(i))*pentagon_r(i);
hexagon(2,i)=sin(r(i))*hexagon_r(i);
heptagon(2,i)=sin(r(i))*heptagon_r(i);
octagon(2,i)=sin(r(i))*octagon_r(i);
ninegon(2,i)=sin(r(i))*ninegon_r(i);
decagon(2,i)=sin(r(i))*decagon_r(i);
end  



%% Figure Geometrical shapes
scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4)/2 scrsz(3)/1 scrsz(4)/3],'Colormap',rand(128,3)); 
cc=hsv(9); % This generates the colours.
line_width=2;

subplot (1,9,1)
plot(circle(1,:),circle(2,:),'LineWidth',line_width,'color',cc(1,:));
%title('A','LineWidth',3,'fontSize', 16,'FontWeight','bold') 
%set(gca, 'visible', 'off') ; 
axis equal
axis([-1.5 1.5 -1.5 1.5]);
%title('A','LineWidth',3,'fontSize', 16,'FontWeight','bold') 

subplot (1,9,2)
plot(triangle(1,:),triangle(2,:),'LineWidth',line_width,'color',cc(2,:));
%set(gca, 'visible', 'off') ; 
axis equal
axis([-2.5 2.5 -2.5 2.5 ]);
%title('B','LineWidth',3,'fontSize', 16,'FontWeight','bold')

subplot (1,9,3)
plot(square(1,:),square(2,:),'LineWidth',line_width,'color',cc(3,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('C','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);

subplot (1,9,4)
plot(pentagon(1,:),pentagon(2,:),'LineWidth',line_width,'color',cc(4,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('D','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);

subplot (1,9,5)
plot(hexagon(1,:),hexagon(2,:),'LineWidth',line_width,'color',cc(5,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('E','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);

subplot (1,9,6)
plot(heptagon(1,:),heptagon(2,:),'LineWidth',line_width,'color',cc(6,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('F','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);

subplot (1,9,7)
plot(octagon(1,:),octagon(2,:),'LineWidth',line_width,'color',cc(7,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('G','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);

subplot (1,9,8)
plot(ninegon(1,:),ninegon(2,:),'LineWidth',line_width,'color',cc(8,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('H','LineWidth',3,'fontSize', 16,'FontWeight','bold')
axis([-1.5 1.5 -1.5 1.5])
%axis([-2.5 2.5 -2.5 2.5 ]);

subplot (1,9,9)
plot(decagon(1,:),decagon(2,:),'LineWidth',line_width,'color',cc(9,:));
%set(gca, 'visible', 'off') ; 
axis equal
%title('I','LineWidth',3,'fontSize', 16,'FontWeight','bold')
%axis([-2.5 2.5 -2.5 2.5 ]);
axis([-1.5 1.5 -1.5 1.5]);
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/fig3AI','epsc');
