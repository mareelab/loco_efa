
% This script creates Figure S3, panel C. Created on the 4-09-2016
% The data files that are loaded were created as follows:
% ./cellshapeanalysis_08 cellshapeanalysis_08.par LOCO-EFA-Ln-star 00003.png
% ./cellshapeanalysis08_compare_LocoEfa_Efa_v02_outputsPn_no_normalized  cellshapeanalysis_08.par LOCO-EFA-Pn-noaligment-star 00003.png
% ./cellshapeanalysis08_compare_LocoEfa_Efa_v02 cellshapeanalysis_08.par LOCO-EFA-Pn-aligned-star 00003.png

% On 10-05-17
% Bar plots were done again using version cellshapeanalysis_09 
 
% clear the working space
clc

%Load the data with headers throughout the file
[header, data1]=mhdrload(['../../output/FigS3/FigS3C/Output_CellShapeAnalysis/LOCO-EFA-Ln-star/LOCO-EFA-Ln-star.dat']);
[header, data2]=mhdrload(['../../output/FigS3/FigS3C/Output_CellShapeAnalysis/LOCO-EFA-Pn-noaligment-star/LOCO-EFA-Pn-noaligment-star.dat']);
[header, data3]=mhdrload(['../../output/FigS3/FigS3C/Output_CellShapeAnalysis/LOCO-EFA-Pn-aligned-star/LOCO-EFA-Pn-aligned-star.dat']);

% Stores de Ln or Pn
Ln = data1(1,60:end);
Pn1 = data2(1,60:end);
Pn2 = data3(1,60:end);

%Makes de bar plot graphs: 'Pn without frequency alignment'

figure('Position',[0 0 900 500],'Colormap',[0 0 1]); 
bar(Pn1(1:15),'LineWidth',1.5)
set(gca,'XTick',1:1:15)
set(gca,'fontsize',26,'LineWidth',2,'FontName', 'Arial')
axis ([0 15 0.0 9]);
%title('Pn without frequency alignment')

% Save dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/figS3C-Pn-no-aligned.eps','epsc');

% Makes de bar plot graphs: 'Pn with frequency alignment'
figure('Position',[0 0 900 500],'Colormap',[0 0 1]); 
bar(Pn2(1:15),'LineWidth',1.5)
set(gca,'XTick',1:1:15)
set(gca,'fontsize',26,'LineWidth',2,'FontName', 'Arial')
axis ([0 15 0.0 9]);
%title('Pn with frequency alignment')
% Save dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/figS3C-Pn-aligned.eps','epsc');

%Makes de bar plot graphs: 'Ln from LOCO-EFA'
figure('Position',[0 0 900 500],'Colormap',[0 0 1]); 
bar(Ln(1:15),'LineWidth',1.5)
set(gca,'XTick',1:1:15)
set(gca,'fontsize',26,'LineWidth',2,'FontName', 'Arial')
axis ([0 15 0.0 0.7]);
%title('Ln from LOCO-EFA')
% Save dimensions of the figure correctly
oldscreenunits = get(gcf,'Units');
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/figS3C-Ln.eps','epsc');






