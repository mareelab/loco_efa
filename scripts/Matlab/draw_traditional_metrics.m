
%% draw outlines of traditional metrics

% clear the working space
clc
%Load the data with headers throughout the file
[header, data]=mhdrload('../../output/Fig1/Output_CellShapeAnalysis/Outline_traditionalmetrics/Outline_traditionalmetrics.dat');
%[header, data]=mhdrload('../../data/unused/Outline_traditionalmetrics_new.dat');
res=0.36*0.36;
resolution=0;
smoothing=1;
areanorm=0;
plotcells=1;
centerOfMass=0;
sigma = sscanf(header(1,:,:),'%*s %*s %d');
maxsigma=max(sigma);
NoOfCells=length(header(1,1,:));

index= data(:,1,1:NoOfCells);
for i=1:NoOfCells
ind(:,i)= index(:,1,i);
end

x= data(:,2,1:NoOfCells);
for i=1:NoOfCells
cellx(:,i)= x(:,1,i);
end

y= data(:,3,1:NoOfCells);
for i=1:NoOfCells
celly(:,i)= y(:,1,i);
end
 
%this a vector containing the number of data points

maximo= max(ind);
numeromaximo=ind(1:end,1);
nummax=length(numeromaximo);


%Create an cell array and erase the zeros

dimensions= ones(1,NoOfCells);
%Create an cell array x
cx=mat2cell(cellx,[nummax],dimensions); 

%%eliminate the zeros of each cell
for i=1:NoOfCells
cx{i}(maximo(i)+1:end)=[];
cx_original{i}=cx{i};
end

%Create an cell array y
cy=mat2cell(celly,[nummax],dimensions); 

%eliminate the zeros of each cell
for i=1:NoOfCells
cy{i}(maximo(i)+1:end)=[];
cy_original{i}=cy{i};
end

%perimeter
cellperimeters= data(:,4,1:NoOfCells);
for i=1:NoOfCells
cellperimeter(i,:)= cellperimeters(maximo(1,i),1,i);
end

%areas
cellareas= data(:,7,1:NoOfCells);
 for i=1:NoOfCells
cellarea(i,:)= cellareas(1,1,i);
 end
 
  if resolution==1
     for i=1:NoOfCells
   cellarea(i,:)=cellarea(i,:)*res; 
   cellperimeter(i,:)=cellperimeter(i,:)*sqrt(res);
     end 
  
  end

%Normalize by area 

if areanorm==1
for j=1:NoOfCells
    for i=1:maximo(j)
    cx{1,j}(i,1)=cx{1,j}(i,1)/sqrt(cellarea(j,1));
    cy{1,j}(i,1)=cy{1,j}(i,1)/sqrt(cellarea(j,1));
    end
end    
end


        for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end

%% % Smoothing the outline
  %Smoothting the outline (once)
  NoOfSmoothing=5;
       

 if smoothing==1
     for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end

  for j=1:NoOfCells
  xs{1,j}(1,1)=cx{1,j}(iNoOfPoints(j)+1,1)/4 + cx{1,j}(1,1)/2 + cx{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,1)=cx{1,j}(i-1,1)/4 + cx{1,j}(i,1)/2 + cx{1,j}(i+1,1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,1)=cx{1,j}(maximo(j),1)/4 + cx{1,j}(iNoOfPoints(j)+1,1)/2 + cx{1,j}(1,1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,1)=xs{1,j}(1,1);
  
  ys{1,j}(1,1)=cy{1,j}(iNoOfPoints(j)+1,1)/4 + cy{1,j}(1,1)/2 + cy{1,j}(2,1)/4;
  for i=2:iNoOfPoints(j)
  ys{1,j}(i,1)=cy{1,j}(i-1,1)/4 + cy{1,j}(i,1)/2 + cy{1,j}(i+1,1)/4;
  end
  ys{1,j}(iNoOfPoints(j)+1,1)=cy{1,j}(iNoOfPoints(j),1)/4 + cy{1,j}(iNoOfPoints(j)+1,1)/2 + cy{1,j}(1,1)/4;
  %Close the contour
  ys{1,j}(iNoOfPoints(j)+2,1)=ys{1,j}(1,1);

  end
  
   %Smoothing the outline (several times)
   for j=1:NoOfCells
   for g=2:NoOfSmoothing
    xs{1,j}(1,g)= xs{1,j}(iNoOfPoints(j)+1,g-1)/4 + xs{1,j}(1,g-1)/2 + xs{1,j}(2,g-1)/4;
    ys{1,j}(1,g)= ys{1,j}(iNoOfPoints(j)+1,g-1)/4 + ys{1,j}(1,g-1)/2 + ys{1,j}(2,g-1)/4;
  for i=2:iNoOfPoints(j)
  xs{1,j}(i,g)=xs{1,j}(i-1,g-1)/4 + xs{1,j}(i,g-1)/2 + xs{1,j}(i+1,g-1)/4;
  ys{1,j}(i,g)=ys{1,j}(i-1,g-1)/4 + ys{1,j}(i,g-1)/2 + ys{1,j}(i+1,g-1)/4;
  end
  xs{1,j}(iNoOfPoints(j)+1,g)=xs{1,j}(iNoOfPoints(j),g-1)/4 + xs{1,j}(iNoOfPoints(j)+1,g-1)/2 + xs{1,j}(2,g-1)/4;
  ys{1,j}(iNoOfPoints(j)+1,g)=ys{1,j}(iNoOfPoints(j),g-1)/4 + ys{1,j}(iNoOfPoints(j)+1,g-1)/2 + ys{1,j}(2,g-1)/4;
  %Close the contour
  xs{1,j}(iNoOfPoints(j)+2,g)=xs{1,j}(1,g);
  ys{1,j}(iNoOfPoints(j)+2,g)=ys{1,j}(1,g);
   end
 
   %the smooth outline came back and it is taken as original
   cx{1,j}=xs{1,j}(:,NoOfSmoothing);
   cy{1,j}=ys{1,j}(:,NoOfSmoothing);
   end
 end 


 if centerOfMass==1     
     for i=1:NoOfCells
iNoOfPoints(1,i) = length(cx{1,i})-1; %In Kluh and Giardina called p
     end
   %Shift x and y, for each real cell to have its center at the origin
  for j=1:NoOfCells
    sumX{1,j}(1) = sum(cx{1,j});
    sumY{1,j}(1) = sum(cy{1,j});
    xshift{1,j}(1)=sumX{1,j}(1)/iNoOfPoints(1,j)+1;
    yshift{1,j}(1)=sumY{1,j}(1)/iNoOfPoints(1,j)+1;
  end
  
  for j=1:NoOfCells
      for i= 1: iNoOfPoints(1,j) + 1
  cx{1,j}(i,1)=cx{1,j}(i,1)-xshift{1,j}(1);
  cy{1,j}(i,1)=cy{1,j}(i,1)-yshift{1,j}(1);
      end
  end
 end
 
 %% Plot the outlines
 
 if plotcells==1
    for j=1:NoOfCells
    figure 
    plot(cx{1,j},cy{1,j}, 'color','k','LineWidth',5)
    %set(gcf,'Color',[0, 0, 0]);
    set(gca, 'visible', 'off') ; 
    axis equal
    title(j)
    %axis ([-2.5 2.5 -2.5 2.5]);
    images = ['../../output/fig1_cell0' num2str(j) '.png'];
    saveas(gcf,images);
    end
end

%% Calculate formfactor 

 for j=1:NoOfCells
     formfactor(j,:)=(cellperimeter(j,1)*cellperimeter(j,1))/(4*pi*cellarea(j,1));
 end

 for j=1:NoOfCells
g=sprintf('%d ', formfactor(j,:));
fprintf('Form factor of cell %d: %s\n',j, g)
 end
 
 %% Calculate  aspect ratio: uses regionprops to extract major and minor axis.

for j=1:NoOfCells
     img=repmat(uint8(0),[50 50]); % Create an m by n array of 0's
     for k=1:length(cx{1,j}) %assuming the length of x and y are same
         img(round(cx{1,j}(k,1)),round(cy{1,j}(k,1))) = 1;
     end
     img=img==1;
     stats(j) = regionprops(img,'MajorAxisLength','MinorAxisLength');
     %imshow(img)
end

for j=1:NoOfCells
 aspectratio(j,:) = stats(j).MinorAxisLength/stats(j).MajorAxisLength;
end

 for j=1:NoOfCells
g=sprintf('%d ', aspectratio(j,:));
fprintf('Aspect ratio of cell %d: %s\n',j, g)
 end
 