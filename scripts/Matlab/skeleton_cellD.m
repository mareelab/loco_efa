% read in the image
 set (gca,'color','none');
 img = imread('../../data/raw/traditional_metrics/cell03.png');

% imwrite(img,'testing1.png');

% the standard skeletonization:
% imwrite(bwmorph(img,'skel',inf),'testing2.png');

% the new method:
% imwrite(bwmorph(skeleton(img)>85,'skel',Inf),'testing3.png');

% in more detail:
[skr,rad] = skeleton(img);

% the intensity at each point is proportional to the degree of evidence
% that this should be a point on the skeleton:
% imagesc(skr);
% colormap jet
% axis image off

% skeleton can also return a map of the radius of the largest circle that
% fits within the foreground at each point:
% imagesc(rad)
% colormap jet
% axis image off

% thresholding the skeleton can return skeletons of thickness 2,
% so the call to bwmorph completes the thinning to single-pixel width.
skel = bwmorph(skr > 45,'skel',inf);
% anaskel returns the locations of endpoints and junction points
[dmap,exy,jxy] = anaskel(skel);
cell_outline = edge(img);
se = strel('disk', 4, 4);
thickened = imdilate(cell_outline, se);
imagesc(-(skel + thickened))
colormap gray
axis image off
hold on
plot(exy(1,:),exy(2,:),'mo','LineWidth',1)
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/fig1H_D1.eps','epsc');

% thresholding the skeleton can return skeletons of thickness 2,
% so the call to bwmorph completes the thinning to single-pixel width.
skel = bwmorph(skr > 65,'skel',inf);
% anaskel returns the locations of endpoints and junction points
[dmap,exy,jxy] = anaskel(skel);
cell_outline = edge(img);
se = strel('disk', 4, 4);
thickened = imdilate(cell_outline, se);
imagesc(-(skel + thickened))
colormap gray
axis image off
hold on
plot(exy(1,:),exy(2,:),'mo','LineWidth',1)
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/fig1H_D2.eps','epsc');

% thresholding the skeleton can return skeletons of thickness 2,
% so the call to bwmorph completes the thinning to single-pixel width.
skel = bwmorph(skr > 85,'skel',inf);
% anaskel returns the locations of endpoints and junction points
[dmap,exy,jxy] = anaskel(skel);
cell_outline = edge(img);
se = strel('disk', 4, 4);
thickened = imdilate(cell_outline, se);
imagesc(-(skel + thickened))
colormap gray
axis image off
hold on
plot(exy(1,:),exy(2,:),'mo','LineWidth',1)
oldpaperunits = get(gcf,'PaperUnits');
oldpaperpos = get(gcf,'PaperPosition');
set(gcf,'Units','pixels');
scrpos = get(gcf,'Position');
newpos = scrpos/100;
set(gcf,'PaperUnits','inches', 'PaperPosition',newpos);
saveas(gcf,'../../output/fig1H_D3.eps','epsc');
