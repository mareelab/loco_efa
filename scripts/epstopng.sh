#!/bin/sh
# Script to convert eps into png
# First it uses epstopdf to create a pdf, then it uses pdftopng to create
# a png. This way things seem to work OK, but if you want to go directly
# from eps to png with epstopng2, gs sometimes doesn't want to convert the
# postscript into png. 

#set -x

PROG=${0##*/}		# program name
TMP=/tmp/$PROG.$$	# tmp-dir
width=""

USAGE="
Usage:
$PROG -options file.eps

	OPTIONS:
	-w <number>:	Width of produced png file in pixels (default 800)
"

# Test if all scripts are available
which epstopdf >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find epstopdf
	"
  exit 1
fi
which pdftopng.sh >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find pdftopng.sh
	"
  exit 1
fi


if [ $# != 1 ] && [ $# != 3 ] ; then
  echo "${USAGE}"
  exit 1
fi

# test if last argument is ps-file

eval LAST=\$$#		# get the last agument
if [ -r ${LAST} ] ; then	# test if it is a readable file
  magic=`dd bs=1 count=4 if=$LAST 2> /dev/null`
  if [ "$magic" != "%!PS" ] ; then
    echo "$LAST is not a ps file"
    exit 1
  fi
  FILE=${LAST}
else
  echo "${LAST} is unreadable"
  exit 1
fi

trap "/bin/rm -rf ${TMP}" EXIT	# Get ready not to leave junk
mkdir ${TMP}
FILE=${FILE##*/}
PDF=${TMP}/${FILE%.*}.pdf
epstopdf --outfile=${PDF} ${FILE}

if [ $# != 1 ] ; then
  case $1 in
    -w) ;;	# $1 is -w?
    *) echo "Unknown argument $1" ; exit 1 ;;
  esac
  if [ $2 -ne 0 -o $2 -eq 0 ] 2> /dev/null ; then	# $2 is an integer?
    pdftopng.sh -w $2 ${PDF}
  else
    echo "$2 is not an integer"
    exit 1
  fi
else
  pdftopng.sh ${PDF}
fi

/bin/cp ${TMP}/${FILE%.*}.png ${FILE%.*}.png
