#!/bin/bash
if [[ ! -d ../build/bin ]]; then
    echo no binary folder found
    echo please run \'compile_all_c_code.sh\' first
    exit 1
fi

which convert >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find convert
	Please look at ../README.md for installation details
        "
  exit 1
fi
which epstopdf >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find epstopdf
	Please look at ../README.md for installation details
        "
  exit 1
fi
which gs >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find gs
	Please look at ../README.md for installation details
        "
  exit 1
fi
which mencoder >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find mencoder
	Please look at ../README.md for installation details
        "
  exit 1
fi
which pdftops >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find pdftops
	Please look at ../README.md for installation details
        "
  exit 1
fi

MATLAB=1
which matlab >& /dev/null
if [ $? != 0 ] ; then
    MATLAB=0
fi

matlab17=0
if [ -f "/Applications/MATLAB_R2017a.app/bin/matlab" ] ; then
    matlab17=/Applications/MATLAB_R2017a.app/bin/matlab
fi

cd ../build/bin
binpath=$(pwd)
cd ../../scripts
export PATH="$(pwd):${binpath}:${PATH}"

#Fig1
mkdir -p ../output/Images/Final/Fig1
cp ../data/raw/real_tissue/Example_wt_02_cropped.png ../output/Images/Final/Fig1/fig1A.png
cp ../data/raw/real_tissue/ExpID2943_spch4_13DAS_10_example.png ../output/Images/Final/Fig1/fig1B.png
cp ../data/raw/amnioscerosa/ExpID0038-embryo2-2-proj4_cropped.tif ../output/Images/Final/Fig1/fig1C.png
cp ../data/raw/traditional_metrics/traditional_metrics_cell03.png ../output/Images/Final/Fig1/fig1Dtop.png
cp ../data/raw/traditional_metrics/traditional_metrics_cell02.png ../output/Images/Final/Fig1/fig1Etop.png
cp ../data/raw/traditional_metrics/traditional_metrics_cell04.png ../output/Images/Final/Fig1/fig1Ftop.png
cp ../data/raw/traditional_metrics/traditional_metrics_cell05.png ../output/Images/Final/Fig1/fig1Gtop.png
mkdir -p ../output/Fig1/Output_CellShapeAnalysis/
cellshapeanalysis ../data/parameterfiles/cellshapeoutline.par Outline_traditionalmetrics ../data/raw/traditional_metrics/cell03.png ../data/raw/traditional_metrics/cell02.png ../data/raw/traditional_metrics/cell04.png ../data/raw/traditional_metrics/cell05.png 
mv Output_CellShapeAnalysis/Outline_traditionalmetrics ../output/Fig1/Output_CellShapeAnalysis

#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/draw_traditional_metrics.m'); quit;"
    mv ../output/fig1_cell01.png ../output/Images/Final/Fig1/fig1Dbottom.png
    mv ../output/fig1_cell02.png ../output/Images/Final/Fig1/fig1Ebottom.png
    mv ../output/fig1_cell03.png ../output/Images/Final/Fig1/fig1Fbottom.png
    mv ../output/fig1_cell04.png ../output/Images/Final/Fig1/fig1Gbottom.png
fi
if [ ! ${matlab17} = 0 ]; then
    rm Matlab/skeleton.mex* Matlab/anaskel.mex*
    cd Matlab
    ${matlab17} -nodesktop -nodisplay -nosplash -r "run('skeleton.m'); quit;"
    ${matlab17} -nodesktop -nodisplay -nosplash -r "run('anaskel.m'); quit;"
    cd ..
    if [ ! ${MATLAB} = 0 ]; then
	matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/skeleton_cellD.m'); quit;"
	matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/skeleton_cellE.m'); quit;"
	matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/skeleton_cellF.m'); quit;"
	matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/skeleton_cellG.m'); quit;"
	mv ../output/fig1H_* ../output/Images/Final/Fig1
    fi
    rm Matlab/anaskel.mex*
    rm Matlab/skeleton.mex*
fi

cp ../data/raw/lobefinder/cell02.tiff ../output/Images/Final/Fig1/fig1H_lobefinder_E.tiff
cp ../data/raw/lobefinder/cell03.tiff ../output/Images/Final/Fig1/fig1H_lobefinder_D.tiff
cp ../data/raw/lobefinder/cell04.tiff ../output/Images/Final/Fig1/fig1H_lobefinder_F.tiff
cp ../data/raw/lobefinder/cell05.tiff ../output/Images/Final/Fig1/fig1H_lobefinder_G.tiff

#Fig2
mkdir -p ../output/Fig2/Output_CellShapeAnalysis/
cellshapeanalysis ../data/parameterfiles/cellshapeoutline.par realcell09_outline ../data/raw/diverse-effects-loco-resolution/realcell09_segmented_01.png
mv Output_CellShapeAnalysis/realcell09_outline ../output/Fig2/Output_CellShapeAnalysis/
mkdir -p ../output/Images/Final/Fig2
sed -n '/^[[:digit:]]/p' ../output/Fig2/Output_CellShapeAnalysis/realcell09_outline/realcell09_outline.dat | xmgrace -pipe -settype xy -block stdin -bxy 2:3 -viewport 0.15 0.15 0.85 0.85 -hardcopy -hdevice SVG -printfile ../output/Images/Final/Fig2/fig2A.svg
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeanalysis_reconstruction.par EFAReconstructionStar ../data/raw/superformula_shapes_amplitude/00003.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis_reconstruction.par LOCOEFAReconstructionStar ../data/raw/superformula_shapes_amplitude/00003.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis_reconstruction.par LOCOEFAReconstructionCell ../data/raw/diverse-effects-loco-resolution/realcell09_segmented_01.png
mv Output_CellShapeAnalysis/*EFAReconstruction* ../output/Fig2/Output_CellShapeAnalysis/
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00000.png ../output/Images/Final/Fig2/fig2E_orig.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00001.png ../output/Images/Final/Fig2/fig2E_1.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00002.png ../output/Images/Final/Fig2/fig2E_2.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00003.png ../output/Images/Final/Fig2/fig2E_3.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00004.png ../output/Images/Final/Fig2/fig2E_4.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00005.png ../output/Images/Final/Fig2/fig2E_5.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00006.png ../output/Images/Final/Fig2/fig2E_6.png
cp ../output/Fig2/Output_CellShapeAnalysis/EFAReconstructionStar/Reconstructed_00001/00007.png ../output/Images/Final/Fig2/fig2E_7.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00000.png ../output/Images/Final/Fig2/fig2F_orig.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00001.png ../output/Images/Final/Fig2/fig2F_1.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00002.png ../output/Images/Final/Fig2/fig2F_2.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00003.png ../output/Images/Final/Fig2/fig2F_3.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00004.png ../output/Images/Final/Fig2/fig2F_4.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00005.png ../output/Images/Final/Fig2/fig2F_5.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00006.png ../output/Images/Final/Fig2/fig2F_6.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionStar/Reconstructed_00001/00007.png ../output/Images/Final/Fig2/fig2F_7.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00001.png ../output/Images/Final/Fig2/fig2G_01.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00002.png ../output/Images/Final/Fig2/fig2G_02.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00003.png ../output/Images/Final/Fig2/fig2G_03.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00004.png ../output/Images/Final/Fig2/fig2G_04.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00006.png ../output/Images/Final/Fig2/fig2G_06.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00008.png ../output/Images/Final/Fig2/fig2G_08.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00012.png ../output/Images/Final/Fig2/fig2G_12.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/Reconstructed_00001/00016.png ../output/Images/Final/Fig2/fig2G_16.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00001.png ../output/Images/Final/Fig2/fig2H_01.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00002.png ../output/Images/Final/Fig2/fig2H_02.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00003.png ../output/Images/Final/Fig2/fig2H_03.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00004.png ../output/Images/Final/Fig2/fig2H_04.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00006.png ../output/Images/Final/Fig2/fig2H_06.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00008.png ../output/Images/Final/Fig2/fig2H_08.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00012.png ../output/Images/Final/Fig2/fig2H_12.png
cp ../output/Fig2/Output_CellShapeAnalysis/LOCOEFAReconstructionCell/XOR_00001/00016.png ../output/Images/Final/Fig2/fig2H_16.png

#Fig3
mkdir -p ../output/Fig3/Output_CellShapeAnalysis/
#first part
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par LOCO_geometricalshapes ../data/raw/superformula_geometrical_shapes/00000.png ../data/raw/superformula_geometrical_shapes/00008.png ../data/raw/superformula_geometrical_shapes/00007.png ../data/raw/superformula_geometrical_shapes/00006.png ../data/raw/superformula_geometrical_shapes/00003.png ../data/raw/superformula_geometrical_shapes/00002.png ../data/raw/superformula_geometrical_shapes/00005.png ../data/raw/superformula_geometrical_shapes/00004.png ../data/raw/superformula_geometrical_shapes/00001.png
# second part
inkscape -z -D -f ../data/raw/virtual_6_lobe/deformed_6_lobe.svg -d 300 -E "`pwd`"/../output/Fig3/deformed_6_lobe.eps
convert +antialias -density 300 ../output/Fig3/deformed_6_lobe.eps PNG24:../output/Fig3/deformed_6_lobe.png
convert ../output/Fig3/deformed_6_lobe.png -background black -alpha remove ../output/Fig3/deformed_6_lobe.png
convert ../output/Fig3/deformed_6_lobe.png -bordercolor black -border 5x5 ../output/Fig3/deformed_6_lobe.png
cellshapeanalysis ../data/parameterfiles/cellshapeoutline_biggest_only.par LOCO_geometricaloutlines_amplitude ../data/raw/superformula_shapes_amplitude/*.png ../output/Fig3/deformed_6_lobe.png ../data/raw/diverse-effects-loco-resolution/realcell09_segmented_01.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis_biggest_only.par LOCO_geometricalshapes_amplitude ../data/raw/superformula_shapes_amplitude/*.png ../output/Fig3/deformed_6_lobe.png ../data/raw/diverse-effects-loco-resolution/realcell09_segmented_01.png
mv Output_CellShapeAnalysis/LOCO_geometricalshapes ../output/Fig3/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/LOCO_geometricalshapes_amplitude Output_CellShapeAnalysis/LOCO_geometricaloutlines_amplitude ../output/Fig3/Output_CellShapeAnalysis
#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/superformula_shapes4.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_geometricalshapes_160217.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/shapesAmplitude.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_amplitude_160217.m'); quit;"
    mkdir -p ../output/Images/Final/Fig3
    mv ../output/fig3AI.eps ../output/fig3JM.eps ../output/fig3NS.eps ../output/fig3TW.eps ../output/Images/Final/Fig3
fi

#Fig4
mkdir -p ../output/Fig4/Output_CellShapeAnalysis

#Fig4A-G
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeoutline_biggest_only.par ExpID03002-cell01_outline ../data/raw/ExpID3002_cell01_segmented/*.png

#Fig4H
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeanalysis_biggest_only.par LOCO-EFA-Pn-noaligment-ExpID03002-cell01 ../data/raw/ExpID3002_cell01_segmented/*.png

#Fig4I
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis_biggest_only.par LOCO-EFA-Ln-ExpID03002-cell01 ../data/raw/ExpID3002_cell01_segmented/*.png

mv Output_CellShapeAnalysis/LOCO-EFA-Pn-noaligment-ExpID03002-cell01 Output_CellShapeAnalysis/ExpID03002-cell01_outline ../output/Fig4/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/LOCO-EFA-Ln-ExpID03002-cell01 ../output/Fig4/Output_CellShapeAnalysis
#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/old_getxy_ExpID3002_cell01.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_ExpID3002.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_ExpID3002_Pn_no_aligned.m'); quit;"
    mkdir -p ../output/Images/Final/Fig4
    mv ../output/fig4AG.eps ../output/fig4H1.eps ../output/fig4H2.eps ../output/fig4I1.eps ../output/fig4I2.eps ../output/Images/Final/Fig4
fi

#Fig5
#Fig5A
#Usage: displaycellproperties parfile microscope_image segmented_image
mkdir -p ../output/Fig5/Output_DisplayProperties
mkdir -p ../output/Fig5/Output_CellShapeAnalysis
displaycellproperties ../data/parameterfiles/display_ExpID2943_spch4_13DAS_10.par display_ExpID2943_spch4_13DAS_10 ../data/raw/real_tissue/ExpID2943_spch4_13DAS_10_example.png ../data/raw/real_tissue/expid_2943_13das_10_spch_segm_corr.png
mv Output_DisplayProperties/display_ExpID2943_spch4_13DAS_10 ../output/Fig5/Output_DisplayProperties
mkdir -p ../output/Images/Final/Fig5
convert ../output/Fig5/Output_DisplayProperties/display_ExpID2943_spch4_13DAS_10/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5A1.png
convert ../output/Fig5/Output_DisplayProperties/display_ExpID2943_spch4_13DAS_10/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5A2.png
convert ../output/Fig5/Output_DisplayProperties/display_ExpID2943_spch4_13DAS_10/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5A3.png
convert ../output/Fig5/Output_DisplayProperties/display_ExpID2943_spch4_13DAS_10/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5A4.png
cp ../data/raw/real_tissue/ExpID2943_spch4_13DAS_10_example_50microns_scalebar.png ../output/Images/Final/Fig5/panel5A5.png
#Fig5B
displaycellproperties ../data/parameterfiles/display_Example_wt_02.par display_Example_wt_02 ../data/raw/real_tissue/Example_wt_02_cropped.png ../data/raw/real_tissue/example_wt_segm_corr.png
mv Output_DisplayProperties/display_Example_wt_02 ../output/Fig5/Output_DisplayProperties
mkdir -p ../output/Images/Final/Fig5
convert ../output/Fig5/Output_DisplayProperties/display_Example_wt_02/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5B1.png
convert ../output/Fig5/Output_DisplayProperties/display_Example_wt_02/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5B2.png
convert ../output/Fig5/Output_DisplayProperties/display_Example_wt_02/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5B3.png
convert ../output/Fig5/Output_DisplayProperties/display_Example_wt_02/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5B4.png
cp ../data/raw/real_tissue/Example_wt_02_cropped_50microns_scalebar.png ../output/Images/Final/Fig5/panel5B5.png
#Fig5C
displaycellproperties ../data/parameterfiles/display_insilico_onecomb1.par display_insilico_onecomb1 ../data/raw/insilico-tissue/PavementCellsonecell_comb1_or0_el1_bo0_1345132594_1_Image.png ../data/raw/insilico-tissue/PavementCellsonecell_comb1_or0_el1_bo0_1345132594_1_Cells.png
displaycellproperties ../data/parameterfiles/display_insilico_popcomb1.par display_insilico_popcomb1 ../data/raw/insilico-tissue/PavementCellspopcell_comb1_or0_el1_bo0_1345132786_1_Image.png ../data/raw/insilico-tissue/PavementCellspopcell_comb1_or0_el1_bo0_1345132786_1_Cells.png
mv Output_DisplayProperties/display_insilico_onecomb1 ../output/Fig5/Output_DisplayProperties
mv Output_DisplayProperties/display_insilico_popcomb1 ../output/Fig5/Output_DisplayProperties
mkdir -p ../output/Images/Final/Fig5
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb1/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Ct1.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb1/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Ct2.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb1/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Ct3.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb1/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Ct4.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb1/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Cb1.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb1/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Cb2.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb1/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Cb3.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb1/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Cb4.png
convert ../output/Images/Final/Fig5/panel5Ct1.png -fill white -opaque black ../output/Images/Final/Fig5/panel5Ct1.png
convert ../output/Images/Final/Fig5/panel5Ct2.png -fill white -opaque black ../output/Images/Final/Fig5/panel5Ct2.png
convert ../output/Images/Final/Fig5/panel5Ct3.png -fill white -opaque black ../output/Images/Final/Fig5/panel5Ct3.png
convert ../output/Images/Final/Fig5/panel5Ct4.png -fill white -opaque black ../output/Images/Final/Fig5/panel5Ct4.png
convert -trim ../output/Images/Final/Fig5/panel5Ct1.png ../output/Images/Final/Fig5/panel5Ct1.png
convert -trim ../output/Images/Final/Fig5/panel5Ct2.png ../output/Images/Final/Fig5/panel5Ct2.png
convert -trim ../output/Images/Final/Fig5/panel5Ct3.png ../output/Images/Final/Fig5/panel5Ct3.png
convert -trim ../output/Images/Final/Fig5/panel5Ct4.png ../output/Images/Final/Fig5/panel5Ct4.png

#Fig5D
displaycellproperties ../data/parameterfiles/display_insilico_onecomb3.par display_insilico_onecomb3 ../data/raw/insilico-tissue/PavementCellsonecell_comb3_or0_el1_bo0_1345133816_4_Image.png ../data/raw/insilico-tissue/PavementCellsonecell_comb3_or0_el1_bo0_1345133816_4_Cells.png
displaycellproperties ../data/parameterfiles/display_insilico_popcomb3.par display_insilico_popcomb3 ../data/raw/insilico-tissue/PavementCellspopcell_comb3_or0_el1_bo0_1345133999_1_Image.png ../data/raw/insilico-tissue/PavementCellspopcell_comb3_or0_el1_bo0_1345133999_1_Cells.png
mv Output_DisplayProperties/display_insilico_onecomb3 ../output/Fig5/Output_DisplayProperties
mv Output_DisplayProperties/display_insilico_popcomb3 ../output/Fig5/Output_DisplayProperties
mkdir -p ../output/Images/Final/Fig5
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb3/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Dt1.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb3/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Dt2.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb3/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Dt3.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_onecomb3/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Dt4.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb3/L4.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Db1.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb3/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Db2.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb3/L6.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Db3.png
convert ../output/Fig5/Output_DisplayProperties/display_insilico_popcomb3/L7.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig5/panel5Db4.png
convert -trim ../output/Images/Final/Fig5/panel5Dt1.png ../output/Images/Final/Fig5/panel5Dt1.png
convert -trim ../output/Images/Final/Fig5/panel5Dt2.png ../output/Images/Final/Fig5/panel5Dt2.png
convert -trim ../output/Images/Final/Fig5/panel5Dt3.png ../output/Images/Final/Fig5/panel5Dt3.png
convert -trim ../output/Images/Final/Fig5/panel5Dt4.png ../output/Images/Final/Fig5/panel5Dt4.png

#Fig5HI
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_main_CPM_lobe06_comb1_or0_el1_bo0_1 ../data/raw/insilico-tissue/PavementCellsonecell_comb1_or0_el1_bo0_1345132594_1_Cells.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_main_CPM_lobe06_comb1_or0_el1_bo0_2 ../data/raw/insilico-tissue/PavementCellspopcell_comb1_or0_el1_bo0_1345132786_1_Cells.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_main_CPM_lobe06_comb3_or0_el1_bo0_1 ../data/raw/insilico-tissue/PavementCellsonecell_comb3_or0_el1_bo0_1345133816_4_Cells.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_main_CPM_lobe06_comb3_or0_el1_bo0_2 ../data/raw/insilico-tissue/PavementCellspopcell_comb3_or0_el1_bo0_1345133999_1_Cells.png
mv Output_CellShapeAnalysis/Cellshapeanalysis_main_CPM_lobe06_comb* ../output/Fig5/Output_CellShapeAnalysis
mkdir -p ../output/Images/Final/Fig5/data
cp ../output/Fig5/Output_CellShapeAnalysis/Cellshapeanalysis_main_CPM_lobe*_comb*_or0_el1_bo0_*/Cellshapeanalysis_main_CPM_lobe*_comb*_or0_el1_bo0_*.dat ../output/Images/Final/Fig5/data
#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plotProportionOnecellPopLobe06.m'); quit;"
fi
mv ../output/Images/Final/Fig5/data ../output/Fig5

#Fig6
mkdir -p ../output/Fig6/Output_CellShapeAnalysis
mkdir -p ../output/Fig6/Output_DisplayProperties
convert ../data/raw/amnioscerosa/ExpID0038-embryo2-2-proj4_cropped.tif PNG24:../output/Fig6/ExpID0038-embryo2-2-proj4_cropped.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Amnioscerosa ../data/raw/amnioscerosa/amnioscerosa_segmented.png
mv Output_CellShapeAnalysis/Amnioscerosa ../output/Fig6/Output_CellShapeAnalysis
displaycellproperties ../data/parameterfiles/display_amnioscerosa.par Amnioscerosa ../output/Fig6/ExpID0038-embryo2-2-proj4_cropped.png ../data/raw/amnioscerosa/amnioscerosa_segmented.png
mv Output_DisplayProperties/Amnioscerosa ../output/Fig6/Output_DisplayProperties
mkdir -p ../output/Images/Final/Fig6
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/L2.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/L2.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/L5.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/L5.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/L8.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/L8.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/area.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/area.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/maxdrop.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/maxdrop.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/entropy.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/entropy.png
convert ../output/Fig6/Output_DisplayProperties/Amnioscerosa/cumulative.png -fill white -draw 'color 0,0 floodfill' ../output/Images/Final/Fig6/cumulative.png
bukowski -X 96 -Y 5 -c YELLOW -d ll -i ../output/Fig6/ExpID0038-embryo2-2-proj4_cropped.png -o ../output/Images/Final/Fig6/rawimage_bar.png
cp ../data/raw/amnioscerosa/segmentation.png ../output/Images/Final/Fig6/segmentation.png
playcolor_07 100 10000 10 ../output/Images/Final/Fig6/colorbar.png

#FigS1
mkdir -p ../output/Images/Final/FigS1
cp ../output/Images/Final/Fig1/fig1H_E1.eps ../output/Images/Final/FigS1/figS1B1.eps
cp ../output/Images/Final/Fig1/fig1H_E2.eps ../output/Images/Final/FigS1/figS1B2.eps
cp ../output/Images/Final/Fig1/fig1H_E3.eps ../output/Images/Final/FigS1/figS1B3.eps

#FigS3
mkdir ../output/FigS3/
mkdir -p ../output/Images/Final/FigS3/

#FigS3A
#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/draw_cell09.m'); quit;"
    mv ../output/figS3A.eps ../output/Images/Final/FigS3/
fi

#FigS3B
efa_draw_example ../data/parameterfiles/draw_realcell.par ../output/FigS3/FigS3B
mkdir -p ../output/Images/Final/FigS3/FigS3B
cp ../output/FigS3/FigS3B/03599.eps ../output/FigS3/FigS3B/07422.eps ../output/FigS3/FigS3B/10767.eps ../output/FigS3/FigS3B/13518.eps ../output/FigS3/FigS3B/16404.eps ../output/FigS3/FigS3B/21604.eps ../output/Images/Final/FigS3/FigS3B
rm -r ../output/FigS3/FigS3B

#FigS3C
mkdir -p ../output/FigS3/FigS3C/Output_CellShapeAnalysis
mkdir -p ../output/Images/Final/FigS3/FigS3C
cellshapeanalysis ../data/parameterfiles/cellshapeoutline.par star_outline ../data/raw/superformula_shapes_amplitude/00003.png
mv Output_CellShapeAnalysis/star_outline ../output/FigS3/FigS3C/Output_CellShapeAnalysis
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/draw_star.m'); quit;"
    mv ../output/figS3C_inset.eps ../output/Images/Final/FigS3/FigS3C
fi
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par LOCO-EFA-Ln-star ../data/raw/superformula_shapes_amplitude/00003.png
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeanalysis.par LOCO-EFA-Pn-noaligment-star ../data/raw/superformula_shapes_amplitude/00003.png
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeanalysis_pnnorm.par LOCO-EFA-Pn-aligned-star ../data/raw/superformula_shapes_amplitude/00003.png
mv Output_CellShapeAnalysis/LOCO-EFA-Ln-star ../output/FigS3/FigS3C/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/LOCO-EFA-Pn-noaligment-star ../output/FigS3/FigS3C/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/LOCO-EFA-Pn-aligned-star ../output/FigS3/FigS3C/Output_CellShapeAnalysis
#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/star_Pn_Ln_bar_plot.m'); quit;"
    mv ../output/figS3C-Pn-no-aligned.eps ../output/figS3C-Pn-aligned.eps ../output/figS3C-Ln.eps ../output/Images/Final/FigS3/FigS3C
fi

#FigS3D
efa_draw_example ../data/parameterfiles/draw_2lobes.par ../output/FigS3/FigS3D
mkdir -p ../output/Images/Final/FigS3/FigS3D
cp ../output/FigS3/FigS3D/21605.eps ../output/Images/Final/FigS3/FigS3D
rm -r ../output/FigS3/FigS3D

#FigS3E
efa_draw_example ../data/parameterfiles/draw_4points.par ../output/FigS3/FigS3E
mkdir -p ../output/Images/Final/FigS3/FigS3E
cp ../output/FigS3/FigS3E/21605.eps ../output/Images/Final/FigS3/FigS3E
rm -r ../output/FigS3/FigS3E

#FigS3F
efa_draw_example ../data/parameterfiles/draw_exception.par ../output/FigS3/FigS3F
mkdir -p ../output/Images/Final/FigS3/FigS3F
cp ../output/FigS3/FigS3F/21605.eps ../output/Images/Final/FigS3/FigS3F
rm -r ../output/FigS3/FigS3F

#FigS4
mkdir -p ../output/FigS4

#FigS4B-E
efa_draw_example ../data/parameterfiles/secondmode.par ../output/FigS4/FigS4B
mkdir -p ../output/Images/Final/FigS4/FigS4B
cp ../output/FigS4/FigS4B/01082.eps ../output/Images/Final/FigS4/FigS4B

efa_draw_example ../data/parameterfiles/secondmode_shift.par ../output/FigS4/FigS4C
mkdir -p ../output/Images/Final/FigS4/FigS4C
cp ../output/FigS4/FigS4C/01082.eps ../output/Images/Final/FigS4/FigS4C

efa_draw_example ../data/parameterfiles/secondmode_big.par ../output/FigS4/FigS4D
mkdir -p ../output/Images/Final/FigS4/FigS4D
cp ../output/FigS4/FigS4D/01082.eps ../output/Images/Final/FigS4/FigS4D

efa_draw_example ../data/parameterfiles/secondmode_big_shift.par ../output/FigS4/FigS4E
mkdir -p ../output/Images/Final/FigS4/FigS4E
cp ../output/FigS4/FigS4E/01082.eps ../output/Images/Final/FigS4/FigS4E

#FigS5
mkdir -p ../output/FigS5/Output_CellShapeAnalysis

#FigS5A-G
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeoutline_biggest_only.par spch1-cell02_outline ../data/raw/spch1_cell02_segmented/*.png

#FigS5H
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeanalysis_biggest_only.par LOCO-EFA-Pn-noaligment-spch1-cell02 ../data/raw/spch1_cell02_segmented/*.png
#FigS5I
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis_biggest_only.par LOCO-EFA-Ln-spch1-cell02 ../data/raw/spch1_cell02_segmented/*.png

mv Output_CellShapeAnalysis/LOCO-EFA-Ln-spch1-cell02 Output_CellShapeAnalysis/spch1-cell02_outline ../output/FigS5/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/LOCO-EFA-Pn-noaligment-spch1-cell02 ../output/FigS5/Output_CellShapeAnalysis

#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/old_getxy_spch1_cell02.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_spch1.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/copy_plot_output_cellshapeanalysis_08_spch1_Pn_no_aligned.m'); quit;"
    mkdir -p ../output/Images/Final/FigS5
    mv ../output/figS5AG.eps ../output/figS5H.eps ../output/figS5I.eps ../output/Images/Final/FigS5
fi

#FigS7
mkdir -p ../output/FigS7/Output_CellShapeAnalysis

#the first image is cellstate of the single cell - example for lobe 3 combination 1
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb1/cellstate_lobe03_comb1_1.png
#the rest of the images are cellstate of the repetitions of populations - example for lobe 3 combination 1
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb1/cellstate_lobe03_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb1/cellstate_lobe03_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb1/cellstate_lobe03_comb1_4.png
# there are 3 combinations (SCS), from lobe number = 3 to 10
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb2/cellstate_lobe03_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb2/cellstate_lobe03_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb2/cellstate_lobe03_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb2/cellstate_lobe03_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb3/cellstate_lobe03_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb3/cellstate_lobe03_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb3/cellstate_lobe03_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe03_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe03_comb3/cellstate_lobe03_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb1/cellstate_lobe04_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb1/cellstate_lobe04_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb1/cellstate_lobe04_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb1/cellstate_lobe04_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb2/cellstate_lobe04_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb2/cellstate_lobe04_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb2/cellstate_lobe04_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb2/cellstate_lobe04_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb3/cellstate_lobe04_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb3/cellstate_lobe04_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb3/cellstate_lobe04_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe04_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe04_comb3/cellstate_lobe04_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb1/cellstate_lobe05_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb1/cellstate_lobe05_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb1/cellstate_lobe05_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb1/cellstate_lobe05_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb2/cellstate_lobe05_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb2/cellstate_lobe05_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb2/cellstate_lobe05_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb2/cellstate_lobe05_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb3/cellstate_lobe05_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb3/cellstate_lobe05_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb3/cellstate_lobe05_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe05_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe05_comb3/cellstate_lobe05_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb1/cellstate_lobe06_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb1/cellstate_lobe06_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb1/cellstate_lobe06_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb1/cellstate_lobe06_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb2/cellstate_lobe06_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb2/cellstate_lobe06_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb2/cellstate_lobe06_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb2/cellstate_lobe06_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb3/cellstate_lobe06_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb3/cellstate_lobe06_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb3/cellstate_lobe06_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe06_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe06_comb3/cellstate_lobe06_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb1/cellstate_lobe07_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb1/cellstate_lobe07_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb1/cellstate_lobe07_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb1/cellstate_lobe07_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb2/cellstate_lobe07_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb2/cellstate_lobe07_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb2/cellstate_lobe07_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb2/cellstate_lobe07_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb3/cellstate_lobe07_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb3/cellstate_lobe07_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb3/cellstate_lobe07_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe07_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe07_comb3/cellstate_lobe07_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb1/cellstate_lobe08_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb1/cellstate_lobe08_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb1/cellstate_lobe08_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb1/cellstate_lobe08_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb2/cellstate_lobe08_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb2/cellstate_lobe08_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb2/cellstate_lobe08_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb2/cellstate_lobe08_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb3/cellstate_lobe08_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb3/cellstate_lobe08_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb3/cellstate_lobe08_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe08_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe08_comb3/cellstate_lobe08_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb1/cellstate_lobe09_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb1/cellstate_lobe09_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb1/cellstate_lobe09_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb1/cellstate_lobe09_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb2/cellstate_lobe09_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb2/cellstate_lobe09_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb2/cellstate_lobe09_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb2/cellstate_lobe09_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb3/cellstate_lobe09_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb3/cellstate_lobe09_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb3/cellstate_lobe09_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe09_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe09_comb3/cellstate_lobe09_comb3_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb1_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb1/cellstate_lobe10_comb1_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb1_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb1/cellstate_lobe10_comb1_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb1_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb1/cellstate_lobe10_comb1_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb1_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb1/cellstate_lobe10_comb1_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb2_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb2/cellstate_lobe10_comb2_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb2_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb2/cellstate_lobe10_comb2_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb2_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb2/cellstate_lobe10_comb2_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb2_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb2/cellstate_lobe10_comb2_4.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb3_or0_el1_bo0_1 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb3/cellstate_lobe10_comb3_1.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb3_or0_el1_bo0_2 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb3/cellstate_lobe10_comb3_2.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb3_or0_el1_bo0_3 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb3/cellstate_lobe10_comb3_3.png
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par Cellshapeanalysis_CPM_lobe10_comb3_or0_el1_bo0_4 ../data/raw/CPM-different-number-of-lobes/cellstate_lobe10_comb3/cellstate_lobe10_comb3_4.png
mv Output_CellShapeAnalysis/Cellshapeanalysis_CPM_lobe* ../output/FigS7/Output_CellShapeAnalysis

mkdir -p ../output/FigS7/data
cp ../output/FigS7/Output_CellShapeAnalysis/Cellshapeanalysis_CPM_lobe*_comb*_or0_el1_bo0_*/Cellshapeanalysis_CPM_lobe*_comb*_or0_el1_bo0_*.dat ../output/FigS7/data

#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe03.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe04.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe05.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe06.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe07.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe08.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe09.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_diff_onecell_pop_CPMCells_LOCO_v08_lobe10.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_3D_Spp_LOCO.m'); quit;"
    mkdir -p ../output/Images/Final/FigS7
    mv ../output/figS7* ../output/Images/Final/FigS7
fi

#FigS9
mkdir -p ../output/FigS9

#FigS9A-F
locoefa_draw_foldedout ../data/parameterfiles/lambdafoldedout.par 0.15 0.15 0 0 0 ../output/FigS9/FigS9AC
mkdir -p ../output/Images/Final/FigS9/FigS9AC
cp ../output/FigS9/FigS9AC/00360.eps ../output/Images/Final/FigS9/FigS9AC

locoefa_draw_foldedout ../data/parameterfiles/lambdafoldedout.par 0.15 0.15 0 180 0 ../output/FigS9/FigS9DF
mkdir -p ../output/Images/Final/FigS9/FigS9DF
cp ../output/FigS9/FigS9DF/00360.eps ../output/Images/Final/FigS9/FigS9DF

#FigS9G-L
locoefa_draw_example ../data/parameterfiles/lambdasin.par 0. 0.1 135. -75. 120. ../output/FigS9/FigS9G
mkdir -p ../output/Images/Final/FigS9/FigS9G
cp ../output/FigS9/FigS9G/00721.eps ../output/Images/Final/FigS9/FigS9G

locoefa_draw_example ../data/parameterfiles/lambdasin.par 0.075 0.  135. -75. 120. ../output/FigS9/FigS9H
mkdir -p ../output/Images/Final/FigS9/FigS9H
cp ../output/FigS9/FigS9H/00721.eps ../output/Images/Final/FigS9/FigS9H

locoefa_draw_example ../data/parameterfiles/lambdasin.par 0.075 0.1 135. -75. 120. ../output/FigS9/FigS9I
mkdir -p ../output/Images/Final/FigS9/FigS9I
cp ../output/FigS9/FigS9I/00721.eps ../output/Images/Final/FigS9/FigS9I

locoefa_draw_example ../data/parameterfiles/lambdasin_circ.par 0. 0.2  135. -75. 120. ../output/FigS9/FigS9J
mkdir -p ../output/Images/Final/FigS9/FigS9J
cp ../output/FigS9/FigS9J/00721.eps ../output/Images/Final/FigS9/FigS9J

locoefa_draw_example ../data/parameterfiles/lambdasin_circ.par 0.15 0.  135. -75. 120. ../output/FigS9/FigS9K
mkdir -p ../output/Images/Final/FigS9/FigS9K
cp ../output/FigS9/FigS9K/00721.eps ../output/Images/Final/FigS9/FigS9K

locoefa_draw_example ../data/parameterfiles/lambdasin.par 0.15 0.2 135. -75. 120. ../output/FigS9/FigS9L
mkdir -p ../output/Images/Final/FigS9/FigS9L
cp ../output/FigS9/FigS9L/00721.eps ../output/Images/Final/FigS9/FigS9L

#Fig S10
mkdir -p ../output/FigS10/Output_CellShapeAnalysis/

#Fig S10A
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeoutline.par rotations_outline ../data/raw/diverse-effects-loco-rotations/realcell09_segmented_*.png

#Fig S10B
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par diverse-effects-loco-efa-rotations ../data/raw/diverse-effects-loco-rotations/*.png

#Fig S10C
cellshapeanalysis_compare_locoefa_efa ../data/parameterfiles/cellshapeoutline_biggest_only.par resolution_outline ../data/raw/diverse-effects-loco-resolution/*.png

#Fig S10D
cellshapeanalysis ../data/parameterfiles/cellshapeanalysis.par diverse-effects-loco-efa-resolution ../data/raw/diverse-effects-loco-resolution/*.png

mv Output_CellShapeAnalysis/diverse-effects-loco-efa-rotations Output_CellShapeAnalysis/rotations_outline ../output/FigS10/Output_CellShapeAnalysis
mv Output_CellShapeAnalysis/diverse-effects-loco-efa-resolution Output_CellShapeAnalysis/resolution_outline ../output/FigS10/Output_CellShapeAnalysis

#matlab
if [ ! ${MATLAB} = 0 ]; then
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/draw_challenging_LOCO_rotations.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_challengingLocoEFA_rotations.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/draw_challenging_LOCO_resolutions.m'); quit;"
    matlab -nodesktop -nodisplay -nosplash -r "run('Matlab/plot_challengingLocoEFA_resolutions.m'); quit;"
    mkdir -p ../output/Images/Final/FigS10
    mv ../output/figS10* ../output/Images/Final/FigS10
fi

## MovieS1
efa_draw_example ../data/parameterfiles/draw_realcell_movie.par ../output/MovieS1
cd ../output/MovieS1
#don't use all frames, otherwise movie becomes too big
for file in *[0,2,4,6,8].eps
do
    echo converting $file
    epstopng.sh -w 800 $file
done
# add 5 pixels at bottom
for file in *.png
do
    convert $file -gravity south -splice 0x5 -background white $file
    echo $file
done
mkdir -p ../Images/Final/MovieS1
mv *.png ../Images/Final/MovieS1
cd ..
rm -rf ./MovieS1
cd Images/Final/MovieS1
mencoder "mf://*.png" -of lavf -ovc lavc -lavcopts vcodec=mpeg1video:vrc_buf_size=917:keyint=18:vrc_maxrate=4000:vbitrate=4000:vmax_b_frames=0 -o movieS1.mpg -ofps 30 -vf harddup -oac lavc
cd ../../../../scripts
mkdir ../output/MovieS1
mv ../output/Images/Final/MovieS1/*.png ../output/MovieS1

## MovieS2
efa_draw_example ../data/parameterfiles/draw_2lobes_movie.par ../output/MovieS2
cd ../output/MovieS2
#don't use all frames, otherwise movie becomes too big
for file in *[0,2,4,6,8].eps
do
    echo converting $file
    epstopng.sh -w 800 $file
done
# add 5 pixels at bottom
for file in *.png
do
    convert $file -gravity south -splice 0x5 -background white $file
    echo $file
done
mkdir -p ../Images/Final/MovieS2
mv *.png ../Images/Final/MovieS2
cd ..
rm -rf ./MovieS2
cd Images/Final/MovieS2
mencoder "mf://*.png" -of lavf -ovc lavc -lavcopts vcodec=mpeg1video:vrc_buf_size=917:keyint=18:vrc_maxrate=4000:vbitrate=4000:vmax_b_frames=0 -o movieS2.mpg -ofps 30 -vf harddup -oac lavc
cd ../../../../scripts
mkdir ../output/MovieS2
mv ../output/Images/Final/MovieS2/*.png ../output/MovieS2

## MovieS3
efa_draw_example ../data/parameterfiles/draw_4points_movie.par ../output/MovieS3
cd ../output/MovieS3
#don't use all frames, otherwise movie becomes too big
for file in *[0,2,4,6,8].eps
 do
    echo converting $file
    epstopng.sh -w 800 $file
done
# add 5 pixels at bottom
for file in *.png
do
    convert $file -gravity south -splice 0x5 -background white $file
    echo $file
done
mkdir -p ../Images/Final/MovieS3
mv *.png ../Images/Final/MovieS3
cd ..
rm -rf ./MovieS3
cd Images/Final/MovieS3
mencoder "mf://*.png" -of lavf -ovc lavc -lavcopts vcodec=mpeg1video:vrc_buf_size=917:keyint=18:vrc_maxrate=4000:vbitrate=4000:vmax_b_frames=0 -o movieS3.mpg -ofps 30 -vf harddup -oac lavc
cd ../../../../scripts
mkdir ../output/MovieS3
mv ../output/Images/Final/MovieS3/*.png ../output/MovieS3

## MovieS4
efa_draw_example ../data/parameterfiles/draw_exception_movie.par ../output/MovieS4
cd ../output/MovieS4
#don't use all frames, otherwise movie becomes too big
for file in *[0,2,4,6,8].eps
do
    echo converting $file
    ../../scripts/epstopng.sh -w 800 $file
done
# add 5 pixels at bottom
for file in *.png
do
    convert $file -gravity south -splice 0x5 -background white $file
    echo $file
done
mkdir -p ../Images/Final/MovieS4
mv *.png ../Images/Final/MovieS4
cd ..
rm -rf ./MovieS4
cd Images/Final/MovieS4
mencoder "mf://*.png" -of lavf -ovc lavc -lavcopts vcodec=mpeg1video:vrc_buf_size=917:keyint=18:vrc_maxrate=4000:vbitrate=4000:vmax_b_frames=0 -o movieS4.mpg -ofps 30 -vf harddup -oac lavc
cd ../../../../scripts
mkdir ../output/MovieS4
mv ../output/Images/Final/MovieS4/*.png ../output/MovieS4
rm -rf Output_CellShapeAnalysis/ Output_DisplayProperties/
