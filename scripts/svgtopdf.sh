#!/bin/sh
which inkscape >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find inkscape
	Please look at ../README.md for installation details
        "
  exit 1
fi
mkdir -p ../output/figures
for file in  "`pwd`"/../figures/*.svg
do
    shortfile=${file##*/}
    echo processing $shortfile
    inkscape -z -D -f  $file -A "`pwd`"/../output/figures/${shortfile%.svg}.pdf
done
