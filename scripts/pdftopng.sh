#!/bin/sh
# Script to convert pdf into png (just creates an eps and runs epstopng2.sh)

#set -x

PROG=${0##*/}		# program name
TMP=/tmp/$PROG.$$	# tmp-dir
width=""

USAGE="
Usage:
$PROG -options file.pdf

	OPTIONS:
	-w <number>:	Width of produced png file in pixels (default 800)
"

# Test if all scripts are available
which pdftops >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find pdftops
	"
  exit 1
fi
which epstopng2.sh >& /dev/null
if [ $? != 0 ] ; then
  echo "ERROR
        Can't find epstopng2.sh
	"
  exit 1
fi

if [ $# != 1 ] && [ $# != 3 ] ; then
  echo "${USAGE}"
  exit 1
fi

# test if last argument is pdf-file

eval LAST=\$$#		# get the last agument
if [ -r ${LAST} ] ; then	# test if it is a readable file
  magic=`dd bs=1 count=4 if=$LAST 2> /dev/null`
  if [ "$magic" != "%PDF" ] ; then
    echo "$LAST is not a pdf file"
    exit 1
  fi
  FILE=${LAST}
else
  echo "${LAST} is unreadable"
  exit 1
fi

trap "/bin/rm -rf $TMP" EXIT	# Get ready not to leave junk
mkdir ${TMP}
EPS=${FILE%.*}.eps
EPS=${TMP}/${EPS##*/}
pdftops -eps ${FILE} ${EPS}

if [ $# != 1 ] ; then
  case $1 in
    -w) ;;	# $1 is -w?
    *) echo "Unknown argument $1" ; exit 1 ;;
  esac
  if [ $2 -ne 0 -o $2 -eq 0 ] 2> /dev/null ; then	# $2 is an integer?
    WIDTH=$2
  else
    echo "$2 is not an integer"
    exit 1
  fi
else
  WIDTH=800
fi

RES=600
FWIDTH=0
# Just continue until size is more than twice the desired size
echo "Please wait..."
while [ ${FWIDTH} -lt $((${WIDTH}*2)) ] ; do
  epstopng2.sh --res=${RES} --outfile=${FILE%.*}-pre.png ${EPS}
  RES=$((${RES}+600))
  FWIDTH=`file ${FILE%.*}-pre.png | cut -d ',' -f2 | cut -d ' ' -f2`
done
convert -geometry ${WIDTH} ${FILE%.*}-pre.png ${FILE%.*}.png
