# Scripts

This directory is intended to contain all scripts which generate the output and do not require compilation

## creating of repository

* *bitbucket-create* : to create the repository itself, for * * *detailed documentation, look [here](../docs/repository.md).
* *compile_all_c_code.sh* : to compile all code used
* *create_all_the_images.sh* : to generate all the figure panels
* *svgtopdf.sh* : to generate all the figures
