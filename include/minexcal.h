#ifndef _EXCAL_H
#define _EXCAL_H 

#ifdef __cplusplus
extern "C" {
#endif
#define VERSION 2.63
#ifdef _SMALL
  typedef short TYPE;
  typedef float FLOAT;
#else  
  typedef int TYPE;
  typedef double FLOAT;
#endif

/*********************************************general*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
/*****************************************end_general*/

/*********************************************include*/
#include <stdarg.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <ctype.h>
#include <png.h>
#include <grace_np.h>
/*****************************************end_include*/
	
  typedef struct POSITION {
    int xx;
    int yy;
  } Position;
  
  typedef struct PNG {
    char dirname[600];
    int nframes;
  } Png;

  typedef struct TREE {
    int cell;
    int count;
    struct TREE *next;
  } Tree;
  
  typedef struct SHAPE {
    int perimeter;
    int targetperimeter;
    Tree *neighbour;
    double sumx;
    double sumy;
    double sumxx;
    double sumxy;
    double sumyy;
    double meanx;
    double meany;
    double dx;
    double dy;
  } Shape;

  typedef struct CONTOUR {
    double x;
    double y;
    double t;
    double deltax;
    double deltay;
    double deltat;
    double sumdeltaxj;
    double sumdeltayj;
    double xi;
    double epsilon;
  } Contour;
  
  typedef struct LOCOEFA {
    double alpha;
    double beta;
    double gamma;
    double delta;
    double tau;
    double alphaprime;
    double gammaprime;
    double rho;
    double alphastar;
    double betastar;
    double gammastar;
    double deltastar;
    double r;
    double a;
    double b;
    double c;
    double d;
    double aprime;
    double bprime;
    double cprime;
    double dprime;
    double phi;
    double theta;
    double lambda1;
    double lambda2;
    double lambda21;
    double lambda12;
    double lambdaplus;
    double lambdaminus;
    double zetaplus;
    double zetaminus;
    double locooffseta;
    double locooffsetc;
    double locolambdaplus;
    double locolambdaminus;
    double locozetaplus;
    double locozetaminus;
    double locoL;
    double locoaplus;
    double locobplus;
    double lococplus;
    double locodplus;
    double locoaminus;
    double locobminus;
    double lococminus;
    double locodminus;
  } Locoefa;
  
  typedef struct CELL {
    int maxcells;
    int maxtypes;
    int **J;
    int *area;
    int *targetarea;
    int *celltype;
    int chance0;
    int chance1;
    double *copyprob;
    Shape *shape;
    Contour **contour;
    int *contourlength;
    Locoefa **locoefa;
    void *extras;
  } Cell;

#define PLANE(A) {int i,j,_ii,_jj;for(i=1,_ii=nrow+1;i<_ii;i++)for(j=1,_jj=ncol+1;j<_jj;j++){A};}
#define NEIGHBOURS(A) {extern Position neighbours[9];int neigh,_kk,x,y;	\
    for(neigh=0,_kk=9;neigh<_kk;neigh++){y=neighbours[neigh].yy;x=neighbours[neigh].xx;A};}
#define CELLS(C,A){int c,_c;for(c=0,_c=C.maxcells;c<_c;c++){A};}
  
/* #define RANDOM()	(rand()*(1.0/(RAND_MAX+1.0)))		*/
/* #define SEED(A)	(srand((unsigned int)A))	*/
  
#define RANDOM()	uniform()
#define SEED(A)		set_seed(A)

#define WRAP	   0
#define	FIXED	   1
#define	ECHO	   2

#define BLACK	   0
#define WHITE	   1
#define RED	   2
#define GREEN	   3
#define	BLUE	   4
#define YELLOW	   5
#define MAGENTA    6
#define GRAY	   7
#define GREY	   7
#define CYAN	   8

#define GGRAPH      0
#define GSET        1
#define GTITLE      2
#define GSUBTITLE   3
#define GVIEWPORT   4
#define GWORLD      5
#define GAXIS       6
#define GSCALE      7
#define GLABEL      8
#define GSETTYPE    9
#define GLEGEND    10
#define GLINE      11 
#define GSYMBOL    12
#define GERRORBAR  13
#define GTYPE      14
#define GSTYLE     15
#define GCOLOR     16
#define GPATTERN   17
#define GSIZE      18
#define GWIDTH     19
#define GPOINT     20
#define GAUTOSCALE 21
#define GREDRAW    22
#define GSORT      23
#define GKILL      24
#define GSAVE      25
#define GLOAD      26
#define GPRINT     27
#define GCLOSE     28
#define GDETACH    29

#define DEBUG 0
#define EFA 0
#define LOCOEFA 1

#define STRING   600
#ifdef _SMALL
#define	EPSILON	FLT_EPSILON
#else
#define	EPSILON	DBL_EPSILON
#endif

#define	max(a,b)	((a) > (b) ? (a) : (b))
#define	min(a,b)	((a) > (b) ? (b) : (a))
 
  /***********************************************basic*/
  int ReadOptions(char*);
  int SaveOptions(char*,char*);
  int InDat(char*,char*,char*,void*);
  TYPE **NewP();
  FLOAT **FNewP();
  TYPE **New();
  FLOAT **FNew();
  TYPE **NewPlane(int,int);
  TYPE **Fill(TYPE**,TYPE);
  int Min(TYPE**);
  int Max(TYPE**);
  long Total(TYPE**);
  TYPE **Copy(TYPE**,TYPE**);
  TYPE AsynCopy(TYPE**,int,int,TYPE);

  /******************************************arithmetic*/
  TYPE **MultV(TYPE**, TYPE**, TYPE);

  /***********************************************color*/
  int ColorTable(int,int, ...);
  int ColorName(int,int, ...);
  int ColorRGB(int,int,int,int);
  int ColorHSV(int,FLOAT,FLOAT,FLOAT);
  int ColorRamp(int,int,int);
  int ColorGrad(int,int);
  void hsv2rgb(FLOAT,FLOAT,FLOAT,unsigned char*,unsigned char*,unsigned char*);
  void rgb2hsv(unsigned char,unsigned char,unsigned char,FLOAT*,FLOAT*,FLOAT*);

  /*********************************************logical*/
  TYPE **Xor(TYPE**,TYPE**,TYPE**);
  
  /***********************************************shift*/
  int NewIndexWrap(int,int);
  int NewIndexEcho(int,int);
  int NewIndexFixed(int,int);
  TYPE **Boundaries(TYPE**);
  
  /*************************************************png*/
  void OpenPNG(Png*,char*);
  void PlanePNG(TYPE**,Png*,int);
  void CFPlanePNG(FLOAT**,TYPE**,Png*,FLOAT,FLOAT,int,int);
  void ClosePNG(Png*);
  TYPE **ReadPatPNG(TYPE**,int,int,char*,int);
  TYPE **ReadPNG(TYPE**,int,int,char*);
  int ReadSizePNG(int*,int*,char*); 

  /**********************************************random*/
  double uniform();
  int booleanrandom();
  int set_seed(int);
  
  /***************************************cellularpotts*/
  Cell *CNew(Cell*,int,int);
  void UpdateCFill(TYPE**,Cell*);
  void CellPNG(TYPE**,Cell*,Png*,int);
  void InitCellPosition(Cell*);
  void UpdateWideCellNeighbours(TYPE**,Cell*);
  void UpdateWideHexCellNeighbours(TYPE**,Cell*);
  void OneCellPosition(TYPE**,Cell*,int);
  void UpdateCellPosition(TYPE**,Cell*);
  FLOAT CFMin(FLOAT**,TYPE**);
  FLOAT CFMax(FLOAT**,TYPE**);

  /*********************************************locoefa*/
  void InitLOCOEFA(Cell*);
  void InitLOCOEFAContour(Cell*,int,int);
  void ReadContour(Cell*,int,char*);
  void CalculateEFACoefficients(Cell*,int);
  void CalculateLOCOCoefficients(Cell*,int);
  void UpdateCellContour(TYPE**,TYPE**,Cell*);
  void ReconstructContourpoint(Cell*,int,double,int,int,int,double*,double*);

  /***********************************************grace*/
  void OpenGrace(int,int,char*);
  void Grace(int,...);
  void CloseGrace(int,...);

  /*****************************************************/

#ifdef __cplusplus
}
#endif
#endif
