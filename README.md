# LOCO_EFA

## Novel method for analysing two-dimensional shape outlines


### This work has been published

**Morphometrics of complex cell shapes: lobe contribution elliptic Fourier analysis (LOCO-EFA)**

Yara E. Sánchez-Corrales<sup>1,*</sup>, Matthew Hartley<sup>1</sup>, Jop van Rooij<sup>1,2</sup>, Athanasius F.M. Marée<sup>1,‡</sup> and Verônica A. Grieneisen<sup>1,‡</sup>

*Development* (2018), v. 145, [doi:10.1242/dev.156778](http://dx.doi.org/10.1242/dev.156778)

1. Computational & Systems Biology, John Innes Centre, Norwich, UK
2. Theoretical Biology/Bioinformatics, Dept. of Biology, Utrecht University, the Netherlands.

‡. To whom correspondence should be addressed. Email: *[Stan.Maree@jic.ac.uk](email:Stan.Maree@jic.ac.uk)*, *[Veronica.Grieneisen@jic.ac.uk](email:Veronica.Grieneisen@jic.ac.uk)*.

\*. Current address: MRC-Laboratory of Molecular Biology, Cambridge Biomedical Campus, Cambridge, UK

### using this code

* To compile install this code, you'll need to install the following dependencies:

	1. Grace (http://plasma-gate.weizmann.ac.il/Grace/)
	2. PNG development libraries

* On OSX, when using `brew`, this can be installed by typing:

	* `brew install grace libpng`
	
* On, for example, CentOS and Fedora Linux, this can be installed by typing:	
	* `yum install grace grace-devel libpng libpng-devel`

* To compile the code, do:

	* `cd scripts`
   	* `compile_all_c_code.sh`

### generating the figures

* During the generation of the figure panels, besides the provided code, the following open-source programs are being used:

	* `convert` for cropping images
	* `epstopdf` for making movies
	* `gs` for making movies
	* `inkscape` for making final figures
	* `mencoder` for making movies
	* `pdftops` for making movies

* On OSX, when using `brew`, this can be installed by typing:

	* `brew install imagemagick ghostscript inkscape mplayer poppler`
	* `brew cask install mactex`

* On, for example, CentOS and Fedora Linux, this can be installed by typing:	
	* `yum install ImageMagick texlive-base ghostscript inkscape mencoder poppler-utils`

* moreover, part to of graphs are made using MatLab; the version used was R2014a; except for the skeletonisation code (which was only used to make the comparison with other methods), as it required R2017a. When MatLab is not available, these steps will be skipped.

* To generate the panels of the figures and the movies, do:

	* `create_all_the_images.sh`
